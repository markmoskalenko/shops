<?php
$rootDir = dirname(dirname(__DIR__));

$params = array_merge(
  require($rootDir . '/common/config/params.php'),
  require($rootDir . '/common/config/params-local.php'),
  require(__DIR__ . '/params.php'),
  require(__DIR__ . '/params-local.php')
);

return [
  'id'                  => 'app-backend',
  'basePath'            => dirname(__DIR__),
  'vendorPath'          => $rootDir . '/vendor',
  'controllerNamespace' => 'backend\controllers',
  'language'            => 'ru',

  'modules'             => [
    'gii' => 'yii\gii\Module',
    'debug' => $params['module.debug'],
  ],
  'extensions'          => require($rootDir . '/vendor/yiisoft/extensions.php'),
  'components'          => [
    'urlManager'   => [
      'enablePrettyUrl' => true,
      'showScriptName'  => false,
      'rules'           => [
        '<controller:\w+>/<id:\d+>'              => '<controller>/view',
        '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
        '<controller:\w+>/<action:\w+>'          => '<controller>/<action>',
        'debug/<controller>/<action>' => 'debug/<controller>/<action>',
      ]
    ],
    'db'           => $params['components.db'],
    'cache'        => $params['components.cache'],
    'mail'         => $params['components.mail'],
    'authManager'  => $params['components.authManager'],
    'user'         => [
      'identityClass' => 'common\models\User',
    ],
    'log'          => [
      'traceLevel' => YII_DEBUG ? 3 : 0,
      'targets'    => [
        [
          'class'  => 'yii\log\FileTarget',
          'levels' => ['error', 'warning'],
        ],
      ],
    ],
    'errorHandler' => [
      'errorAction' => 'site/error',
    ],
  ],
  'params'              => $params,
];