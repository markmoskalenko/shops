<?php

namespace backend\controllers;

use common\models\User;
use common\models\CategoryBase;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


class CategoryController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),

                'only' => ['index', 'delete'],

                'rules' => [
					[
						'actions' => ['index', 'delete'],
						'allow' => true,
						'roles' => [ User::ROLE_ADMIN ],
					],
				],
			],
		];
	}

	public function actionIndex( $id = null )
	{
        $oModel = $this->_loadModel( $id );

        if( $oModel->load($_POST) && $oModel->saveItem() ){

            Yii::$app->session->setFlash('flash','Категория усепешно сохранена.');
            
            Yii::$app->getResponse()->redirect( ['category/index'] );
        }

		return $this->render('update',[
              'oModel'=>$oModel
          ]);
	}

    public function actionDelete($id)
    {
        $this->_loadModel($id)->deleteNode();
        return $this->redirect(['index']);
    }

    protected function _loadModel( $id = null ){
        if( $id === null )
            return new CategoryBase();
        else{

            $oModel = CategoryBase::findOne((int)$id);

            if( $oModel === null )
                throw new NotFoundHttpException('Станица не найдена.');

            return $oModel;
        }
    }
}