<?php

namespace backend\controllers;

use common\models\Attributes;
use common\models\User;
use common\models\AttributesGroup;
use common\models\ProductGroup;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii;
use yii\web\Response;

/**
 * AttributesGroupController implements the CRUD actions for Attributes model.
 */
class AttributesGroupController extends Controller
{
    public function behaviors()
    {
        return [
          'access' => [
            'class' => \yii\filters\AccessControl::className(),

            'only' => [ 'index', 'attach', 'de-attach' ],

            'rules' => [
              [
                'actions' => [ 'index', 'attach', 'de-attach' ],
                'allow'   => true,
                'roles'   => [User::ROLE_ADMIN],
              ],
            ],
          ],
        ];
    }

	/**
	 * Lists all Attributes models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$oModelsProductGroup = ProductGroup::find()->all();
		$oModelsAttributes   = Attributes::find()->all();

		return $this->render('index', [
			'oModelsProductGroup' => $oModelsProductGroup,
			'oModelsAttributes' => $oModelsAttributes,
		]);
	}

    /**
     * AJAX экшн для привязки атрибута к группетовара
     *
     * @param $iProductGroupId - ID группы товара
     * @param $iAttributeId - id атрибута
     *
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionAttach( $iProductGroupId, $iAttributeId ){

        if( !\Yii::$app->request->isAjax )
            throw new NotFoundHttpException('Страница не найдена.');

        Yii::$app->response->format = Response::FORMAT_JSON;

        AttributesGroup::attach( $iProductGroupId, $iAttributeId );

    }

    /**
     * AJAX экшн для отвязки атрибута от группы товара
     *
     * @param $iProductGroupId - ID группы товара
     * @param $iAttributeId - id атрибута
     *
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDeAttach( $iProductGroupId, $iAttributeId ){

        if( !\Yii::$app->request->isAjax )
            throw new NotFoundHttpException('Страница не найдена.');

        Yii::$app->response->format = Response::FORMAT_JSON;

        AttributesGroup::deAttach( $iProductGroupId, $iAttributeId );
    }
}