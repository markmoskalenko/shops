<?php

namespace backend\controllers;

use common\models\CategoryCatalog;
use common\models\Manufacturer;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use common\models\User;

class ManufacturerController extends Controller
{
    public function behaviors()
    {
        return [
          'access' => [
            'class' => \yii\filters\AccessControl::className(),

            'only' => ['index', 'delete'],

            'rules' => [
              [
                'actions' => ['index', 'delete'],
                'allow' => true,
                'roles' => [ User::ROLE_ADMIN ],
              ],
            ],
          ],
        ];
    }

	public function actionIndex( $id = null )
	{

        $oModel = $this->_loadModel( $id );

        if( $oModel->load($_POST) && $oModel->save() ){

            Yii::$app->session->setFlash('flash','Мануфактура усепешно сохранена.');

            Yii::$app->getResponse()->redirect( ['manufacturer/index'] );
        }

        $oModelCategories = CategoryCatalog::getTreeMap();

        $oModelList = Manufacturer::getMapGroupByCategory();

		return $this->render('update',[
              'oModel'=>$oModel,
              'oModelList'=>$oModelList,
              'oModelCategories'=>$oModelCategories
          ]);
	}

    public function actionDelete( $id ){

        $this->_loadModel( $id )->delete();

        $this->redirect(['index']);
    }

    protected function _loadModel( $id = null ){

        if( $id === null )
            return new Manufacturer();
        else{

            $oModel = Manufacturer::findOne( (int)$id );

            if( $oModel === null )
                throw new NotFoundHttpException(404, 'Not Found');

            return $oModel;
        }
    }
}