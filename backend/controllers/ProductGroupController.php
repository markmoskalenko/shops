<?php

namespace backend\controllers;

use common\models\CategoryCatalog;
use common\models\ProductGroup;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use common\models\User;

class ProductGroupController extends Controller
{
    public function behaviors()
    {
        return [
          'access' => [
            'class' => \yii\filters\AccessControl::className(),

            'only' => ['index'],

            'rules' => [
              [
                'actions' => ['index'],
                'allow' => true,
                'roles' => [ User::ROLE_ADMIN ],
              ],
            ],
          ],
        ];
    }

	public function actionIndex( $id = null )
	{

        $oModel = $this->_loadModel( $id );

        if( $oModel->load($_POST) && $oModel->save() ){

            Yii::$app->session->setFlash('flash','Группа усепешно сохранена.');

            Yii::$app->getResponse()->redirect( ['productgroup/index'] );

        }

        $oModelCategories = CategoryCatalog::getTreeMap();

        $oModelList = ProductGroup::find()->all();

		return $this->render('update',[
            'oModel'           => $oModel,
            'oModelList'       => $oModelList,
            'oModelCategories' => $oModelCategories
          ]);
	}

    protected function _loadModel( $id = null ){

        if( $id === null )
            return new ProductGroup();
        else{

            $oModel = ProductGroup::findOne( (int)$id );

            if( $oModel === null )
                throw new NotFoundHttpException(404, 'Not Found');

            return $oModel;
        }
    }
}