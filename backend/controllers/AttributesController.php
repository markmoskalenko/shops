<?php

namespace backend\controllers;

use common\models\User;
use common\models\Attributes;
use common\models\AttributesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class AttributesController extends Controller
{
	public function behaviors()
	{
        return [
          'access' => [
            'class' => \yii\filters\AccessControl::className(),

            'only' => ['index', 'create', 'update', 'delete'],

            'rules' => [
              [
                'actions' => ['index', 'create', 'update', 'delete'],
                'allow'   => true,
                'roles'   => [User::ROLE_ADMIN],
              ],
            ],
          ],
          'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
        ];
	}

	public function actionIndex()
	{
		$searchModel = new AttributesSearch;
		$dataProvider = $searchModel->search($_GET);

		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	public function actionCreate()
	{
		$model = new Attributes;

		if ($model->load($_POST) && $model->save()) {
			return $this->redirect(['create']);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load($_POST) && $model->save()) {
			return $this->redirect(['index']);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	public function actionDelete($id)
	{
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	protected function findModel($id)
	{
		if (($model = Attributes::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('Страница не найдена.');
		}
	}
}