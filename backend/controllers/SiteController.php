<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use common\models\LoginForm;
use common\models\User;

class SiteController extends Controller
{

    public function behaviors()
    {
        return [
          'access' => [
            'class' => \yii\filters\AccessControl::className(),
            'only'  => ['index', 'delete'],
            'rules' => [
              [
                'actions' => ['login', 'error'],
                'allow'   => true,
              ],
              [
                'actions' => ['index', 'build-rbac', 'logout'],
                'allow'   => true,
                'roles'   => [User::ROLE_ADMIN],
              ],
            ],
          ],
        ];
    }

	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	public function actionIndex()
	{
		return $this->render('index');
	}

	public function actionLogin()
	{
		if (!\Yii::$app->user->isGuest) {
			$this->goHome();
		}

		$model = new LoginForm();
		if ($model->load($_POST) && $model->login()) {
			return $this->goBack();
		} else {
			return $this->render('login', [
				'model' => $model,
			]);
		}
	}

	public function actionLogout()
	{
		Yii::$app->user->logout();
		return $this->goHome();
	}

    public function actionBuildRbac(){

        $auth = \Yii::$app->getAuthManager();

        $rule = new \common\rbac\RetailRule();
        $auth->add($rule);

        $retail = $auth->createRole('retail');
        $retail->ruleName = $rule->name;
        $auth->add($retail);

        $rule = new \common\rbac\AdministratorRule();
        $auth->add($rule);

        $admin = $auth->createRole('administrator');
        $admin->ruleName = $rule->name;
        $auth->add($admin);
        $auth->addChild($admin, $retail);

        $auth->save();
    }
}