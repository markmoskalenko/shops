$(function () {
    // 6 create an instance when the DOM is ready
    $('#tree_category').jstree();
    // 7 bind to events triggered on the tree
//    $('#tree_category').on("changed.jstree", function (e, data) {
//        console.log(data.selected);
//    });
    // 8 interact with the tree - either way is OK
//    $('button').on('click', function () {
//        $('#tree_category').jstree(true).select_node('child_node_1');
//        $('#tree_category').jstree('select_node', 'child_node_1');
//        $.jstree.reference('#tree_category').select_node('child_node_1');
//    });


    // Bind tree events
    $('#tree_category').delegate("a", "click", function (event) {
            // On link click get parent li ID and redirect to category update action
            var id = $(this).parent("li").attr('id').replace('node_', '');
            window.location = '/category/index/' + id;
        }).bind("move_node.jstree", function (e, data) {
            data.rslt.o.each(function (i) {
                $.ajax({
                    async : false,
                    type: 'GET',
                    url: "/admin/store/category/moveNode",
                    data : {
                        "id" : $(this).attr("id").replace('node_',''),
                        "ref" : data.rslt.cr === -1 ? 1 : data.rslt.np.attr("id").replace('node_',''),
                        "position" : data.rslt.cp + i
                    }
//            success : function (r) {
//            }
                });
            });
        });
    $('#tree_category').jstree('open_all');
});