$('.attach').click(function(){
    var el = $(this);
    var product_group_id = el.attr('data-id-product-group');
    var attribute_id = el.attr('data-id-attribute');
    $.ajax({
        url : '/attributes-group/attach',
        data : { iProductGroupId : product_group_id, iAttributeId : attribute_id }
    }).done(function(r){
            el.hide();
            el.prev('a').show();
        });
});

$('.de-attach').click(function(){
    var el = $(this);
    var product_group_id = el.attr('data-id-product-group');
    var attribute_id = el.attr('data-id-attribute');
    $.ajax({
        url : '/attributes-group/de-attach',
        data : { iProductGroupId : product_group_id, iAttributeId : attribute_id }
    }).done(function(r){
            el.hide();
            el.next('a').show();
        });
});