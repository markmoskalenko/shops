$('#attributes-type').change(function(){

    if( $(this).val() == '102' ){

        $('.field-attributes-is_multi, .list-values').show();

    }else{
        $('.field-attributes-is_multi, .list-values').hide();

    }

});

$('#add-value').click(function() {
    var template = '<div class="item row">' +
        '<div class="del col-md-1"><a href="javascript:void(0)"><span class="glyphicon glyphicon-remove"></span></a></div>' +
        '<div class="input col-md-11"><input name="Attributes[value][]" class="form-control" type="text"/></div>' +
        '</div>';
    $(template).fadeIn('slow').appendTo('#container-input');
});

$(document).on('click', '.del', function(){
    $(this).parents('.item').remove();
})

$('#attributes-type').change();