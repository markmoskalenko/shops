<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \common\models\CategoryBase;
use backend\assets\JsTreeAsset;
use backend\assets\CategoryAsset;
use backend\widgets\CategoryTree;

JsTreeAsset::register($this);
CategoryAsset::register($this);

/**
 * @var yii\web\View $this
 * @var common\models\Category $model
 * @var ActiveForm $form
 */

?>

<div class="category-update col-xs-10">
    <?php if(Yii::$app->session->hasFlash('flash')): ?>
        <div class="alert alert-success">
            <?= Yii::$app->session->getFlash('flash') ?>
        </div>
    <?php endif; ?>

    <div class="row-fluid">
        <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($oModel, 'name') ?>

            <?= $form->field($oModel, 'root_id')->dropDownList( CategoryBase::getTreeChildAndRootMap(), ['prompt'=>'-- Корневой раздел --']) ?>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
                <a href="<?= Url::to(['/category/delete','id'=>$oModel->id]) ?>" class="btn btn-danger">Удалить</a>
            </div>
        <?php  ActiveForm::end(); ?>
    </div>

</div>
<div id="tree_category" class="col-xs-2">
    <?= CategoryTree::widget() ?>
</div>