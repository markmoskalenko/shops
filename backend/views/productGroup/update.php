<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\Category $model
 * @var ActiveForm $form
 */

?>

<div class="category-update col-xs-8">
    <?php if( !$oModel->isNewRecord ): ?>
        <h4>
            Обновление группы товаров - <?= $oModel->name ?>
            <a href="<?= Yii::$app->urlManager->createUrl('/productgroup/index'); ?>">Создать новую группу</a>
        </h4>
    <?php else: ?>
        <h4>
            Создание новой группы товаров
        </h4>
    <?php endif; ?>

    <?php if(Yii::$app->session->hasFlash('flash')): ?>
        <div class="alert alert-success">
            <?= Yii::$app->session->getFlash('flash') ?>
        </div>
    <?php endif; ?>

    <div class="row-fluid">

    <?php $form = ActiveForm::begin(); ?>

		<?= $form->field($oModel, 'name') ?>

        <?= $form->field($oModel, 'category_id')->dropDownList( $oModelCategories, ['prompt'=>'-- Выберите категорию --'] ) ?>

        <div class="form-group">
			<?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
		</div>
	<?php ActiveForm::end(); ?>

    </div>

</div><!-- category-update -->

<div id="tree_category" class="col-xs-4">
    <ul>
        <?php foreach( $oModelList as $oItem ): ?>

          <li><a href="<?= Yii::$app->urlManager->createUrl('/productgroup/index', [ 'id' => $oItem->id ]); ?>"><?= $oItem->name; ?></a></li>

        <?php endforeach; ?>
    </ul>
</div>

