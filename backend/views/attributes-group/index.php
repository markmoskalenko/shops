<?php

use yii\helpers\Html;
use backend\assets\AttributesGroupIndexAsset;

AttributesGroupIndexAsset::register($this);

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\models\AttributesSearch $searchModel
 */

$this->title = 'Attributes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attributes-group-index">

	<h1><?= Html::encode($this->title) ?></h1>

    <?php foreach( $oModelsProductGroup as $oModelProductGroup ): ?>
      <div class="item">
          <div class="header">
              <?= $oModelProductGroup->name; ?>
          </div>
          <div class="attributes">
              <ul>
                  <?php  foreach( $oModelsAttributes as $oModelAttribute): ?>
                    <li><?= $oModelAttribute->title; ?>
                        <?php $isVisibleAttachLink = true; ?>
                        <?php if( $oModelProductGroup->isAttachAttribute( $oModelAttribute->id ) ): ?>
                          <?php $isVisibleAttachLink = false; ?>
                        <?php endif; ?>

                        <a <?= $isVisibleAttachLink ? 'style="display:none"' : ''?> class="de-attach " data-id-product-group="<?= $oModelProductGroup->id ?>" data-id-attribute="<?= $oModelAttribute->id ?>" href="javascript:void(0)">Открепить</a>
                        <a <?= !$isVisibleAttachLink ? 'style="display:none"' : ''?> class="attach" data-id-product-group="<?= $oModelProductGroup->id ?>" data-id-attribute="<?= $oModelAttribute->id ?>" href="javascript:void(0)">Привязать</a>
                    </li>
                  <?php endforeach; ?>
              </ul>
          </div>
      </div>
    <?php endforeach; ?>

</div>