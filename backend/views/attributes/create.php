<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Attributes $model
 */

$this->title = 'Создание атрибута';
$this->params['breadcrumbs'][] = ['label' => 'Attributes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attributes-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>
</div>