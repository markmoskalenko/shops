<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\assets\_AttributesFormAsset;

_AttributesFormAsset::register($this);

/**
 * @var yii\web\View $this
 * @var common\models\Attributes $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="attributes-form">

	<?php $form = ActiveForm::begin(); ?>

        <div class="col-md-8">

            <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'type')->textInput()->dropDownList( $model->getMapTypes() ) ?>

            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'is_visible')->checkbox() ?>

            <?= $form->field($model, 'is_multi')->checkbox() ?>

        </div>
        <div class="col-md-4 list-values">
            <h4>Значения списка</h4>
            <div id="container-input">
                <?php foreach( $model->attributesValues as $oValue ): ?>
                <div class="item row">
                    <div class="del col-md-1"><a href="javascript:void(0)"><span class="glyphicon glyphicon-remove"></span></a></div>
                    <div class="input col-md-11"><input name="Attributes[value][<?=$oValue->id ?>]" value="<?=$oValue->value_string ?>" class="form-control" type="text"/></div>
                </div>
                <?php endforeach; ?>
            </div>
            <a href="javascript:void(0)" class="btn btn-success" id="add-value"><span class="glyphicon glyphicon-plus"></span> Добавить</a>
        </div>
        <div style="clear: both"></div>
        <div class="form-group">
            <?= Html::submitButton('Сохраниеть', ['class' => 'btn btn-success']) ?>
        </div>

	<?php ActiveForm::end(); ?>

</div>