<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\Category $oModel
 * @var ActiveForm $form
 * @var array $oModelCategories
 */

?>

<div class="category-update col-xs-8">
    <?php if( !$oModel->isNewRecord ): ?>
        <h4>
            Обновление мануфактуры - <?= $oModel->name ?>
            <a href="<?= Url::to(['/manufacturer/index']); ?>">Создать новую мануфактуру</a>
        </h4>
    <?php else: ?>
        <h4>Создание новой мануфактуры</h4>
    <?php endif; ?>
    <?php if(Yii::$app->session->hasFlash('flash')): ?>
        <div class="alert alert-success">
            <?= Yii::$app->session->getFlash('flash') ?>
        </div>
    <?php endif; ?>

    <div class="row-fluid">

    <?php $form = ActiveForm::begin(); ?>

		<?= $form->field($oModel, 'name') ?>
		<?= $form->field($oModel, 'category_id')->dropDownList( $oModelCategories, ['prompt'=>'-- Выберите категорию --'] ) ?>

		<div class="form-group">
			<?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            <a href="<?= Url::to(['/manufacturer/delete','id'=>$oModel->id]) ?>" class="btn btn-danger">Удалить</a>
        </div>
	<?php ActiveForm::end(); ?>

    </div>

</div><!-- category-update -->

<div id="tree_category" class="col-xs-4">
    <?php foreach( $oModelCategories as $key=>$oCategoryRoot ): ?>
      <div>
        <h3><?= $key ?></h3>
          <ul>
              <?php foreach( $oCategoryRoot as $id=>$oCategory ): ?>
                  <li>
                      <?= $oCategory ?>
                      <?php if (isset($oModelList[$id]) && count($oModelList[$id])): ?>
                          <ul>
                              <?php foreach ($oModelList[$id] as $iManufacturerId => $oModel): ?>
                                  <li>
                                      <a href="<?= Url::to( ['/manufacturer/index', 'id' => $iManufacturerId] ); ?>"><?= $oModel ?></a>
                                  </li>
                              <?php endforeach ?>
                          </ul>
                      <?php endif; ?>
                  </li>
            <?php endforeach; ?>
          </ul>
        </div>
    <?php endforeach; ?>
</div>