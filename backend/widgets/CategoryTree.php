<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 15.02.14
 * Time: 0:42
 */

namespace backend\widgets;

use common\models\CategoryBase;
use yii\base\Widget;
use yii\helpers\Html;

class CategoryTree extends Widget {

    public function run()
    {

        parent::init();

        $oCategories = CategoryBase::find()->orderBy('root, lft')->all();

        $level = 0;

        foreach ($oCategories as $category)
        {
            if ($category->level == $level) {
                echo Html::endTag('li') . "\n";
            } elseif ($category->level > $level) {
                echo Html::beginTag('ul') . "\n";
            } else {
                echo Html::endTag('li') . "\n";

                for ($i = $level - $category->level; $i; $i--) {
                    echo Html::endTag('ul') . "\n";
                    echo Html::endTag('li') . "\n";
                }
            }

            echo Html::beginTag('li', ['id'=>'node_'.$category->id]);
            echo Html::encode($category->name);
            $level = $category->level;
        }

        for ($i = $level; $i; $i--) {
            echo Html::endTag('li') . "\n";
            echo Html::endTag('ul') . "\n";
        }


    }

} 