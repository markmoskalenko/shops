<?php

namespace backend\assets;

use yii\web\AssetBundle;

class _AttributesFormAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $js = ['js/script.attributes.form.js'];
	public $depends = [
      'yii\web\JqueryAsset',
    ];
}