<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

class CategoryAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
//	public $css = ['css/jstree/style.css'];
	public $js = ['js/script.category.js'];
	public $depends = [
		'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'backend\assets\jsTreeAsset',
    ];
}
