<?php

namespace backend\assets;

use yii\web\AssetBundle;

class AttributesGroupIndexAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $js = ['js/script.attributes-group.index.js'];
	public $depends = [
      'yii\web\JqueryAsset',
    ];
}