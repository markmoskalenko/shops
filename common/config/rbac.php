<?php
return array (
  'items' => 
  array (
    'retail' => 
    array (
      'type' => 1,
      'description' => NULL,
      'ruleName' => 'Retail',
      'data' => NULL,
    ),
    'administrator' => 
    array (
      'type' => 1,
      'description' => NULL,
      'ruleName' => 'Administrator',
      'data' => NULL,
      'children' => 
      array (
        0 => 'retail',
      ),
    ),
  ),
  'rules' => 
  array (
    'Retail' => 'O:22:"common\\rbac\\RetailRule":3:{s:4:"name";s:6:"Retail";s:9:"createdAt";N;s:9:"updatedAt";N;}',
    'Administrator' => 'O:29:"common\\rbac\\AdministratorRule":3:{s:4:"name";s:13:"Administrator";s:9:"createdAt";N;s:9:"updatedAt";N;}',
  ),
);