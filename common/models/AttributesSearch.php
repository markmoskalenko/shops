<?php

namespace common\models;

use yii\data\ActiveDataProvider;
use \common\filters\HtmlPurifier;
use yii\base\Model;


class AttributesSearch extends Attributes
{

	public function rules()
	{
		return [
            ['id', 'integer'],

            ['type', 'integer'],

            ['is_visible', 'integer'],

            ['title', 'string', 'max' => 255],
            ['title', 'filter', 'filter' => function ($value) {
                  return HtmlPurifier::escape( $value );
            }],

            ['description', 'string'],
            ['description', 'filter', 'filter' => function ($value) {
                  return HtmlPurifier::escape( $value );
            }],
		];
	}

    public function scenarios()
    {
        return Model::scenarios();
    }

	public function search($params)
	{
		$query = Attributes::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

        $query->andFilterWhere( [ 'id' => $this->id ]);

        $query->andFilterWhere( [ 'type' => $this->type ]);

        $query->andFilterWhere( [ 'is_visible' => $this->is_visible ]);

        $query->andFilterWhere( ['LIKE', 'title', $this->title ] );

        $query->andFilterWhere( ['LIKE', 'description', $this->description ] );

		return $dataProvider;
	}
}