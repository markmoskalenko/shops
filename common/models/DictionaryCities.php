<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_dictionary_cities".
 *
 * @property integer $id
 * @property integer $country_id
 * @property integer $region_id
 * @property string $name
 *
 * @property DictionaryCountries $country
 * @property DictionaryRegions $region
 * @property Shops[] $shops
 */
class DictionaryCities extends \yii\db\ActiveRecord
{
	public static function tableName()
	{
		return 'tbl_dictionary_cities';
	}

	public function rules()
	{
		return [
			['region_id', 'integer'],
			['region_id', 'required'],

			['country_id', 'integer'],
            ['region_id', 'required'],

            ['name', 'string', 'max' => 128]
		];
	}

	public function attributeLabels()
	{
		return [
          'id'         => 'ID',
          'country_id' => 'Страна',
          'region_id'  => 'Регион',
          'name'       => 'Город',
		];
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getCountry()
	{
		return $this->hasOne(DictionaryCountries::className(), ['id' => 'country_id']);
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getRegion()
	{
		return $this->hasOne(DictionaryRegion::className(), ['id' => 'region_id']);
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getShops()
	{
		return $this->hasMany(Shop::className(), ['city_id' => 'id']);
	}

    /**
     * Выборка городов по региону
     * @param $id
     *
     * @return array id=>name
     */
    public static function getMapByRegionId( $id ){

        $oModel = self::find()
          ->where( [ 'region_id' => (int)$id ] )
          ->asArray()
          ->all();

        return ArrayHelper::map( $oModel, 'id', 'name');
    }
}