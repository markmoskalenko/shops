<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_dictionary_countries".
 *
 * @property integer $id
 * @property string $name
 *
 * @property DictionaryCities[] $dictionaryCities
 * @property DictionaryRegions[] $dictionaryRegions
 */
class DictionaryCountries extends \yii\db\ActiveRecord
{
	public static function tableName()
	{
		return 'tbl_dictionary_countries';
	}

	public function rules()
	{
		return [
			['name', 'required'],
			['name', 'string', 'max' => 128]
		];
	}

	public function attributeLabels()
	{
        return [
          'id'   => 'ID',
          'name' => 'Страна',
        ];
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getDictionaryCities()
	{
		return $this->hasMany(DictionaryCities::className(), ['country_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getDictionaryRegions()
	{
		return $this->hasMany(DictionaryRegion::className(), ['country_id' => 'id']);
	}

    /**
     * Выборка всех городов
     * Возвращает массив key=>value
     *
     * @return array
     */
    public static function getMap(){

        $oModel = self::find()->asArray()->all();

        return ArrayHelper::map( $oModel, 'id', 'name' );
    }
}