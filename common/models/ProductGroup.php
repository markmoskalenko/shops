<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_product_group".
 *
 * @property integer $id
 * @property string  $name
 */
class ProductGroup extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'tbl_product_group';
    }

    public function rules()
    {
        return [
            ['category_id', 'required'],
            ['category_id', 'integer'],

            ['name', 'required'],
            ['name', 'string', 'max' => 255]
        ];
    }

    public function attributeLabels()
    {
        return [
          'id'          => 'ID',
          'name'        => 'Название',
          'category_id' => 'Категория',
        ];
    }

    /**
     * @return \yii\db\ActiveRelation
     */
    public function getAttributesGroup()
    {
        return $this->hasMany(AttributesGroup::className(), ['product_group_id' => 'id']);
    }

    /**
     * Возвращает массив key=>value
     * Нужнен для dropDown силектов
     *
     * @return array
     */
    public static function getMap(){

        $oModel = self::find()->asArray()->all();

        return ArrayHelper::map( $oModel, 'id', 'name' );
    }

    /**
     * Возвращает все группы товаров принадлежащие категории ID и ее руту.
     *
     * Нельзя запросить мануфактуры рутовской категории.
     *
     * @param $id - Категрия
     *
     * @return array
     */
    public static function getMapByCategoryId( $id ){

        $oCategory = CategoryCatalog::findOne( (int)$id );

        if( $oCategory === null || $oCategory->isRoot() ) return [];

        $oModel = self::find()->where( ['category_id'=>[ $oCategory->id, $oCategory->root ]] )->asArray()->all();

        return ArrayHelper::map( $oModel, 'id', 'name' );
    }

    /**
     * Проеверяем принадлежит ли атрибут группе товара.
     *
     * @param $iAttributeId - ID атрибута
     *
     * @return bool
     */
    public function isAttachAttribute( $iAttributeId ){

        $oModelsAttaches = $this->attributesGroup;

        foreach( $oModelsAttaches  as $oModel )
            if( $oModel->attribute_id == $iAttributeId ) return true;

        return false;
    }
}