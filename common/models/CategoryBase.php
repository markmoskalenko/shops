<?php

namespace common\models;

use common\components\behaviors\nestedSetBehavior\NestedSet;

/**
 * This is the model class for table "tbl_category".
 *
 * @property integer $id
 * @property integer $lft
 * @property integer $rgt
 * @property integer $level
 * @property integer $root
 * @property string  $url
 * @property string  $name
 */
class CategoryBase extends \yii\db\ActiveRecord
{
    public $root_id;

    public static function tableName()
    {
        return 'tbl_category';
    }

    public function rules()
    {
        return [

          ['url', 'string', 'max' => 255],
          ['url', 'unique'],
          ['url', 'required'],

          ['name', 'string', 'max' => 255],
          ['name', 'required'],

          ['root_id', 'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
          'url'     => 'Url',
          'name'    => 'Название',
          'root_id' => 'Родитель'
        ];
    }

    public function behaviors()
    {
        return [
          [
            'class' => NestedSet::className(),
          ],
          'slug' => [
            'class' => 'common\components\behaviors\Slug',
            'in_attribute' => 'name',
            'out_attribute' => 'url',
            'translit' => true
          ]
        ];
    }

    /**
     * Список категорий для селекта
     * @return array
     */
    public static function getTreeMap()
    {
        $oCategories  = self::find()->orderBy('lft, root')->all();

        $result       = array();

        $rootName = '';
        foreach ($oCategories as $c){
            if( !$c->isLeaf() ) {
                $rootName = $c->name;
                continue;
            }
            $result[$rootName][$c->id] = $c->name;
        }

        return $result;
    }

    /**
     * Список категорий для селекта с возможностью выделения рутов
     * @return array
     */
    public static function getTreeChildAndRootMap()
    {
        $oCategories  = self::find()->orderBy('root, lft')->all();

        $result       = array();

        foreach ($oCategories as $c)
            $result[$c->id] = str_repeat('-', $c->level - 1) . ' ' . $c->name;

        return $result;
    }

    /**
     * Сохраняем категорию
     * Если в параметрах есть id рута, то сохраняем как дочернего иначе это новый рут.
     *
     * @return bool
     */
    public function saveItem()
    {

        if (!empty($this->root_id) && $this->isNewRecord) {

            $oRootCategory = self::findOne((int) $this->root_id);

            if ($oRootCategory !== null) {

                return $this->appendTo($oRootCategory);

            } else {

                return false;

            }
        } else

            if (!empty($this->root_id) && !$this->isNewRecord) {

                $oTarget = self::findOne($this->root_id);

                if ($oTarget->id != $this->id) {

                    return $this->moveAsFirst($oTarget);

                }

            }

        if(empty($this->root_id) && !$this->isNewRecord) return $this->moveAsRoot();

        return $this->saveNode();
    }

    public function afterFind(){

        if( !$this->isRoot() )
            $this->root_id = $this->root;

        parent::afterFind();
    }

    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }
}