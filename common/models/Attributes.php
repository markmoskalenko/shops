<?php

namespace common\models;

use yii\helpers\ArrayHelper;

use \common\filters\HtmlPurifier;

/**
 * This is the model class for table "tbl_attributes".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $type
 * @property integer $is_visible
 * @property integer $is_multi
 *
 * @property AttributeGroup[] $attributeGroups
 * @property AttributesValue[] $attributesValues
 */
class Attributes extends \yii\db\ActiveRecord
{
    /**
     * Значения списка.
     * @var
     */
    public $value = [];

    const TYPE_BOOLEAN  = 101;
    const TYPE_LIST     = 102;
    const TYPE_INTEGER  = 103;

    const NOT_MULTI_LIST = 0;
    const MULTI_LIST     = 1;

    const PUBLISHED    = 1;
    const NO_PUBLISHED = 0;

    private static $typeLabels = [
        self::TYPE_BOOLEAN => 'Логический тип',
        self::TYPE_LIST    => 'Список',
        self::TYPE_INTEGER => 'Число',
      ];

    const TYPE_BOOLEAN_FALSE = 0;
    const TYPE_BOOLEAN_TRUE  = 1;

    private static $typeBooleanLabels = [
      self::TYPE_BOOLEAN_FALSE => 'Нет',
      self::TYPE_BOOLEAN_TRUE  => 'Да'
    ];

	public static function tableName()
	{
		return 'tbl_attributes';
	}

	public function rules()
	{
		return [
          ['title', 'required'],
          ['title', 'string', 'max' => 255],
          ['title', 'filter', 'filter' => function ($value) {
                return HtmlPurifier::escape( $value );
          }],

          ['type', 'required'],
          ['type', 'integer'],
          ['type', 'in', 'range' => [ self::TYPE_BOOLEAN, self::TYPE_INTEGER, self::TYPE_LIST ]],

          ['is_multi', 'integer'],
          ['is_multi', 'in', 'range' => [ self::NOT_MULTI_LIST, self::MULTI_LIST ]],


          ['description', 'string'],
          ['description', 'filter', 'filter' => function ($value) {
                return HtmlPurifier::escape( $value );
          }],

          ['is_visible', 'integer'],
          ['is_visible', 'in', 'range' => [ self::PUBLISHED, self::NO_PUBLISHED ]],

          ['value', 'safe'],
		];
	}

	public function attributeLabels()
	{
		return [
          'id'          => 'ID',
          'title'       => 'Название',
          'description' => 'Описание',
          'type'        => 'Тип',
          'is_visible'  => 'Показывать',
          'is_multi'    => 'Множественный выбор',
		];
	}

	/**
     * Группы товаров
     *
	 * @return \yii\db\ActiveRelation
	 */
	public function getAttributeGroups()
	{
		return $this->hasMany(AttributesGroup::className(), ['attribute_id' => 'id']);
	}

	/**
     * Значения параметров
     *
	 * @return \yii\db\ActiveRelation
	 */
	public function getAttributesValues()
	{
		return $this->hasMany(AttributesValue::className(), ['attribute_id' => 'id']);
	}

    /**
     * Список типов $code=>$label
     *
     * @return array
     */
    public function getMapTypes(){
        return static::$typeLabels;
    }

    /**
     * Название типа по коду
     *
     * @param $iCode - код типа
     *
     * @return string
     */
    public function getTitleByTypeCode( $iCode ){
        return isset( static::$typeLabels[ $iCode ] ) ? static::$typeLabels[ $iCode ] : '';
    }

    public static function getMapBooleanLabels(){
        return static::$typeBooleanLabels;
    }

    /**
     * Список значий для селекта
     *
     * @return array
     */
    public function getMapValues(){

        if( $this->type != self::TYPE_LIST ) return [];

        return AttributesValue::getMapByAttribute( $this->id );

    }

    /**
     * Возвращает массив - <id параметра> => <код типа>
     *
     * @param array $id
     *
     * @return array
     */
    public static function getTypeById( array $id ){

        if( !count( $id ) ) return [];

        $oDb = \Yii::$app->getDb();

        $sSql = "select id, `type` from `tbl_attributes` where id in (".implode(',',$id).")";

        $aResult = $oDb->createCommand($sSql)->queryAll();

        return count( $aResult ) ? ArrayHelper::map($aResult, 'id','type') : [];

    }

    /**
     * Минимальное значение числового параметра
     *
     * @param $id
     *
     * @return bool|string
     */
    public static function getMinValueNumerical( $id ){
        $oDb = \Yii::$app->getDb();

        $sSql = "select min(value_integer) from `tbl_attributes_value` where attribute_id = ".(int)$id;

        $aResult = $oDb->createCommand($sSql)->queryScalar();

        return $aResult;

    }

    /**
     * Максимильное значение числового параметра
     *
     * @param $id
     *
     * @return bool|string
     */
    public static function getMaxValueNumerical( $id ){
        $oDb = \Yii::$app->getDb();

        $sSql = "select max(value_integer) from `tbl_attributes_value` where attribute_id = ".(int)$id;

        $aResult = $oDb->createCommand($sSql)->queryScalar();

        return $aResult;

    }

    public function afterSave( $insert ){

        if( $this->type == self::TYPE_LIST ){

            AttributesValue::saveDefaultValue( $this->id, $this->value );
        }else{

            $this->is_multi = self::NOT_MULTI_LIST;
        }

        parent::afterSave( $insert );
    }
}