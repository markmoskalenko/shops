<?php
namespace common\models;

use yii\db\ActiveRecord;
use yii\helpers\Security;
use yii\web\IdentityInterface;

/**
 * Class User
 *
 * @package common\models
 *
 * @property integer $id
 * @property string  $username
 * @property string  $password_hash
 * @property string  $password_reset_token
 * @property string  $email
 * @property string  $auth_key
 * @property integer $role
 * @property integer $status
 * @property integer $create_time
 * @property integer $update_time
 */
class User extends ActiveRecord implements IdentityInterface
{
    /**
     * @var string the raw password. Used to collect password input and isn't saved in database
     */
    public $password;

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE  = 1;

    const ROLE_GUEST  = 0;
//    const ROLE_USER   = 1;
    const ROLE_RETAIL = 'retail';
    const ROLE_ADMIN  = 'administrator';


    public function behaviors()
    {
        return [
          'timestamp' => [
            'class'      => 'yii\behaviors\TimestampBehavior',
            'attributes' => [
              ActiveRecord::EVENT_BEFORE_INSERT => ['create_time', 'update_time'],
              ActiveRecord::EVENT_BEFORE_UPDATE => 'update_time',
            ],
          ],
        ];
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     *
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     *
     * @return null|User
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => static::STATUS_ACTIVE]);
    }

    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }


    public function getRole()
    {
        return $this->role;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShops()
    {
        return $this->hasMany(Shop::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveRelation
     */
    public function getCity()
    {
        return $this->hasOne(DictionaryCities::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveRelation
     */
    public function getCountry()
    {
        return $this->hasOne(DictionaryCountries::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveRelation
     */
    public function getRegion()
    {
        return $this->hasOne(DictionaryRegion::className(), ['id' => 'region_id']);
    }

    /**
     * @param string $authKey
     *
     * @return boolean if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @param string $password password to validate
     *
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Security::validatePassword($password, $this->password_hash);
    }

    public static function sendPasswordResetEmail( $email )
    {
        $oUser = self::find([
            'status'    => User::STATUS_ACTIVE,
            'email'     => $email
          ]);

        if ( $oUser === null ) return false;


        $oUser->password_reset_token = Security::generateRandomKey();

        if ( $oUser->save( false ) ) {
            return \Yii::$app->mail->compose('passwordResetToken', ['user' => $oUser])
              ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
              ->setTo($email)
              ->setSubject('Сброс паролья ' . \Yii::$app->name)
              ->send();
        }

        return false;
    }

    public function rules()
    {
        return [
          ['username', 'filter', 'filter' => 'trim'],
          ['username', 'required', 'message' => 'Поле имя обязательное для заполнения.'],
          ['username', 'string', 'min' => 2, 'max' => 30, 'message' => 'Поле имя может содержать от 2 до 30 символов.'],

          ['email', 'filter', 'filter' => 'trim'],
          ['email', 'required', 'message' => 'Поле почты обязательное для заполнения.'],
          ['email', 'email', 'message' => 'Введенная вами страка не является корректным почтовым адресом.'],
          ['email', 'unique', 'message' => 'Пользователь с таким email уже существует.', 'on' => 'signup'],
          ['email', 'exist', 'message' => 'Пользователь с такой почтой не найден.', 'on' => 'requestPasswordResetToken'],

          ['status', 'integer'],
          ['status', 'in', 'range' => [ self::STATUS_ACTIVE, self::STATUS_DELETED ]],

          ['avatar', 'filter', 'filter' => 'trim'],
          ['avatar', 'string', 'min' => 2, 'max' => 255],

          ['password', 'required', 'message' => 'Поле пароль обязательное для заполнения.'],
          ['password', 'string', 'min' => 6, 'message' => 'Пароль должен содержать больше 5 символов.'],

          ['role', 'required'],
          ['role', 'in', 'range' => [ self::ROLE_RETAIL ], 'on' => 'signup'],

          ['country_id', 'required'],
          ['country_id', 'integer'],

          ['region_id', 'required'],
          ['region_id', 'integer'],

          ['city_id', 'required'],
          ['city_id', 'integer'],

          ['phone','required'],
          ['phone', 'string', 'max'=>30],
          ['phone', 'filter', 'filter' => function ($value) {
                return \common\filters\HtmlPurifier::escape( $value );
            }],

          ['skype', 'string', 'max'=>30],
          ['skype', 'filter', 'filter' => function ($value) {
                return \common\filters\HtmlPurifier::escape( $value );
            }],
        ];
    }

    public function scenarios()
    {
        return [
          'signup'                    => ['username', 'email', 'password', 'role', 'country_id', 'region_id', 'city_id', 'phone'],
          'resetPassword'             => ['password'],
          'requestPasswordResetToken' => ['email'],
        ];
    }


    public function attributeLabels()
    {
        return [
          'username'   => 'Имя',
          'email'      => 'Почта',
          'password'   => 'Пароль',
          'role'       => 'Роль',
          'city_id'    => 'Город',
          'country_id' => 'Страна',
          'region_id'  => 'Регион',
          'phone'      => 'Контактный телефон',
          'skype'      => 'Skype',
        ];
    }

    public function beforeValidate(){

        if( $this->isNewRecord ){
            $this->status = self::STATUS_ACTIVE;

            if( $this->scenario == 'signup' )
                $this->role = self::ROLE_RETAIL;
        }

        return parent::beforeValidate();

    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (($this->isNewRecord || $this->getScenario() === 'resetPassword') && !empty($this->password)) {
                $this->password_hash = Security::generatePasswordHash($this->password);
            }
            if ($this->isNewRecord) {
                $this->auth_key = Security::generateRandomKey();
            }

            return true;
        }

        return false;
    }
}