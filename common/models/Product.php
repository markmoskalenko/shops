<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_product".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $shop_id
 * @property integer $manufacturer_id
 * @property integer $category_id
 * @property integer $product_group_id
 * @property string $url
 * @property string $price
// * @property string $price
 * @property integer $status
 * @property integer $created
 * @property integer $updated
 * @property integer $amount
 * @property integer $code
 * @property integer $user_id
 * @property integer $count
 *
 * @property CategoryCatalog $category
 * @property Manufacturer $manufacturer
 * @property ProductGroup $productGroup
 * @property Shop $shop
 */
class Product extends \yii\db\ActiveRecord
{
    const DEFAULT_PAGE_SIZE = 10;
    const DEFAULT_ORDER = 'title ASC';
    public $attributesEav = [];

    const STATUS_PUBLISHED = 1;
    const STATUS_NOT_PUBLISHED = 0;

    const STATUS_STOCK = 1;
    const STATUS_ON_ORDER = 2;
    const STATUS_NOT_AVAILABLE = 3;

    protected static $_STATUS_PUBLISHED_TITLE = [
        self::STATUS_PUBLISHED => 'Опубликовано',
        self::STATUS_PUBLISHED => 'Не опубликовано',
    ];

    protected static $_STATUS_TITLE = [
      self::STATUS_STOCK         => 'В наличии',
      self::STATUS_ON_ORDER      => 'Под заказ',
      self::STATUS_NOT_AVAILABLE => 'Нет в наличии',
    ];

    public static function getPublishedTitleByStatusCode( $iCode ){
        return static::$_STATUS_PUBLISHED_TITLE[ $iCode ];
    }

    public static function getTitleByStatusCode( $iCode ){
        return static::$_STATUS_TITLE[ $iCode ];
    }

    public static function getMapStatus(){
        return static::$_STATUS_TITLE;
    }

    public function getStatusTitle(){
        return self::$_STATUS_TITLE[ $this->status ];
    }

	public static function tableName()
	{
		return 'tbl_product';
	}

	public function rules()
	{
		return [
            ['shop_id','required'],
            ['shop_id','integer'],

            ['user_id','required'],
            ['user_id','integer'],

            ['manufacturer_id','required'],
            ['manufacturer_id','integer'],

            ['category_id','required'],
            ['category_id','integer'],

            ['product_group_id','required'],
            ['product_group_id','integer'],

            ['is_published','integer'],

            ['count','validationCount'],
            ['count','required'],
            ['count','integer'],

            ['status','required'],
            ['status','integer'],

            ['created','integer'],

            ['updated','integer'],

            ['code','integer'],

            ['description', 'string'],
            ['description', 'filter', 'filter' => function ($value) {
                  return nl2br( \common\filters\HtmlPurifier::escape( $value, 'br') );
            }],

            ['price','required'],
            ['price','number'],

            ['title','required'],
            ['title', 'filter', 'filter' => function ($value) {
                  return \common\filters\HtmlPurifier::escape( $value );
            }],

            ['url','required'],
            ['url', 'filter', 'filter' => function ($value) {
                  return \common\filters\HtmlPurifier::escape( $value );
            }],

            ['amount','integer'],

            ['attributesEav','safe']
		];
	}

	public function attributeLabels()
	{
		return [
          'id'               => 'ID',
          'title'            => 'Название',
          'description'      => 'Описание',
          'shop_id'          => 'Магазин',
          'manufacturer_id'  => 'Производитель',
          'category_id'      => 'Категория товара',
          'product_group_id' => 'Группа товара',
          'url'              => 'Url',
          'price'            => 'Стоимость, $',
          'status'           => 'Статус',
          'created'          => 'Дата создания',
          'updated'          => 'Дата обновления',
          'amount'           => 'Скидка, %',
          'code'             => 'Код товара',
          'count'            => 'Количество на складе (шт.)',
          'is_published'     => 'Опубликовать в каталоге товаров',
		];
	}

    public function behaviors()
    {
        return [
          'slug' => [
            'class'         => 'common\components\behaviors\Slug',
            'in_attribute'  => 'title',
            'out_attribute' => 'url',
            'translit'      => true
          ]
        ];
    }

    public function beforeSave( $insert )
    {
        if( $this->isNewRecord ){

            $this->created = time();
            $this->code    = time();
        }

        $this->updated = time();

        return parent::beforeSave( $insert );
    }



    public function validationCount($attribute) {
        if( $this->status == static::STATUS_STOCK && $this->$attribute <= 0 ){
            $this->addError($attribute, 'Укажите количество товара на складе. Количество должно быть больше нуля.');
        }elseif( $this->status != static::STATUS_STOCK && $this->$attribute > 0 ){
            $this->addError($attribute, 'Количество товара не может быть больше нуля, так как у Вас нет его в наличии.');
        }
    }

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getCategory()
	{
		return $this->hasOne(CategoryCatalog::className(), ['id' => 'category_id']);
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getManufacturer()
	{
		return $this->hasOne(Manufacturer::className(), ['id' => 'manufacturer_id']);
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getProductGroup()
	{
		return $this->hasOne(ProductGroup::className(), ['id' => 'product_group_id']);
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getShop()
	{
		return $this->hasOne(Shop::className(), ['id' => 'shop_id']);
	}

    /**
     * @return \yii\db\ActiveRelation
     */
    public function getImages()
    {
        return $this->hasMany(ProductImages::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveRelation
     */
    public function getMainImage()
    {
        return $this->hasOne(ProductImages::className(), ['product_id' => 'id'])->where(['is_main'=>1]);
    }

    /**
     * TODO: via
     * Атрибуты группы товара
     * @return mixed
     */
    public function getAttachAttributes(){
      return  $this->hasMany( Attributes::className(), ['id'=>'attribute_id'])
                   ->viaTable('tbl_attribute_group', ['product_group_id' => 'product_group_id']);
    }

    /**
     * Значения заполненных атрибутов.
     * @return mixed
     */
    public function getAttachAttributesValues(){
        return $this->hasMany( AttributesValue::className(), ['id'=>'attribute_value_id'])
          ->viaTable('tbl_attributes_relation', ['product_id' => 'id'])->asArray();
    }

    /**
     * Получить массив EAV элементов.
     * Массив содержит тип элемента, значение, если оно заполнено
     * Массив значений, если это список
     * Название атрибута
     * Данный метод мы вызываем из view файла.
     *
     * @return array
     */
    public function getEav(){

        $aResult = [];

        $oModelAttachAttributes = $this->attachAttributes;


        foreach( $oModelAttachAttributes as $oAttribute ){

            $aResult[$oAttribute->id]['title'] = $oAttribute->title;
            $aResult[$oAttribute->id]['type']  = $oAttribute->type;
            $aResult[$oAttribute->id]['name'] = 'Product[attributesEav]['.$oAttribute->id.']';

            if( $oAttribute->type == Attributes::TYPE_LIST ){
                $aResult[$oAttribute->id]['value'] = $this->getValueByAttribute( $oAttribute->id, Attributes::TYPE_LIST );
                $aResult[$oAttribute->id]['values']   = $oAttribute->getMapValues();
                $aResult[$oAttribute->id]['is_multi'] = $oAttribute->is_multi;

            }elseif( $oAttribute->type == Attributes::TYPE_BOOLEAN ){
                $aResult[$oAttribute->id]['value'] = $this->getValueByAttribute( $oAttribute->id, Attributes::TYPE_BOOLEAN );
                $aResult[$oAttribute->id]['values']   = Attributes::getMapBooleanLabels();

            }elseif( $oAttribute->type == Attributes::TYPE_INTEGER ){
                $aResult[$oAttribute->id]['value'] = $this->getValueByAttribute( $oAttribute->id, Attributes::TYPE_INTEGER );
            }

        }
        return $aResult;
    }

    public function getEavFront(){
        $aResult = [];

        $oModelAttachAttributes = $this->attachAttributes;

        foreach( $oModelAttachAttributes as $oAttribute ){

            $aResult[$oAttribute->id]['title'] = $oAttribute->title;
            $aResult[$oAttribute->id]['type']  = $oAttribute->type;

            if( $oAttribute->type == Attributes::TYPE_LIST ){
                $aResult[$oAttribute->id]['value'] = '';

                $aProductValue = $this->getValueByAttribute( $oAttribute->id, Attributes::TYPE_LIST );

                $aAttributeValues = $oAttribute->getMapValues();

                foreach( $aProductValue as $iIdValue ){

                    $aResult[$oAttribute->id]['value'][] = $aAttributeValues[ $iIdValue ];

                }

            }elseif( $oAttribute->type == Attributes::TYPE_BOOLEAN ){

                $aProductValue = $this->getValueByAttribute( $oAttribute->id, Attributes::TYPE_BOOLEAN ) ? 1 : 0;

                $aAttributeValues = Attributes::getMapBooleanLabels();

                $aResult[$oAttribute->id]['value'] = [ $aAttributeValues[$aProductValue] ];

            }elseif( $oAttribute->type == Attributes::TYPE_INTEGER ){

                $aResult[$oAttribute->id]['value'] = [ $this->getValueByAttribute( $oAttribute->id, Attributes::TYPE_INTEGER ) ];
            }

        }

        foreach( $aResult as $key=>$aResultItem){
            if( !is_array($aResultItem['value'])) unset($aResult[$key]);
        }
        return $aResult;

    }

    /**
     * Возвращает знвчение атрибута.
     * Результат ответа зависит от типа атрибута.
     * Если это select то мы передаем массив выделенных элементов.
     * Если число или логический, то отдаем поле value_integer
     * @param $iId - ID атрибута
     * @param $iType - Тип атрибута
     *
     * @return mixed
     */
    public function getValueByAttribute( $iId, $iType ){

        $oModelAttachAttributesValue = $this->attachAttributesValues;

        $r = [];
        foreach( $oModelAttachAttributesValue as $oModel ){
            if( $oModel['attribute_id'] == $iId ){
                if( $iType == Attributes::TYPE_LIST )
                    $r[] = $oModel['id'];

                if( $iType == Attributes::TYPE_BOOLEAN || $iType == Attributes::TYPE_INTEGER )
                    return (float)$oModel['value_integer'];
            }
        }
        return $iType == Attributes::TYPE_LIST ? $r : null;
    }

    public function afterSave( $insert ){

        AttributesValue::saveEav( $this->attributesEav, $this->id );

        parent::afterSave( $insert );
    }

    public function beforeDelete(){

        AttributesRelation::clearAllByProductAndAttribute( $this->id );

        return parent::beforeDelete();
    }



    public static function arrayToInt( $array ){
        foreach($array as $key=>$value) $array[$key] = (int)$value;
        return $array;
    }

    /**
     * Выборка товара по его адресу
     *
     * @param $url
     *
     * @return mixed
     */
    public static function getProductByUrl( $url ){

        return self::find()->where('url = :url', [':url' => $url])->one();
    }

    /**
     * Выборка товаров по категории с применением фильтра или без фильтра.
     *
     * @param null  $sCategoryUrl - Алиас категории каталога
     * @param array $aFilterParams - Если нет данных для фильтрации, то производим выборку по категории каталога
     * @param bool  $isCount - Если истина, то вернет количество найденых товаров
     * @return mixed
     */
    public static function getProductByFilter( $sCategoryUrl = null, $aFilterParams = [], $isCount = false, $iLimit = 15, $iOffset = 0, $order = 'title ASC' ){

        $oModel = self::find()
                    ->joinWith( 'category' )
                    ->orderBy((string)$order);


        if( $sCategoryUrl !== null )
            $oModel->andWhere([ 'tbl_category.url' => $sCategoryUrl ] );


        if( count( $aFilterParams ) ){

            static::processEAVFilter( $aFilterParams, $oModel);
        }

        if( !$isCount )
            $oModel
              ->offset( $iOffset )
              ->limit( $iLimit );

        return $isCount ? $oModel->count() : $oModel->all();
    }

    /**
     * Поиск товаров по параметрам фильтра
     *
     * @param $aFilterParams
     * @param $oProductModel
     *
     * @return bool
     */
    protected static function processEAVFilter( &$aFilterParams, &$oProductModel ){

        $oDb = \Yii::$app->getDb();

        $aProductIds = [];

        $aAttributeIds = [];

        foreach( $aFilterParams as $iId => $aParam ) $aAttributeIds[] = (int)$iId;

        $aAttributesType = Attributes::getTypeById( $aAttributeIds );

        static::clearEmptyIntegerEAVAttribute( $aFilterParams, $aAttributesType );

        if( !count( $aFilterParams ) ) return false;

        static::searchEAVNumericalValue( $aFilterParams, $aAttributesType );

        $iCountParams = count( $aFilterParams );

        if( $iCountParams ){

            $sSql = "SELECT * FROM (
                SELECT product_id, COUNT(product_id) AS amount FROM `tbl_attributes_relation`
                WHERE (";

            $iCounterCondition = 0;

            $iCountParams = count( $aFilterParams );

            foreach( $aFilterParams as $iId => $aParam ){

                $iId = (int)$iId;

                if( !isset( $aAttributesType[$iId] ) ) continue;

                $iCounterCondition ++;

                switch ( $aAttributesType[$iId] ){

                    case Attributes::TYPE_LIST :

                        $sValueIds = is_array( $aParam ) ? implode(',', static::arrayToInt($aParam)) : (int)$aParam;

                        $sSql .= "((`attribute_id`={$iId}) AND (`attribute_value_id` IN ({$sValueIds})))";

                        if( $iCounterCondition < $iCountParams ) $sSql .= "OR";

                        break;

                    case Attributes::TYPE_INTEGER :

                        $sSql .= "((`attribute_id`={$iId}) AND (`attribute_value_id` IN ({$aParam})))";

                        if( $iCounterCondition < $iCountParams ) $sSql .= "OR";

                        break;

                    case Attributes::TYPE_BOOLEAN :

                        $sSql .= "(`attribute_id`={$iId})";

                        if( $iCounterCondition < $iCountParams ) $sSql .= "OR";

                        break;
                }
            };

            $sSql .= ")
                    GROUP BY product_id
                    ) AS foo WHERE amount={$iCountParams}";


            $aResult = $oDb->createCommand($sSql)->queryAll();

            foreach( $aResult as $result )$aProductIds[] = $result['product_id'];

            $oProductModel->andWhere(['tbl_product.id'=>$aProductIds]);

        }else
            $oProductModel->andWhere('1=0'); // Заглушка - если на входе были параметры, но после обработки их не стело, это означает что либо подменены данные либо неудовлетворяет условие integer
    }

    /**
     * Поиск id значения атрибута по числовому значению
     * @param $aFilterParams - Входной массив параметров
     * @param $aAttributesType - Типы полученных параметров
     *
     * @return bool
     */
    protected static function searchEAVNumericalValue( &$aFilterParams, $aAttributesType ){

        $oDb = \Yii::$app->getDb();

        $sSqlFindIntegerValues = "SELECT id, attribute_id FROM tbl_attributes_value WHERE ";

        $aCondition = [];

        foreach( $aFilterParams as $iId => $aParam ){

            if( isset( $aAttributesType[$iId] ) &&  $aAttributesType[$iId] == Attributes::TYPE_INTEGER && isset($aParam['min']) && isset($aParam['max'])){
                if( empty($aParam['min']) && !empty($aParam['max']) )
                    $aCondition[] = "( value_integer <= ".(int)$aParam['max']." AND attribute_id = ".(int)$iId  .")";

                elseif( empty($aParam['max']) && !empty($aParam['min']) )
                    $aCondition[] = "( value_integer >= ".(int)$aParam['min']." AND attribute_id = ".(int)$iId  .")";

                elseif( !empty($aParam['max']) && !empty($aParam['min']) )
                    $aCondition[] = "( value_integer <= ".(int)$aParam['max']." AND value_integer >= ".(int)$aParam['min']." AND attribute_id = ".(int)$iId ." )";

                unset($aFilterParams[$iId]);
            }
        }

        if( count($aCondition) ){

            $sSqlFindIntegerValues .= implode(' OR ', $aCondition);

            $aResults = $oDb->createCommand($sSqlFindIntegerValues)->queryAll();

            if(count($aResults)){

                foreach( $aResults as $aResult ){
                    $aFilterParams[$aResult['attribute_id']] = $aResult['id'];
                }
            }

        }else return false;

        return true;
    }

    /**
     * Очистка массива параметров от пустышок, input передает  параметр даже, если там ничего не заполнино.
     * А если ничего не заполнино, то и не надо по нем фильтровать.
     * @param $aFilterParams - Входной массив параметров
     * @param $aAttributesType - Типы полученных параметров
     */
    protected static function clearEmptyIntegerEAVAttribute( &$aFilterParams, &$aAttributesType ){

        foreach( $aFilterParams as $iId => $aParam ){

            if( isset( $aAttributesType[$iId] ) &&  $aAttributesType[$iId] == Attributes::TYPE_INTEGER && isset($aParam['min']) && isset($aParam['max']) && empty($aParam['max']) && empty($aParam['min'])){

                unset($aFilterParams[$iId]);

            }
        }
    }
}