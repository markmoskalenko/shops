<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_dictionary_regions".
 *
 * @property integer $id
 * @property integer $country_id
 * @property string $name
 *
 * @property DictionaryCities[] $dictionaryCities
 * @property DictionaryCountries $country
 */
class DictionaryRegion extends \yii\db\ActiveRecord
{
	public static function tableName()
	{
		return 'tbl_dictionary_regions';
	}

	public function rules()
	{
		return [
			['country_id', 'integer'],
			['country_id', 'required'],

            ['name', 'required'],
            ['name', 'string', 'max' => 64]
		];
	}

	public function attributeLabels()
	{
		return [
          'id'         => 'ID',
          'country_id' => 'Страна',
          'name'       => 'Регион',
		];
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getDictionaryCities()
	{
		return $this->hasMany(DictionaryCities::className(), ['region_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getCountry()
	{
		return $this->hasOne(DictionaryCountries::className(), ['id' => 'country_id']);
	}

    /**
     * Возвращает все регионы относящиеся к городу $id
     *
     * @param $id
     *
     * @return array $id=>$name
     */
    public static function getMapByCountryId( $id ){

        $oModel = self::find()
                      ->where( [ 'country_id' => (int)$id ] )
                      ->asArray()
                      ->all();

        return ArrayHelper::map( $oModel, 'id', 'name');

    }
}