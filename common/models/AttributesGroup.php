<?php

namespace common\models;

/**
 * This is the model class for table "tbl_attribute_group".
 *
 * @property integer $id
 * @property integer $product_group_id
 * @property integer $attribute_id
 *
 * @property Attributes $attribute
 * @property ProductGroup $productGroup
 * @property AttributesRelation[] $attributesRelations
 */
class AttributesGroup extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'tbl_attribute_group';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
          ['product_group_id', 'required'],
          ['product_group_id', 'integer'],


          ['attribute_id', 'required'],
          ['attribute_id', 'integer'],
		];
	}

	/**
     * Атрибуты группы
     *
	 * @return \yii\db\ActiveRelation
	 */
	public function getAttributesEav()
	{
		return $this->hasOne(Attributes::className(), ['id' => 'attribute_id']);
	}

	/**
     * Группы товаров
     *
	 * @return \yii\db\ActiveRelation
	 */
	public function getProductGroup()
	{
		return $this->hasOne(ProductGroup::className(), ['id' => 'product_group_id']);
	}

    /**
     * Привязка атрибута к группе товаров
     *
     * @param $iProductGroupId - ID группы товара
     * @param $iAttributeId - ID Атрибута
     *
     * @return bool
     */
    public static function attach( $iProductGroupId, $iAttributeId ){

        $oModel = new self;

        $oModel->product_group_id = (int)$iProductGroupId;

        $oModel->attribute_id = (int)$iAttributeId;

        return $oModel->save();
    }

    /**
     * Отвязать параметр от группы товара
     *
     * @param $iProductGroupId - ID группы товара
     * @param $iAttributeId - ID Атрибута
     *
     * @return bool
     */
    public static function deAttach( $iProductGroupId, $iAttributeId ){

        $oModel = self::find()->where( [ 'product_group_id'=>(int)$iProductGroupId, 'attribute_id'=>(int)$iAttributeId ] )->one();

        if( $oModel ) return $oModel->delete();

        return false;
    }
}