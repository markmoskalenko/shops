<?php

namespace common\models;

/**
 * This is the model class for table "tbl_attributes_relation".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $attribute_id
 * @property integer $attribute_value_id
 *
 * @property AttributeGroup $attribute
 * @property Product $product
 * @property AttributesValue $attributeValue
 */
class AttributesRelation extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'tbl_attributes_relation';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['product_id', 'required'],
			['product_id', 'integer'],

            ['attribute_id', 'required'],
            ['attribute_id', 'integer'],

            ['attribute_value_id', 'required'],
            ['attribute_value_id', 'integer'],
		];
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
    public function getAttributeEav()
    {
        return $this->hasOne(Attributes::className(), ['id' => 'attribute_id']);
    }

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getProduct()
	{
		return $this->hasOne(Product::className(), ['id' => 'product_id']);
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getAttributeValue()
	{
		return $this->hasOne(AttributesValue::className(), ['id' => 'attribute_value_id']);
	}

    /**
     * Удалить все связи между значениями атрибутов и товарами по условию
     * @param $iProductId
     */
    public static function clearAllByProductAndAttribute( $iProductId ){

        $oModels = self::find()->where( [ 'product_id' => (int)$iProductId ] )->all();

        foreach( $oModels as $oModel ) $oModel->delete();
    }

    /**
     * Сохранение связи между значением атрибута и товаром
     *
     * @param $iProductId
     * @param $iAttributeId
     * @param $iAttributeValueId
     *
     * @return bool
     */
    public static function saveRelation( $iProductId, $iAttributeId, $iAttributeValueId ){

        $oModels = new self;

        $oModels->product_id = (int)$iProductId;

        $oModels->attribute_id = (int)$iAttributeId;

        $oModels->attribute_value_id = (int)$iAttributeValueId;

        return $oModels->save();
    }

    /**
     * Проверяем тип параметр.
     * Если это число или логическое, то нужно удалить и значение из таблици AttributesValue.
     */
    public function afterDelete(){

        $oAttribute = $this->attributeEav;

        if( $oAttribute->type == Attributes::TYPE_INTEGER || $oAttribute->type == Attributes::TYPE_BOOLEAN ){

            $oValue = AttributesValue::findOne( $this->attribute_value_id );

            if( $oValue !== null ) $oValue->delete();

        }
        return parent::afterDelete();
    }
}