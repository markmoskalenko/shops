<?php

namespace common\models;

use yii\db\ActiveQuery;

/**
 * This is the model class for table "tbl_category".
 *
 * @property integer $id
 * @property integer $lft
 * @property integer $rgt
 * @property integer $level
 * @property integer $root
 * @property string  $url
 * @property string  $name
 */
class CategoryCatalog extends CategoryBase
{
    const CATEGORY_ROOT_ID = 71;

    public static function find()
    {
        return parent::find()->where('tbl_category.root = :root AND tbl_category.id != :root', [ ':root' => self::CATEGORY_ROOT_ID ]);
    }

    public function getProductGroup(){
        return  $this->hasMany( ProductGroup::className(), ['category_id'=>'id']);
    }

    public function getAttributeGroup(){
        return  $this->hasMany( AttributesGroup::className(), ['product_group_id'=>'id'])
          ->via('productGroup');
    }

    public function getAttachAttributes(){
        return  $this->hasMany( Attributes::className(), ['id'=>'attribute_id'])
          ->via('attributeGroup');
    }

    /**
     * Получить массив EAV элементов.
     * Массив содержит тип элемента, значение, если оно заполнено
     * Массив значений, если это список
     * Название атрибута
     * Данный метод мы вызываем из view файла.
     *
     * @return array
     */
    public function getEav(){

        $aResult = [];

        $oModelAttachAttributes = $this->attachAttributes;

        foreach( $oModelAttachAttributes as $oAttribute ){

            $aResult[$oAttribute->id]['title'] = $oAttribute->title;
            $aResult[$oAttribute->id]['type']  = $oAttribute->type;
            $aResult[$oAttribute->id]['description']  = $oAttribute->description;
            $aResult[$oAttribute->id]['name'] = 'Product[attributesEav]['.$oAttribute->id.']';

            if( $oAttribute->type == Attributes::TYPE_LIST ){
                $aResult[$oAttribute->id]['values']   = $oAttribute->getMapValues();
                $aResult[$oAttribute->id]['is_multi'] = $oAttribute->is_multi;

            }elseif( $oAttribute->type == Attributes::TYPE_BOOLEAN ){
                $aResult[$oAttribute->id]['values']   = Attributes::getMapBooleanLabels();

            }elseif( $oAttribute->type == Attributes::TYPE_INTEGER ){
                $aResult[$oAttribute->id]['min'] = Attributes::getMinValueNumerical($oAttribute->id);
                $aResult[$oAttribute->id]['max'] = Attributes::getMaxValueNumerical($oAttribute->id);
            }

        }
        return $aResult;
    }
}