<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_manufacturer".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string  $name
 */
class Manufacturer extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'tbl_manufacturer';
    }

    public function rules()
    {
        return [
          ['category_id', 'required'],
          ['category_id', 'integer'],

          ['name', 'required'],
          ['name', 'string', 'max' => 255]
        ];
    }

    public function attributeLabels()
    {
        return [
          'id'          => 'ID',
          'category_id' => 'Категория',
          'name'        => 'Название',
        ];
    }

    /**
     * Возвращает массив key=>value
     * Нужнен для dropDown силектов
     *
     * @return array
     */
    public static function getMap(){

        $oModel = self::find()->asArray()->all();

        return ArrayHelper::map( $oModel, 'id', 'name' );
    }

    /**
     * Возвращает все мануфактуры принадлежащие категории ID и ее руту.
     *
     * Нельзя запросить мануфактуры рутовской категории.
     *
     * @param $id - Категрия
     *
     * @return array $id=>$name
     */
    public static function getMapByCategoryId( $id ){

        $oCategory = CategoryCatalog::findOne( (int)$id );

        if( $oCategory === null || $oCategory->isRoot() ) return [];

        $oModel = self::find()->where( ['category_id'=>[ $oCategory->id, $oCategory->root ]] )->asArray()->all();

        return ArrayHelper::map( $oModel, 'id', 'name' );
    }

    /**
     * Список мануфактур сгруппированных по категории
     *
     * @return array
     */
    public static function getMapGroupByCategory(){

        $oModel = self::find()->asArray()->all();

        return ArrayHelper::map( $oModel, 'id', 'name', 'category_id' );
    }
}