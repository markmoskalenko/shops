<?php

namespace common\models;
use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "tbl_product_images".
 *
 * @property integer $id
 * @property string $alias
 * @property string $description
 * @property integer $is_main
 * @property integer $product_id
 * @property integer $sort
 *
 * @property Product $product
 */
class ProductImages extends \yii\db\ActiveRecord
{
    /**
     * Абсолютный адрес до директории загрузки
     * @var string
     */
    public $sUploadDirectory;

    /**
     * url путь к папке с загруженными картинками
     * @var string
     */
    public static $sImageUrl = '/uploads';

    /**
     * Хранит файл до сохранения
     * @var
     */
    public $file;

    /**
     * Главное изображение товара
     */
    const MAIN_IMAGE = 1;

    /**
     * НЕ Главное изображение товара
     */
    const NOT_MAIN_IMAGE = 0;

    public function __construct(){

        $this->sUploadDirectory = Yii::getAlias('@frontend/web/uploads');
    }

	public static function tableName()
	{
		return 'tbl_product_images';
	}

	public function rules()
	{
		return [
            ['product_id','required'],
            ['product_id','integer'],

            ['alias','required'],
            ['alias', 'string', 'max' => 255],

            ['id','integer'],

            ['is_main','integer'],

            ['sort','integer'],

			['description', 'string', 'max' => 255],

            ['file','file','types'=> ['jpg','png']],
		];
	}

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['upload'] = ['file'];
        return $scenarios;
    }

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'alias' => 'Alias',
			'description' => 'Описание',
			'is_main' => 'Главное',
			'product_id' => 'Продукт',
			'sort' => 'Сортировка',
		];
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getProduct()
	{
		return $this->hasOne(Product::className(), ['id' => 'product_id']);
	}

    /**
     * Сохраняет изображения на сервер.
     * На данном этаме еще нету привязки к моделе, просто сохранение на серваке.
     * Привязка будет произведена при сохранении товара.
     *
     * @return bool - результат сохранения изображения
     */
    public function saveImg(){

        $this->file = UploadedFile::getInstance( $this, 'file' );

        $aSourcePath = pathinfo($this->file->name);

        $this->alias = md5(microtime().rand(1,10)) .'.'. $aSourcePath['extension'];

        return $this->validate() ? $this->file->saveAs($this->sUploadDirectory.DIRECTORY_SEPARATOR.$this->alias) : false;
    }

    /**
     * Сохранение изображений к продуктам.
     *
     * @param $sImages - список названий картинок через запятую ( fileName1.jpg, fileName2.png, ... )
     * @param $sMainImage - название главного изображения ( fileName1.jpg )
     * @param $iProductId - ID продукта
     *
     * @return bool
     */
    public static function attachImageByNames( $sImages, $sMainImage, $iProductId ){

        $aImages = explode(',', $sImages);

        if( !count( $aImages ) ) return false;

        foreach( $aImages as $sImage ){

            static::attachImage( $sImage, $iProductId );

        }

        static::cleaned( $aImages, $iProductId );

        static::setMainByName( $sMainImage, $iProductId );

        return true;

    }

    /**
     * Очистка удаленных картинок пользователем
     * Внимание! Очистка производит лишь удаление из БД. Файл остается на сервере.
     * @param $aImages
     * @param $iProduct
     */
    public static function cleaned( $aImages, $iProduct ){

        $oModels = self::find()
            ->where(['not in','alias', $aImages ])
            ->andWhere(['product_id'=>$iProduct])
            ->all();

        foreach ( $oModels as $oModel) {
            $oModel->delete();
        }
    }

    /**
     * Сохраняем изображения в бд с ключом товара
     *
     * @param $sImage - Название картинки
     * @param $iProductId - ID Товара
     *
     * @return bool - Результат сохранения
     */
    public static function attachImage( $sImage, $iProductId ){

        if( !static::imagesFileExist( $sImage ) || static::imageExist( $sImage, $iProductId )) return true;

        $oModel = new self;

        $oModel->alias = $sImage;

        $oModel->product_id = $iProductId;

        $oModel->is_main = self::NOT_MAIN_IMAGE;

        return ( $oModel->validate() && $oModel->save() ? true : false );
    }

    /**
     * Статическая функция для проверки на существование изображения к товару.

     * @param $sImage - название изображения
     * @param $iProductId - ID продукта
     *
     * @return boolean
     */
    public static function imageExist( $sImage, $iProductId ){

        return self::find()
          ->where([
              'alias'      => $sImage,
              'product_id' => $iProductId
            ])
          ->exists();
    }

    /**
     * Проверка на существование файла на сервере.
     *
     * @param $sImage - Название изображения
     *
     * @return bool
     */
    public static function imagesFileExist( $sImage ){

        $oModel = new self;

        if( !file_exists( $oModel->sUploadDirectory.DIRECTORY_SEPARATOR.$sImage  ) ) return false;

        return true;
    }

    /**
     * Установка главного изображения товара
     * @param $sImage
     * @param $iProductId
     *
     * @return mixed
     */
    public static function setMainByName( $sImage, $iProductId ){

        if( static::imagesFileExist( $sImage ) && static::imageExist( $sImage, $iProductId )){

            $oModel = self::findOne([
                'alias'      => $sImage,
                'product_id' => $iProductId
              ]);

        }else{

            $oModel = self::findOne([
                'product_id' => $iProductId
              ]);

        }

        if( $oModel !== null ) $oModel->setMain();

        return $oModel;
    }

    /**
     * Установить изображение как главное
     *
     * @return bool
     */
    public function setMain(){

        static::setAllNotMainByProductId( $this->product_id, $this->id );

        $this->is_main = self::MAIN_IMAGE;

        return $this->save();
    }

    /**
     * Установить флаг НЕ главное изображение у все картинок товара.
     *
     * @param $iProductId - ID товара
     *
     * @return bool
     */
    public static function setAllNotMainByProductId( $iProductId, $iId ){

        $oModels = self::find()
          ->where(['not in','id', (int)$iId ])
          ->andWhere(['product_id' => (int)$iProductId])
          ->all();

        foreach( $oModels as $oModel ){

            $oModel->is_main = self::NOT_MAIN_IMAGE;

            $oModel->save();

        }

        return true;
    }

    /**
     * УРЛ картинки текущей модели, принимает два параметра - высоту и ширину желаемого изображения
     * @param null $width
     * @param null $height
     *
     * @return string
     */
    public function getUrl( $width = null, $height = null ){

        return static::processUrl( $this->alias, $width, $height );
    }

    public static function processUrl( $alias, $width = null, $height = null ){

        $sImage = ( $width == null && $height == null ) ? $alias : preg_replace('/(.*)\.(jpg|jpeg|png|gif)/i', '\\1_'.$width.'x'.$height.'.\\2', $alias);

        return static::$sImageUrl.DIRECTORY_SEPARATOR.$sImage;
    }
}