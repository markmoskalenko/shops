<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_attributes_value".
 *
 * @property integer $id
 * @property integer $attribute_id
 * @property integer $value_integer
 * @property string $value_string
 *
 * @property AttributesRelation[] $attributesRelations
 * @property Attributes $attribute
 */
class AttributesValue extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'tbl_attributes_value';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['attribute_id',  'required'],
			['attribute_id',  'integer'],

			['value_integer', 'number'],

            ['value_string',  'string', 'max' => 255]
		];
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getAttributesRelations()
	{
		return $this->hasMany(AttributesRelation::className(), ['attribute_value_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getAttributesEav()
	{
		return $this->hasOne(Attributes::className(), ['id' => 'attribute_id']);
	}

    /**
     * Сохраняем дефолтные значения списка.
     * Так же тут удаляются значения которых нет в переменной $aValues.
     * И добавляются новые
     *
     * @param $iAttributeID - ID атирбута для значения
     *
     * @param $aValues - Список значений ( $ID => $Value )
     *
     * @return bool
     */
    public static function saveDefaultValue( $iAttributeID, $aValues ){

        $oModels = self::find()->where( [ 'attribute_id'=>$iAttributeID ] )->all();

        foreach( $oModels as $oModel ){

            if( !array_key_exists( $oModel->id, $aValues ) || empty($aValues[ $oModel->id ]) ) { $oModel->delete(); continue; }

            $oModel->value_string = $aValues[ $oModel->id ];

            $oModel->save();

            unset( $aValues[ $oModel->id ] );

        }

        foreach( $aValues as $aValue ){

            if( empty( $aValue ) ) continue;

            $oModel = new self;

            $oModel->value_string = $aValue;

            $oModel->attribute_id = (int)$iAttributeID;

            $oModel->save();
        }

        return true;
    }

    public static function saveEav( $aEav, $iProductId ){

        AttributesRelation::clearAllByProductAndAttribute( $iProductId );

        foreach( $aEav as $id=>$aItem ){

            $oModelAttribute = Attributes::findOne((int)$id);

            if( $oModelAttribute == null ) continue;

            if( $oModelAttribute->type == Attributes::TYPE_LIST ){

                if( is_array( $aItem ) && $oModelAttribute->is_multi ){

                    foreach( $aItem as $iValId ){

                        AttributesRelation::saveRelation( $iProductId, $oModelAttribute->id, $iValId );

                    }

                }elseif( !is_array( $aItem) && !$oModelAttribute->is_multi ){

                    AttributesRelation::saveRelation( $iProductId, $oModelAttribute->id, $aItem );

                }

            }elseif( ($oModelAttribute->type == Attributes::TYPE_INTEGER || $oModelAttribute->type == Attributes::TYPE_BOOLEAN) && !is_array( $aItem) ){

                $oModelValue = static::saveUserValue( $iProductId, $oModelAttribute->id, $aItem );

                if(  $oModelValue ){

                    AttributesRelation::saveRelation( $iProductId, $oModelAttribute->id, $oModelValue );

                }

            }
        }
    }

    /**
     * Получаем список значений для атрибута.
     * Как правило требуется для постороения дропдовна
     *
     * @param $iId - id атрибута
     *
     * @return array - $id => value_string
     */
    public static function getMapByAttribute( $iId ){

        $oModel = self::find()->where( [ 'attribute_id' => $iId ] )->all();

        return ArrayHelper::map( $oModel, 'id', 'value_string' );
    }

    /**
     * Сохранение значение числового атрибута
     * Это данные которые относятся только к данному товару и подлежат удалению вместе с товаром.
     * @param $iProductId
     * @param $iAttributeId
     * @param $sValue
     *
     * @return bool|int
     */
    public static function saveUserValue( $iProductId, $iAttributeId, $sValue ){

        $oModelRelationValue = AttributesRelation::find()->where([ 'attribute_id' => (int)$iAttributeId, 'product_id' => (int)$iProductId ] )->one();

        if( empty( $sValue ) ){
            if( $oModelRelationValue && $oModelRelationValue->attributeValue ){
                $oModelValue = $oModelRelationValue->attributeValue;
                $oModelValue->delete();
            }
            return false;
        }

        if( $oModelRelationValue !== null )
            $oModelValue = $oModelRelationValue->attributeValue;
        else
            $oModelValue = new self;

        $oModelValue->attribute_id  = (int)$iAttributeId;

        $oModelValue->value_integer = (float)$sValue;

        $oModelValue->save();

        return $oModelValue->id;
    }
}