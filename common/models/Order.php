<?php

namespace common\models;

use frontend\modules\profile\models\Shop;
use Yii;
use \common\filters\HtmlPurifier;

/**
 * This is the model class for table "tbl_order".
 *
 * @property string $id
 * @property string $shop_id
 * @property string $product_id
 * @property string $client_id
 * @property integer $status
 * @property string $retail_comment
 * @property string $date_created
 * @property string $client_comment
 * @property string $product_count
 * @property string $execution_date
 * @property string $expected_completion_date
 *
 * @property User $client
 * @property Product $product
 * @property Shops $shop
 */
class Order extends \yii\db\ActiveRecord
{
    const STATUS_NEW                = 101; // Новый
    const STATUS_VIEWS              = 102; // Просмотрено магазином
    const STATUS_DECLINED_SHOP      = 103; // Отклонено магазином
    const STATUS_DECLINED_CLIENT    = 104; // Отклонено клиентом
    const STATUS_CONFIRMED          = 105; // Подтверждено
    const STATUS_PAID               = 106; // Оплачено
    const STATUS_COMPLETED          = 107; // Завершенно

    protected static $_STATUS_TITLE = [
      self::STATUS_NEW             => 'Новый',
      self::STATUS_VIEWS           => 'Просмотренный исполнителем',
      self::STATUS_DECLINED_SHOP   => 'Отклонено магазином',
      self::STATUS_DECLINED_CLIENT => 'Отклонено клиентом',
      self::STATUS_CONFIRMED       => 'Подтверждено',
      self::STATUS_PAID            => 'Оплачено',
      self::STATUS_COMPLETED       => 'Завершенно',
    ];

    protected static $_STATUS_CLASS = [
      self::STATUS_NEW             => 'info',
      self::STATUS_VIEWS           => 'info',
      self::STATUS_DECLINED_SHOP   => 'danger',
      self::STATUS_DECLINED_CLIENT => 'danger',
      self::STATUS_CONFIRMED       => 'warning',
      self::STATUS_PAID            => 'success',
      self::STATUS_COMPLETED       => 'default',
    ];

    /**
     * Разрешено ли редактирование карточки заявки.
     * На возможность смены статуса не влияет
     *
     * @return bool
     */
    public function isReadOnly(){

        return !Shop::isByUser( $this->shop_id );
    }

    /**
     * Моя заявка
     * @return bool
     */
    public function isMyBid(){

        return $this->client_id == Yii::$app->user->id;
    }

    /**
     * Мне заявка
     * @return bool
     */
    public function isIBid(){

        return !$this->isMyBid();
    }

    public static function getMapStatus(){
        return static::$_STATUS_TITLE;
    }

    public function getStatusTitle(){
        return self::$_STATUS_TITLE[ $this->status ];
    }

    public function getStatusHtmlClass(){
        return self::$_STATUS_CLASS[ $this->status ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['shop_id', 'required'],
            ['shop_id', 'integer'],

            ['product_id', 'required'],
            ['product_id', 'integer'],

            ['client_id', 'required'],
            ['client_id', 'integer'],

            ['expected_completion_date', 'safe'],

            ['execution_date', 'integer'],

            ['product_count', 'integer'],

            ['date_created', 'integer'],

            ['status', 'integer'],

            ['retail_comment', 'string'],
            ['retail_comment', 'filter', 'filter' => function ($value) {
                  return HtmlPurifier::escape( $value );
            }],

            ['client_comment', 'string'],
            ['client_comment', 'filter', 'filter' => function ($value) {
                  return HtmlPurifier::escape( $value );
            }],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['update'] = ['expected_completion_date', 'retail_comment', 'execution_date', 'product_count'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
          'id'                       => 'ID',
          'shop_id'                  => 'Магазин',
          'product_id'               => 'Товар',
          'client_id'                => 'Клиент',
          'status'                   => 'Статус',
          'retail_comment'           => 'Комментарий продавца (видит только продавец)',
          'date_created'             => 'Дата поступления заявки',
          'client_comment'           => 'Пожелание клиента',
          'product_count'            => 'Количество заказанной продукции',
          'execution_date'           => 'Дата завершения',
          'expected_completion_date' => 'Ожидаемая дата выполнения заказа',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(User::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShop()
    {
        return $this->hasOne(\common\models\Shop::className(), ['id' => 'shop_id']);
    }

    /**
     * Создание заказа
     *
     * @param $productId - товар
     * @param $productCount - количество позиций
     * @param $shopId - магазин исполнитель
     * @param $clientComment - пожелание покупателя
     *
     * @return bool - сохранен ли в бд
     */
    public static function create( $productId, $productCount, $shopId, $clientComment = null ){

        if( !Product::find()
                ->where('id = :id AND shop_id = :shop_id', [ ':id'=>(int)$productId, ':shop_id' => (int)$shopId] )
                ->exists()
          ) return false;

        $productCount = (int)$productCount;

        $oModel = new Order();

        $oModel->shop_id       = (int) $shopId;
        $oModel->product_id    = (int) $productId;
        $oModel->client_id     = Yii::$app->user->id;
        $oModel->status        = self::STATUS_NEW;
        $oModel->client_comment = $clientComment;
        $oModel->product_count = $productCount > 0 ? $productCount : 1;

        return $oModel->save();
    }

    public function beforeValidate( ){

        if( !empty( $this->expected_completion_date ) && !is_integer( $this->expected_completion_date ) ){
            $this->expected_completion_date = strtotime( $this->expected_completion_date );
        }

        if( $this->isNewRecord ){

            $this->date_created = time();

        }
        return parent::beforeValidate();
    }

    // ********** УСТАНОВКА СТАТУСОВ ************ //

    /**
     * Установка статуса по коду
     *
     * @param $iStatusCode
     *
     * @return bool
     */
    public function setStatus( $iStatusCode ){

        switch ( $iStatusCode ) {
            case self::STATUS_VIEWS :
                return $this->setStatusView();

            case self::STATUS_DECLINED_SHOP :
                return $this->setStatusDeclinedShop();

            case self::STATUS_DECLINED_CLIENT :
                return $this->setStatusDeclinedClient();

            case self::STATUS_CONFIRMED :
                return $this->setStatusConfirmed();

            case self::STATUS_PAID :
                return $this->setStatusPaid();

            case self::STATUS_COMPLETED :
                return $this->setStatusCompleted();
        }

        return false;
    }

    /**
     * Установить статус - просмотрено исполнителем
     * @return bool
     */
    public function setStatusView(){

        if( $this->status == self::STATUS_NEW && $this->client_id != Yii::$app->user->id ){

            $this->status = self::STATUS_VIEWS;

            return $this->save();
        }
        return false;
    }

    /**
     * Установить статус - отклонено магазином
     * @return bool
     */

    public function setStatusDeclinedShop(){

        if( $this->client_id != Yii::$app->user->id ){

            $this->status = self::STATUS_DECLINED_SHOP;

            return $this->save();
        }

        return false;
    }

    /**
     * Установить статус - отклонено клиентом
     * @return bool
     */
    public function setStatusDeclinedClient(){

        if( $this->client_id == Yii::$app->user->id && $this->status !== self::STATUS_DECLINED_SHOP && $this->status !== self::STATUS_COMPLETED ){

            $this->status = self::STATUS_DECLINED_CLIENT;

            return $this->save();
        }

        return false;
    }

    /**
     * Установить статус - подтверждено
     * @return bool
     */
    public function setStatusConfirmed(){

        if( $this->status !== self::STATUS_DECLINED_CLIENT && $this->status !== self::STATUS_DECLINED_SHOP && $this->client_id != Yii::$app->user->id ){

            $this->status = self::STATUS_CONFIRMED;

            return $this->save();
        }
        return false;
    }

    /**
     * Установить статус - оплачено
     * @return bool
     */
    public function setStatusPaid(){

        if( $this->status === self::STATUS_CONFIRMED && $this->client_id != Yii::$app->user->id ){

            $this->status = self::STATUS_PAID;

            return $this->save();
        }
        return false;
    }

    /**
     * Установить статус - выполнено
     * @return bool
     */
    public function setStatusCompleted(){

        if( $this->status === self::STATUS_PAID && $this->client_id != Yii::$app->user->id ){

            $this->status = self::STATUS_COMPLETED;

            $this->execution_date = time();

            return $this->save();
        }

        return false;
    }

    // ********** КОНЕЦ УСТАНОВКА СТАТУСОВ ************ //
}