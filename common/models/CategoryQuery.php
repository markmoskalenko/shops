<?php

namespace common\models;

use yii\db\ActiveQuery;

class CategoryQuery extends ActiveQuery
{
    public function byUrl( $url )
    {
        $this->andWhere(['url' => $url]);

        return $this;
    }
}