<?php

namespace common\models;

use yii\helpers\ArrayHelper;
use common\filters\HtmlPurifier;

/**
 * This is the model class for table "tbl_shops".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $status
 * @property integer $create_time
 * @property string $alias
 * @property integer $city_id
 * @property integer $country_id
 * @property integer $region_id
 * @property string $street
 * @property integer $house
 * @property integer $corps
 * @property integer $cabinet
 * @property string $phone
 * @property string $skype
 * @property string $fax
 * @property string $email
 * @property string $rating
 * @property integer $user_id
 *
 * @property Product[] $products
 * @property DictionaryCities $city
 * @property User $user
 */
class Shop extends \yii\db\ActiveRecord
{
    const STATUS_PUBLISHED = 1;

    const STATUS_NO_PUBLISHED = 0;

    protected static $_aStatusTitle = [
        self::STATUS_PUBLISHED => 'Опубликовано',
        self::STATUS_NO_PUBLISHED => 'Не опубликовано',
    ];

    /**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'tbl_shops';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
            ['country_id','required'],
            ['country_id','integer'],

            ['region_id','required'],
            ['region_id','integer'],

            ['title','required'],
            ['title', 'string', 'max'=>255],
            ['title', 'filter', 'filter' => function ($value) {
                  return HtmlPurifier::escape( $value );
            }],

            ['description','required'],
            ['description', 'string'],
            ['description', 'filter', 'filter' => function ($value) {
                  return nl2br( HtmlPurifier::escape( $value, 'br') );
            }],

            ['status','required'],
            ['status','integer'],

            ['create_time','required'],
            ['create_time','integer'],

            ['alias','required'],

            ['city_id','required'],
            ['city_id','integer'],

            ['phone','required'],
            ['phone', 'string', 'max'=>30],
            ['phone', 'filter', 'filter' => function ($value) {
                  return HtmlPurifier::escape( $value );
            }],

            ['email','required'],
            ['email', 'string', 'max'=>30],
            ['email','email'],

            ['user_id','required'],
            ['user_id','integer'],

            ['skype', 'string', 'max'=>30],
            ['skype', 'filter', 'filter' => function ($value) {
                  return HtmlPurifier::escape( $value );
            }],

            ['fax', 'string', 'max'=>30],
            ['fax', 'filter', 'filter' => function ($value) {
                  return HtmlPurifier::escape( $value );
            }],

            ['street', 'string', 'max'=>30],
            ['street', 'filter', 'filter' => function ($value) {
                  return HtmlPurifier::escape( $value );
            }],

            ['house','integer'],

            ['cabinet','integer'],

            ['user_id','integer'],
            ['corps','integer'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
          'id'          => 'ID',
          'title'       => 'Название магазина',
          'description' => 'Описание',
          'status'      => 'Статус',
          'create_time' => 'Дата создания',
          'alias'       => 'url',
          'city_id'     => 'Город',
          'country_id'  => 'Страна',
          'region_id'   => 'Регион',
          'street'      => 'Улица',
          'house'       => 'Дом',
          'corps'       => 'Корпус',
          'cabinet'     => 'Офис',
          'phone'       => 'Контактный телефон',
          'skype'       => 'Skype',
          'fax'         => 'Факс',
          'email'       => 'Email',
          'rating'      => 'Рейтинг',
          'user_id'     => 'Пользователь',
		];
	}

    public function behaviors()
    {
        return [
          'slug' => [
            'class' => 'common\components\behaviors\Slug',
            'in_attribute' => 'title',
            'out_attribute' => 'alias',
            'translit' => true
          ]
        ];
    }

    public function beforeValidate()
    {

        if( $this->isNewRecord ){

            $this->create_time = time();

            $this->rating = 0.0;

        }

        return parent::beforeValidate();
    }

    /**
     * Название статус текущей модели
     * @return string
     */
    public function getStatusTitle(){
        return static::$_aStatusTitle[$this->status];
    }

    /**
     * Массив статусов key=>title
     * @return array
     */
    public static function getAllStatusTitle(){
        return static::$_aStatusTitle;
    }

    /**
     * Возвращает название статуса по его коду
     *
     * @param $iCode - Код статуса
     *
     * @return string
     */
    public static function getStatusTitleByStatusCode( $iCode ){
        return isset(static::$_aStatusTitle[$iCode]) ? static::$_aStatusTitle[$iCode] : '';
    }

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getProducts()
	{
		return $this->hasMany(Product::className(), ['shop_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getCity()
	{
		return $this->hasOne(DictionaryCities::className(), ['id' => 'city_id']);
	}

    /**
     * @return \yii\db\ActiveRelation
     */
    public function getCountry()
    {
        return $this->hasOne(DictionaryCountries::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveRelation
     */
    public function getRegion()
    {
        return $this->hasOne(DictionaryRegion::className(), ['id' => 'region_id']);
    }

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getUser()
	{
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

    /**
     * Возвращает массив key=>value
     * Нужнен для dropDown силектов
     *
     * @return array
     */
    public static function getMap(){

        $oModel = self::find()->asArray()->all();

        return ArrayHelper::map( $oModel, 'id', 'title' );
    }
}