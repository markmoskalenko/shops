<?php
namespace common\components;

use Yii;
use yii\rbac\Role;

class PhpManager extends \yii\rbac\PhpManager
{
    public function init()
    {
        $this->authFile=Yii::getAlias('@common/config/rbac').'.php';

        parent::init();
        if (!Yii::$app->user->isGuest) {
            $user = Yii::$app->getUser();
                $identity = $user->getIdentity();
            if (!$this->getAssignment($user->identity->role, $identity->getId())) {
                $role = new Role([
                  'name' => $user->identity->role
                ]);
                $this->assign($role, $identity->getId());
            }
        }
    }
}