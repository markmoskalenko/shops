
<?php
use frontend\modules\profile\models\Order;
use frontend\assets\ProfileAsset;
use frontend\assets\FancyboxAsset;
use frontend\widgets\jGrowl\JGrowl;

use \yii\helpers\Url;
use \yii\widgets\Breadcrumbs;

ProfileAsset::register($this);
FancyboxAsset::register($this);

JGrowl::widget();

$this->beginPage();

?>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,700&subset=latin,cyrillic" rel="stylesheet" type="text/css">

    <?php $this->head() ?>
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
</head>
<body id="body">
    <?php $this->beginBody() ?>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div style="border: 5px solid #000; margin-left: -20px; margin-right: -20px"></div>
                    <div class="indent-bottom"></div>
                    <div class="row">
                        <div class="col-xs-12 content">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="column-left">
                                        <div class="list-group">
                                            <a href="<?= Url::to('/'); ?>" class="list-group-item">Вернуться на сайт</a>
                                            <a href="<?= Url::to('/profile/shop'); ?>" class="list-group-item active">Магазины</a>
                                            <a href="<?= Url::to('/profile/shop'); ?>" class="list-group-item padding">Список филиалов</a>
                                            <a href="<?= Url::to('/profile/product'); ?>" class="list-group-item padding">Товары</a>
                                            <a href="<?= Url::to('/profile/order'); ?>" class="list-group-item padding">Входящие заявки <span class="label label-info"><?= Order::getCountNewByShop(); ?></span></a>
                                            <a href="<?= Url::to('/profile/order/my'); ?>" class="list-group-item">Мои заявки <span class="label label-info"><?= Order::getCountNewByUser(); ?></span></a>
                                            <!--<a href="" class="list-group-item padding">Импорт / Экспорт</a>-->
                                            <a href="" class="list-group-item">Настройки</a>
<!--<a href="" class="list-group-item">Продвижение</a>-->
<!--<a href="" class="list-group-item">Подать объявление</a>-->
<!--<a href="" class="list-group-item">Написать статью</a>-->
<!--<a href="" class="list-group-item">Заказть рекламу</a>-->
                                            <a href="" class="list-group-item">Платформа</a>
                                        </div>
                                    </div>
                                    <div class="column-center">
                                        <div class="wrapper">
                                            <?php
                                                echo Breadcrumbs::widget([
                                                    'links' => $this->aBreadcrumbs,
                                                    'options'=>['class'=>'breadcrumb-custom']
                                                  ]);
                                            ?>
                                        </div>
                                        <div class="indent-bottom"></div>
                                        <div class="wrapper"  style="padding: 0 10px;">
                                            <?= $content; ?>
                                        </div>
                                        <div class="indent-bottom"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>