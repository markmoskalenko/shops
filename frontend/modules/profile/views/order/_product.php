<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\Order $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="order-form">
    <h2>#<?= $model->product->code ?> <?= $model->product->manufacturer->name ?> <?= $model->product->title ?></h2>
    <div class="images">
        <?php foreach( $model->product->images as $oImage): ?>
            <img src="<?= $oImage->getUrl(200,200) ?>" alt=""/>
        <?php endforeach ?>
    </div>

    <h2>Описание</h2>

    <div class="description">
        <?= $model->product->description ?>
    </div>

    <h2>Параметры</h2>

    <div class="eav">
        <table class="table table-bordered">
            <?php foreach(  $model->product->getEavFront() as $aAttribute ): ?>
                <tr>
                    <td><?=$aAttribute['title'] ?></td>
                    <td><?=implode(',', $aAttribute['value']) ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>

    <a class="edit-product" href="<?= \yii\helpers\Url::to(['/profile/product/update', 'id'=>$model->product->id]); ?>">Перейти к редактированию товара</a>
</div>

<style type="text/css">
    .order-form img{
        border: 1px solid #dddddd;
        padding: 3px;
        margin: 0 5px 5px 0;
    }

    .order-form .edit-product{
        margin-bottom: 20px;
        display: block;
    }
</style>