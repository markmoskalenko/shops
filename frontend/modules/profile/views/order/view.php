<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var common\models\Order $model
 */

$this->title = 'Заявка №'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Перейти к карточке заявки', ['update', 'id' => $model->id], ['class' => 'btn btn-success right']) ?>
        <div class="clear"></div>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
          [
            'attribute' => 'shop_id',
            'value'     => $model->shop->title,
          ],
          [
            'attribute' => 'product_id',
            'value'     => $model->product->title,
          ],
          'product_count',
          [
            'attribute' => 'status',
            'value'     => $model->getStatusTitle(),
          ],
          [
            'attribute' => 'client_id',
            'value'     => $model->client->username,
          ],
          [
            'attribute' => 'retail_comment',
            'format'    => 'ntext',
            'visible'   => $model->isIBid()

          ],
          'client_comment:ntext',
          'date_created:datetime',
          'expected_completion_date:datetime',
          'execution_date:datetime'
        ],
    ]) ?>
</div>