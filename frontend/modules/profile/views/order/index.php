<?php

use yii\helpers\Html;
use yii\grid\GridView;

\frontend\assets\_ProfileOrderIndexAsset::register($this);

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\models\OrderSearch $searchModel
 */

$this->title = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
              'attribute' => 'shop_id',
              'filter' => \frontend\modules\profile\models\Shop::getMap(),
              'value'=>function( $value ){ return $value->shop->title;},
            ],
            'client.username',
            'product.title',
            [
              'attribute' => 'status',
              'filter' => \common\models\Order::getMapStatus(),
              'format'=>'html',
              'value'=>function( $value ){ return '<span class="label label-'.$value->getStatusHtmlClass().'">'.$value->statusTitle.'</span>';},
            ],
             [
                 'attribute' => 'date_created',
                 'format' => 'datetime',
                 'options'=> ['style'=>'width:220px'],

                 'filter' =>
                   yii\jui\DatePicker::widget([
                        'language' => 'ru',
                        'value'=> ( !empty( $searchModel->date_created_start ) ) ? date('Y-m-d',$searchModel->date_created_start) : null,
                        'name' => 'OrderSearch[date_created_start]',
                        'clientOptions' => [
                            'dateFormat' => 'yy-mm-dd',
                     ],
                        'options'=>[
                          'class'=>'form-control',
                          'style'=>'width : 100px; display : inline'

                        ]
                   ]).' '.
                   yii\jui\DatePicker::widget([
                       'language' => 'ru',
                       'value'=> ( !empty( $searchModel->date_created_end ) ) ? date('Y-m-d',$searchModel->date_created_end) : null,
                       'name' => 'OrderSearch[date_created_end]',
                       'clientOptions' => [
                         'dateFormat' => 'yy-mm-dd',
                       ],
                       'options'=>[
                         'class'=>'form-control',
                         'style'=>'width : 100px; display : inline'

                       ]
                     ])
                 ,
             ],
            [
              'class' => 'yii\grid\ActionColumn',
              'template' => '{view} {update}'
            ],
        ],
    ]); ?>

</div>
