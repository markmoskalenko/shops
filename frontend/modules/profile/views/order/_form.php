<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Order;

/**
 * @var yii\web\View $this
 * @var common\models\Order $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>

    <h4>Товар</h4>

    <table style="margin-top:10px" class="table table-striped table-bordered">
        <thead>
            <tr>
                <td></td>
                <td>Код товара</td>
                <td>Мануфактура</td>
                <td>Модель</td>
                <td>Стоимость</td>
                <td>Скидка %</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="width: 70px"><img class="product-image" src="<?= $model->product->mainImage ? $model->product->mainImage->getUrl(50,50) : '' ?>" alt=""/></td>
                <td><?= $model->product->code ?> </td>
                <td><?= $model->product->manufacturer->name ?> </td>
                <td><?= $model->product->title ?></td>
                <td><?= $model->product->price ?></td>
                <td><?= $model->product->amount ?></td>
            </tr>
        </tbody>
    </table>

    <h4>Пожелание клиента</h4>

    <?php if( !empty( $model->client_comment ) ): ?>
        <?= $model->client_comment ?>
    <?php else: ?>
        <i>Нет пожеланий..</i>
    <?php endif ?>

    <?= $form->field($model, 'status', ['template'=>"{input}"])->hiddenInput(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'product_count')->textInput(['maxlength' => 10, 'readonly'=>$model->isReadOnly() ]) ?>
        </div>

        <div class="col-md-6">
            <?php if( !$model->isReadOnly() ): ?>
                <?= $form->field($model, 'expected_completion_date', [ 'class' => 'common\components\ActiveField' ])->datepickerInput([
                          'clientOptions'=>[
                            'format' => 'dd-mm-yyyy',
                          ],
                          'value'=> ( !empty( $model->expected_completion_date ) ? date('d-m-Y', $model->expected_completion_date) : '' ),
                ]);?>
            <?php else: ?>
                <?= $form->field($model, 'expected_completion_date')->textInput(['value' => ( !empty( $model->expected_completion_date ) ? date('d-m-Y', $model->expected_completion_date) : '' ), 'readonly'=>$model->isReadOnly() ]) ?>

            <?php endif; ?>
        </div>
    </div>

    <?php if( $model->isIBid() ): ?>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'retail_comment')->textarea([ 'readonly'=>$model->isReadOnly() ]) ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="form-group">
        <?php if( $model->isIBid() ): ?>

            <?php if( $model->status == Order::STATUS_VIEWS ): ?>
                <?= Html::submitButton('Принять в работу', ['class' => 'btn btn-success set-status', 'data-status'=>Order::STATUS_CONFIRMED]) ?>
                <?= Html::submitButton('Откланить', ['class' => 'btn btn-danger set-status', 'data-status'=>Order::STATUS_DECLINED_SHOP]) ?>
            <?php elseif( $model->status == Order::STATUS_CONFIRMED ): ?>
                <?= Html::submitButton('Оплачен', ['class' => 'btn btn-primary set-status', 'data-status'=>Order::STATUS_PAID]) ?>
            <?php elseif( $model->status == Order::STATUS_PAID  ): ?>
                <?= Html::submitButton('Завершить', ['class' => 'btn btn-info set-status', 'data-status'=>Order::STATUS_COMPLETED]) ?>
            <?php endif; ?>

        <?php elseif( $model->isMyBid() && $model->status !== Order::STATUS_DECLINED_SHOP && $model->status !== Order::STATUS_COMPLETED && $model->status !== Order::STATUS_DECLINED_CLIENT ): ?>
            <?= Html::submitButton('Откланить', ['class' => 'btn btn-danger set-status', 'data-status'=>Order::STATUS_DECLINED_CLIENT]) ?>
        <?php endif; ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>