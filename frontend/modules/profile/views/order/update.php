<?php

use yii\helpers\Html;

\frontend\assets\_ProfileOrderUpdateAsset::register($this);

/**
 * @var yii\web\View $this
 * @var common\models\Order $model
 */

$this->title = 'Обработка заявки: №' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="order-update">
    <h1><?= Html::encode($this->title) ?> <span class="label-status label label-<?=$model->getStatusHtmlClass(); ?>">Статус: <?= $model->statusTitle; ?></span> <span class="label-created label label-success">Дата поступления заявки: <?= date( 'd-m-Y h:m', $model->date_created ) ?></span></h1>

    <?= yii\bootstrap\Tabs::widget([
        'items' => [
              [
                'label'   => 'Общее',
                'content' => $this->render('_form', [ 'model' => $model ]),
                'active'  => true
              ],
              [
                'label'   => 'Товар',
                'content' => $this->render('_product', [ 'model' => $model ]),
              ],
              [
                'label'   => 'Клиент',
                'content' => $this->render('_client', [ 'model' => $model ]),
              ],
        ],
      ]);
    ?>
</div>