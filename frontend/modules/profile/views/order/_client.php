<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\Order $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="order-form">
    <h2>Контактная информация</h2>

        <table class="table table-bordered">
            <tr>
                <td>Имя</td>
                <td><?= $model->client->username; ?></td>
            </tr>

            <tr>
                <td>Электронная почта</td>
                <td><?= $model->client->email; ?></td>
            </tr>

            <tr>
                <td>Страна</td>
                <td><?= $model->client->country->name;?></td>
            </tr>

            <tr>
                <td>Регион</td>
                <td><?= $model->client->region->name;?></td>
            </tr>

            <tr>
                <td>Город</td>
                <td><?= $model->client->city->name;?></td>
            </tr>



            <tr>
                <td>Контактный телефон</td>
                <td><?= $model->client->phone;?></td>
            </tr>

            <tr>
                <td>Skype</td>
                <td><?= $model->client->skype;?></td>
            </tr>
        </table>
    </div>