<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\models\ShopSearch $searchModel
 */

$this->title = 'Мои магазины';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-index">

	<h1><?= Html::encode($this->title) ?></h1>
    <p class="alert alert-info">
        Наша система построена таким образом, что один человек может управлять несколькими магазинами из одного аккаунта.<br>
        Каждый магазин должен соответствовать региону, в котором он находится. Это нужно, чтобы Вас находил именно целевой покупатель, тот который находится ближе всего к Вам.
        <br>
        <br>
        <strong>Внимание!</strong>
        <br>
        Не надо создавать несколько магазинов, если Вы торгуете только в одном регионе. Напоминаем, один регион - один магазин.
    </p>

	<p>
		<?= Html::a('Создать магазин!', ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?php echo GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
        'emptyText'=>'Магазинов не найдено',
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
            [
              'attribute' => 'title',
              'options'=>['style'=>'width: 170px']

            ],
			'description:html',
            [
              'attribute' => 'create_time',
              'format'=>'datetime',
              'options'=>['style'=>'width: 170px']

            ],
            'rating',
             [
               'attribute' => 'status',
               'value'=>function( $value ){ return $value->getStatusTitle(); },
             ],
			[
              'class' => 'yii\grid\ActionColumn',
             'options'=>['style'=>'width: 70px']
            ],
		],
	]); ?>

</div>