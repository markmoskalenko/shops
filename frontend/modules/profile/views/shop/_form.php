<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\assets\_ProfileShopUpdateAsset;

_ProfileShopUpdateAsset::register($this);

/**
 * @var yii\web\View $this
 * @var common\models\Shop $oModelShop
 * @var yii\widgets\ActiveForm $form
 * @var array $aCities
 * @var array $aRegions
 * @var array $aCounties
 */
?>

<div class="shop-form">

	<?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-xs-6"><?= $form->field($oModelShop, 'title')->textInput(['maxlength' => 255]) ?></div>
            <div class="col-xs-6"><?= $form->field($oModelShop, 'email')->textInput(['maxlength' => 255]) ?></div>
        </div>

        <div class="row">
            <div class="col-xs-6"><?= $form->field($oModelShop, 'phone')->textInput(['maxlength' => 255]) ?></div>
            <div class="col-xs-6"><?= $form->field($oModelShop, 'skype')->textInput(['maxlength' => 255]) ?></div>
        </div>

        <div class="row">
            <div class="col-xs-6"><?= $form->field($oModelShop, 'fax')->textInput(['maxlength' => 255]) ?></div>
        </div>

        <div class="row">
            <div class="col-xs-6"><?= $form->field($oModelShop, 'country_id')->dropDownList( $aCounties, ['prompt'=>'-- Выберите страну --'] ) ?></div>
            <div class="col-xs-6"><?= $form->field($oModelShop, 'region_id' )->dropDownList( $aRegions, ['prompt'=>'-- Выберите регион --'] ) ?></div>
        </div>

        <div class="row">
            <div class="col-xs-6"><?= $form->field($oModelShop, 'city_id')->dropDownList( $aCities, ['prompt'=>'-- Выберите город --'] ) ?></div>
        </div>

        <div class="row">
            <div class="col-xs-6"><?= $form->field($oModelShop, 'street')->textInput(['maxlength' => 255]) ?></div>
            <div class="col-xs-6"><?= $form->field($oModelShop, 'house')->textInput() ?></div>
        </div>

        <div class="row">
            <div class="col-xs-6"><?= $form->field($oModelShop, 'corps')->textInput() ?></div>
            <div class="col-xs-6"><?= $form->field($oModelShop, 'cabinet')->textInput() ?></div>
        </div>

		<?= $form->field($oModelShop, 'description')->textarea(['rows' => 6]) ?>

        <?= $form->field($oModelShop, 'status')->dropDownList( $oModelShop::getAllStatusTitle() )?>

		<div class="form-group">
			<?= Html::submitButton('Готово!', [ 'class' => 'btn btn-success' ]) ?>
		</div>

	<?php ActiveForm::end(); ?>
</div>