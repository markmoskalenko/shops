<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
/**
 * @var yii\web\View $this
 * @var common\models\Shop $model
 */

$this->title = $model->title;
?>
<div class="shop-view">

	<h1>Магазин - "<?= $this->title ?>"</h1>

	<p>
		<?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
	</p>

	<?php echo DetailView::widget([
		'model' => $model,
		'attributes' => [
			'title:html',
            'email:email',
            'skype',
            'phone',
            'fax',
			'create_time:datetime',
            [
              'attribute' => 'country_id',
              'value'=>$model->country->name
            ],
            [
              'attribute' => 'region_id',
              'value'=>$model->region->name
            ],
            [
              'attribute' => 'city_id',
              'value'=>$model->city->name
            ],
			'street',
			'house',
			'corps',
			'cabinet',
			'rating',
            [
              'attribute' => 'status',
              'value'=>$model->getStatusTitle()
            ],
            'description:html',
        ],
	]); ?>
</div>