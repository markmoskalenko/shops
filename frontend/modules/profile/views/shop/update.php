<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Shop $oModelShop
 * @var array $aCities
 * @var array $aRegions
 * @var array $aCounties
 */

$this->title = 'Обновление вашего магазина: ' . $oModelShop->title;
?>
<div class="shop-update">

	<h1><?= Html::encode($this->title) ?></h1>

    <?php echo $this->render('_form', [
        'oModelShop' => $oModelShop,
        'aCounties'  => $aCounties,
        'aRegions'   => $aRegions,
        'aCities'    => $aCities
      ]); ?>

</div>
