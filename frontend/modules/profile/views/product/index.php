<?php

use yii\helpers\Html;
use yii\grid\GridView;

use frontend\modules\profile\models\Shop;
use frontend\modules\profile\models\Product;
use frontend\assets\_ProfileProductIndexAsset;
use common\models\Manufacturer;
use common\models\ProductGroup;
use common\models\CategoryCatalog;

_ProfileProductIndexAsset::register($this);

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\models\ProductSearch $searchModel
 */

$this->title = 'Товары';
?>
<div class="product-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a('Добавить новый товар', ['create'], ['class' => 'btn btn-success']) ?>
	</p>

    <?php echo GridView::widget(
      [
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
          [
            'format' => 'image',
            'value'  => function ($value) {
                  return $value->mainImage ? $value->mainImage->getUrl(50, 50) : 'http://placehold.it/50x50';
              },
          ],
          [
            'attribute' => 'code',
            'format' => 'html',
            'value'     => function ($value) {
                  return Html::a( $value->code, \yii\helpers\Url::to(['update', 'id'=>$value->id]));
              },
            'options'   => ['style' => 'width:70px'],
          ],
          [
            'attribute' => 'title',
            'options'   => ['style' => 'width:170px'],
          ],
          [
            'attribute' => 'shop_id',
            'filter'    => Shop::getMap(),
            'value'     => function ($value) {
                  return $value->shop->title;
              },
            'options'   => ['style' => 'width:150px'],
          ],
          [
            'attribute' => 'manufacturer_id',
            'filter'    => Manufacturer::getMap(),
            'value'     => function ($value) {
                  return $value->manufacturer->name;
              },
          ],
          [
            'attribute' => 'product_group_id',
            'filter'    => ProductGroup::getMap(),
            'value'     => function ($value) {
                  return $value->productGroup->name;
              },
            'options'   => ['style' => 'width:150px'],
          ],
          [
            'attribute' => 'category_id',
            'filter'    => CategoryCatalog::getTreeMap(),
            'value'     => function ($value) {
                  return $value->category->name;
              },
            'options'   => ['style' => 'width:250px'],
          ],
          [
            'label'     => 'Цена',
            'attribute' => 'price',
            'options'   => ['style' => 'width:70px'],
          ],
          [
            'label'     => 'Скидка',
            'attribute' => 'amount',
            'options'   => ['style' => 'width:70px'],
          ],
          [
            'attribute' => 'status',
            'filter'    => Product::getMapStatus(),
            'value'     => function ($value) {
                  return $value->getStatusTitle();
              },
            'options'   => ['style' => 'width:150px'],
          ],
          [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}'
          ],
        ],
      ]
    ); ?>
</div>