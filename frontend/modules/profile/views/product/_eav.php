<?php
    use \yii\helpers\Html;
?>
<?php $iCounterEav = 0; ?>
<?php $oModelsEav = $oModel->getEav(); ?>
<?php if($oModelsEav): ?>
    <?php foreach( $oModelsEav as $oEav ): $iCounterEav++; ?>
        <div class="col-xs-3">
            <p class="header"><?= $oEav['title']?></p>
            <?php if( $oEav['type'] == \common\models\Attributes::TYPE_LIST && $oEav['is_multi'] ): ?>
                <?= Html::checkboxList( $oEav['name'], $oEav['value'], $oEav['values'] ); ?>
            <?php elseif( $oEav['type'] == \common\models\Attributes::TYPE_LIST ): ?>
                <?= Html::radioList( $oEav['name'], $oEav['value'], $oEav['values'] ) ?>
            <?php elseif( $oEav['type'] == \common\models\Attributes::TYPE_BOOLEAN ): ?>
                <div class="checkbox">
                    <label>
                        <?= Html::checkbox( $oEav['name'], $oEav['value'] ) ?>
                        Да
                    </label>
                </div>
            <?php elseif( $oEav['type'] == \common\models\Attributes::TYPE_INTEGER ): ?>
                <?= Html::textInput( $oEav['name'], $oEav['value'], ['class'=>'form-control']) ?>
            <?php endif; ?>
        </div>
        <?php if($iCounterEav % 4 == 0 ): ?>
            <div class="clear"></div>
            <hr/>
        <?php endif; ?>
    <?php endforeach; ?>
<?php else: ?>
  Для данной группы товара нет параметров.
<?php endif; ?>