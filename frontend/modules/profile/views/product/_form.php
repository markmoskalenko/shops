<?php

use frontend\assets\_ProfileProductUpdateAsset;

_ProfileProductUpdateAsset::register($this);

/**
 * @var yii\web\View $this
 * @var common\models\Product $oModel
 * @var yii\widgets\ActiveForm $form
 * @var array $aManufacturer
 * @var array $aShops
 * @var array $aGroups
 * @var array $aCategories
 */
?>

<div class="product-form row">

<?php
echo yii\bootstrap\Tabs::widget([
     'items' => [
         [
             'label' => 'Общее',
             'content' => $this->render('_tabMain',['oModel'=>$oModel,'aShops'=>$aShops,'aCategories'=>$aCategories,'aManufacturer'=>$aManufacturer,'aGroups'=>$aGroups]),
             'active' => true
         ],
         [
             'label' => 'Картинки',
             'content' => $this->render('_tabImages',['oModel'=>$oModel,'aShops'=>$aShops,'aCategories'=>$aCategories,'aManufacturer'=>$aManufacturer,'aGroups'=>$aGroups]),
         ],
     ],
 ])
?>
</div>