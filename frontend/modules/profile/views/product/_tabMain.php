<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="col-xs-12">

    <?= Html::activeHiddenInput($oModel, 'id'); ?>
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-xs-6"><?= $form->field($oModel, 'shop_id')->dropDownList( $aShops ) ?></div>
        <div class="col-xs-6"><?= $form->field($oModel, 'price')->textInput(['maxlength' => 10]) ?></div>
    </div>

    <div class="row">
        <div class="col-xs-6"><?= $form->field($oModel, 'title') ?></div>
        <div class="col-xs-6"><?= $form->field($oModel, 'amount') ?></div>
    </div>

    <div class="row">
        <div class="col-xs-6"><?= $form->field($oModel, 'category_id')->dropDownList( $aCategories, ['prompt'=>'-- Выберите категорию товара --'] ) ?></div>
    </div>

    <div class="row">
        <div class="col-xs-6"><?= $form->field($oModel, 'manufacturer_id')->dropDownList( $aManufacturer, ['prompt'=>'-- Выберите производителя товара --'] ) ?></div>
    </div>

    <div class="row">
        <div class="col-xs-6"><?= $form->field($oModel, 'product_group_id')->dropDownList( $aGroups, ['prompt'=>'-- Выберите группу товара --'] ) ?></div>
    </div>

    <div class="row">
        <div class="col-xs-12"><?= $form->field($oModel, 'description')->textarea(['rows' => 6]) ?></div>

    </div>
    <div class="row">
        <div class="col-xs-6"><?= $form->field($oModel, 'status')->dropDownList( $oModel::getMapStatus() ) ?></div>
        <div class="col-xs-6"><?= $form->field($oModel, 'count')->textInput() ?></div>
    </div>

    <div class="row">
        <div class="col-xs-6"><?= $form->field($oModel, 'is_published')->checkbox() ?></div>

    </div>

    <?= $form->field($oModel, 'sImagesForm',    ['template'=>"{input}"])->input('hidden'); ?>
    <?= $form->field($oModel, 'sImageMainForm', ['template'=>"{input}"])->input('hidden'); ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Параметры товара</h3>
        </div>
        <div class="panel-body" id="eav-container">
            <?= $this->render('_eav',[ 'oModel' => $oModel ]);?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>