<div class="col-xs-12">
    <form id="upload" method="post" action="<?php echo Yii::$app->getUrlManager()->createUrl('/profile/product-image/upload') ?>" enctype="multipart/form-data">
        <div id="drop">
            <div class="alert alert-info">
                Для загрузки картинки, перетащите ее сюда либо
                <a href="javascript:void(0)" class="btn btn-success">откройте с компьютера</a>
                <input type="file" name="ProductImages[file]" multiple />
            </div>
            <div style="float: right" class="alert alert-success"> Для выбора главного изображения нажмите на него.</div>
            <ul>
                <?php foreach( $oModel->images as $oProductImage ): ?>
                    <li>
                        <img src="<?= $oProductImage->getUrl(100,100) ?>" data-img="<?= $oProductImage->alias ?>" class="<?= $oProductImage->is_main ? 'active' : '' ?>">
                        <a class="delete-img" href="javascript:void(0)" data-img="<?= $oProductImage->alias ?>">Удалить</a>
                    </li>
                <?php endforeach ?>
            </ul>
        </div>
    </form>
</div>