<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Product $oModel
 * @var array $aManufacturer
 * @var array $aShops
 * @var array $aGroups
 * @var array $aCategories
 */

$this->title = 'Обновление товара: ' . $oModel->title;
?>
<div class="product-update">

	<h1><?= Html::encode($this->title) ?></h1>

	<?php echo $this->render('_form', [
        'oModel'        => $oModel,
        'aManufacturer' => $aManufacturer,
        'aShops'        => $aShops,
        'aGroups'       => $aGroups,
        'aCategories'   => $aCategories,
      ]); ?>
</div>
