<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Product $oModel
 * @var array $aManufacturer
 * @var array $aShops
 * @var array $aGroups
 * @var array $aCategories
 */

$this->title = 'Создаем новый товар..';
?>
<div class="product-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?php echo $this->render('_form', [
        'oModel'        => $oModel,
        'aManufacturer' => $aManufacturer,
        'aShops'        => $aShops,
        'aGroups'       => $aGroups,
        'aCategories'   => $aCategories,
      ]); ?>
</div>