<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 15.02.14
 * Time: 16:39
 */

namespace frontend\modules\profile;

use Yii;
use yii\base\Module;

class Profile extends Module{

    public function init(){

        $this->setLayoutPath('@frontend/modules/profile/views/layouts');
        $this->layout = '@frontend/modules/profile/views/layouts/main.php';

        Yii::$app->getView()->bLeftSidebar  = false;
        Yii::$app->getView()->bRightSidebar = false;
        Yii::$app->getView()->bTopSlider    = false;
        Yii::$app->getView()->bTopBanner    = false;

        return parent::init();
    }

}