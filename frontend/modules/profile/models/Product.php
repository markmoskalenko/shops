<?php

namespace frontend\modules\profile\models;

use common\models\ProductImages;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_product".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $shop_id
 * @property integer $manufacturer_id
 * @property integer $category_id
 * @property integer $product_group_id
 * @property string $url
 * @property string $price
 * @property integer $status
 * @property integer $created
 * @property integer $updated
 * @property integer $amount
 * @property integer $code
 * @property integer $user_id
 * @property integer $count
 *
 * @property CategoryCatalog $category
 * @property Manufacturer $manufacturer
 * @property ProductGroup $productGroup
 * @property Shops $shop
 */
class Product extends \common\models\Product
{
    /**
     * Список названий изображений через запятую, загруженных для этого продукта.
     * Требуется для обработки формы.
     * @var string
     */
    public $sImagesForm = '';

    /**
     * Название главного изображения товара.
     * Требуется для обработки формы.
     * @var string
     */
    public $sImageMainForm = '';

    public function rules()
    {
        return ArrayHelper::merge(
          parent::rules(),
          [
            ['sImagesForm', 'filter', 'filter' => function ($value) {
                  return \common\filters\HtmlPurifier::escape( $value );
            }],

            ['sImageMainForm', 'filter', 'filter' => function ($value) {
                  return \common\filters\HtmlPurifier::escape( $value );
            }],
          ]
        );
    }

    public static function find()
    {
        return parent::find()->where( [ 'user_id' => \Yii::$app->user->getId() ] );
    }

    /**
     * Like запрос.
     * Ищет похожие названия товаров. Нужно для автокомплита.
     *
     * @param $desired - Искомое слово
     *
     * @return array
     */
    public static function getMapAllByLikeName( $desired ){

        $aModels = self::find()
          ->where( ['like', 'title', "{$desired}"] )
          ->with(
            [
              'mainImage'    => function($q) { $q->select(['alias','product_id']); },
              'category'     => function($q) { $q->select(['name','id']); },
              'images'       => function($q) { $q->select(['alias','product_id']); },
              'manufacturer' => function($q) { $q->select(['name','id']); },
              'productGroup' => function($q) { $q->select(['name','id']); }
            ]
          )
          ->asArray()
          ->all();

        foreach( $aModels as &$aModel ){
            $aModel['description'] = \common\filters\HtmlPurifier::clear( $aModel['description'] );

            foreach( $aModel['images'] as &$aImage ){
                $aImage['absolute_url'] = ProductImages::processUrl( $aImage['alias'], 100, 100 );
            }
            unset( $aImage );

        }
        unset( $aModel );

        return ['term'=>$desired,'data'=>$aModels];
    }

    /**
     * Выполняется перед валидацией данных.
     *
     * $this->user_id - Записываем ID пользователя
     *
     * @return bool
     */
    public function beforeValidate(){
        if( $this->isNewRecord ){
            $this->user_id = \Yii::$app->user->getId();
        }
        return parent::beforeValidate();
    }

    /**
     * Метод выполняется после сохранения модели в бд.
     * @param bool $insert
     */
    public function afterSave( $insert ){

        ProductImages::attachImageByNames( $this->sImagesForm, $this->sImageMainForm, $this->id );

        parent::afterSave( $insert );
    }

    /**
     * Метод выполняется после поиска данных
     *
     * Тут мы заполняем публичную переменную sImagesForm названиями картинок через запятую.
     * Требуется при работе с формой создания товара.
     *
     */
    public function afterFind(){

        $oImages = $this->images;

        $aImages = [];

        foreach( $oImages as $oImage ){

            if( $oImage->is_main ) $this->sImageMainForm = $oImage->alias;

            $aImages[] = $oImage->alias;
        }

        $this->sImagesForm = implode(',',$aImages);

        parent::afterFind();
    }
}