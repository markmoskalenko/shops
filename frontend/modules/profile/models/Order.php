<?php

namespace frontend\modules\profile\models;

use Yii;

/**
 * This is the model class for table "tbl_order".
 *
 * @property string $id
 * @property string $shop_id
 * @property string $product_id
 * @property string $client_id
 * @property integer $status
 * @property string $retail_comment
 * @property string $date_created
 * @property string $client_comment
 * @property string $product_count
 * @property string $execution_date
 * @property string $expected_completion_date
 *
 * @property User $client
 * @property Product $product
 * @property Shops $shop
 */
class Order extends \common\models\Order
{
    public static function find()
    {
        return new OrderQuery(get_called_class());
    }

    public static function getCountNewByShop(){
        return self::find()->where(['status'=>self::STATUS_NEW])->byShop()->count();
    }

    public static function getCountNewByUser(){
        return self::find()->where(['status'=>self::STATUS_NEW])->byUser()->count();
    }
}