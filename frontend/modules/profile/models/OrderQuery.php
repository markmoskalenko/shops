<?php

namespace frontend\modules\profile\models;

use Yii;

class OrderQuery extends \yii\db\ActiveQuery
{
    public function byShop()
    {
        $this->andWhere( [ Order::tableName().'.shop_id' => Shop::getIDsByUser() ] );
        return $this;
    }

    public function byUser()
    {
        $this->andWhere( [ Order::tableName().'.client_id' => Yii::$app->user->id ] );
        return $this;
    }

    public function byShopOrUser(){

        $this->andWhere([
            'or',
            [ Order::tableName().'.shop_id' => Shop::getIDsByUser() ],
            [ Order::tableName().'.client_id' => Yii::$app->user->id ]
          ]);

        return $this;
    }
}