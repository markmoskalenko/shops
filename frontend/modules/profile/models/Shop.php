<?php

namespace frontend\modules\profile\models;

use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_shops".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $status
 * @property integer $create_time
 * @property string $alias
 * @property integer $city_id
 * @property string $street
 * @property integer $house
 * @property integer $corps
 * @property integer $cabinet
 * @property string $phone
 * @property string $skype
 * @property string $fax
 * @property string $email
 * @property string $rating
 * @property integer $user_id
 *
 * @property Product[] $products
 * @property DictionaryCities $city
 * @property User $user
 */
class Shop extends \common\models\Shop
{

    /**
     * Выполняется перед валидацией данных.
     * @return bool
     */
    public function beforeValidate()
    {
        if( $this->isNewRecord ){
            $this->user_id = \Yii::$app->user->getId();
        }
        return parent::beforeValidate();
    }

    public static function find()
    {
        return parent::find()->where( [ 'user_id' => \Yii::$app->user->getId() ] );
    }

    /**
     * Возвращает массив key=>value
     * Нужнен для dropDown силектов
     *
     * @return array
     */
    public static function getMap(){

        $oModel = self::find()->asArray()->all();

        return ArrayHelper::map( $oModel, 'id', 'title' );
    }

    public static function getIDsByUser(){

        $aModels = self::find()->asArray()->all();

        $aResult = [];

        foreach( $aModels as $aModel ){

            $aResult[] = $aModel['id'];
        }

        return $aResult;
    }

    /**
     * Проверяет принадлежность магазина к текущему пользователю
     * @param $iShopId
     *
     * @return bool
     */
    public static function isByUser( $iShopId ){

        return self::find()->andWhere(['id'=>(int)$iShopId])->exists();

    }
}