<?php

namespace frontend\modules\profile\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

use common\filters\HtmlPurifier;


class ProductSearch extends Product
{
	public function rules()
	{
		return [
            ['code', 'integer'],

            ['shop_id', 'integer'],

            ['manufacturer_id', 'integer'],

            ['product_group_id', 'integer'],

            ['category_id', 'integer'],

            ['price','number'],

            ['amount','integer'],

            ['status','integer'],

            ['title', 'string', 'max' => 255],
            ['title', 'filter', 'filter' => function ($value) {
                  return HtmlPurifier::escape( $value );
              }],
		];
	}

    public function scenarios()
    {
        return Model::scenarios();
    }

	public function search($params)
	{
		$query = Product::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

        $query->andFilterWhere( [ 'code' => $this->code ]);

        $query->andFilterWhere( [ 'shop_id' => $this->shop_id ]);

        $query->andFilterWhere( [ 'manufacturer_id' => $this->manufacturer_id ]);

        $query->andFilterWhere( [ 'product_group_id' => $this->product_group_id ]);

        $query->andFilterWhere( [ 'category_id' => $this->category_id ]);

        $query->andFilterWhere( [ 'price' => $this->price ]);

        // TODO:: Сделать интервал
        $query->andFilterWhere( [ 'price' => $this->price ]);

        $query->andFilterWhere( [ 'amount' => $this->amount ]);

        $query->andFilterWhere( [ 'status' => $this->status ]);

        $query->andFilterWhere( [ 'LIKE', 'title', $this->title ] );

		return $dataProvider;
	}
}