<?php

namespace frontend\modules\profile\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ShopSearch represents the model behind the search form about Shop.
 */
class ShopSearch extends Model
{
	public $id;
	public $title;
	public $description;
	public $status;
	public $create_time;
	public $alias;
	public $city_id;
	public $street;
	public $house;
	public $corps;
	public $cabinet;
	public $phone;
	public $skype;
	public $fax;
	public $email;
	public $rating;
	public $user_id;

	public function rules()
	{
		return [
			[['id', 'status', 'create_time', 'city_id', 'house', 'corps', 'cabinet', 'user_id'], 'integer'],
			[['title', 'description', 'alias', 'street', 'phone', 'skype', 'fax', 'email'], 'safe'],
			[['rating'], 'number'],
		];
	}

	public function search($params)
	{
		$query = Shop::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$this->addCondition($query, 'title');
		$this->addCondition($query, 'title', true);
		$this->addCondition($query, 'description');
		$this->addCondition($query, 'description', true);
		$this->addCondition($query, 'alias');
		$this->addCondition($query, 'alias', true);
		$this->addCondition($query, 'street');
		$this->addCondition($query, 'street', true);
		$this->addCondition($query, 'phone');
		$this->addCondition($query, 'phone', true);
		$this->addCondition($query, 'skype');
		$this->addCondition($query, 'skype', true);
		$this->addCondition($query, 'fax');
		$this->addCondition($query, 'fax', true);
		$this->addCondition($query, 'email');
		$this->addCondition($query, 'email', true);
		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}