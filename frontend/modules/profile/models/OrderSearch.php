<?php

namespace frontend\modules\profile\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class OrderSearch extends Order
{
    public $date_created_start;
    public $date_created_end;

    public function attributes()
    {
        return array_merge(parent::attributes(), ['product.title', 'client.username']);
    }

    public function rules()
    {
        return [
            [[
               'date_created_start',
               'date_created_end',
               'shop_id',
               'status',
             ], 'integer'],
            ['product.title', 'safe'],
            ['client.username', 'safe']
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Order::find()->with('shop');

        $query->joinWith(['product' => function($query) {
                $query->from( [ 'product'=>Product::tableName() ]);  }]
        );

        $query->joinWith(['client' => function($query) {
                $query->from( [ 'client'=>'tbl_user' ]); }]
        );

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $dataProvider->sort->attributes['product.title'] = [
          'asc' => ['product.title' => SORT_ASC],
          'desc' => ['product.title' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['client.username'] = [
          'asc' => ['client.username' => SORT_ASC],
          'desc' => ['client.username' => SORT_DESC],
        ];

        if( !empty( $params['OrderSearch']['date_created_start'] )){
            $params['OrderSearch']['date_created_start'] = strtotime( $params['OrderSearch']['date_created_start'] );
        }

        if( !empty( $params['OrderSearch']['date_created_end'] )){
            $params['OrderSearch']['date_created_end'] = strtotime( $params['OrderSearch']['date_created_end'] );
        }

        if (!($this->load($params) && $this->validate())) {

            $query->joinWith(['product']);
            return $dataProvider;
        }

        $query->andFilterWhere([
            Order::tableName().'.shop_id'    => $this->shop_id,
            Order::tableName().'.status'     => $this->status,
        ]);

        $query->andFilterWhere(['LIKE', 'product.title', $this->getAttribute('product.title')]);

        $query->andFilterWhere(['LIKE', 'client.username', $this->getAttribute('client.username')]);

        if( !empty( $this->date_created_start ) ){
            $d = $this->date_created_start;
            $query->andWhere( 'date_created > :date_start', [
                ':date_start'   =>  mktime(00, 00, 00, date( 'm', $d ), date( 'd', $d ), date( 'Y', $d ))
              ]);
        }

        if( !empty( $this->date_created_end ) ){
            $d = $this->date_created_end;
            $query->andWhere( 'date_created < :date_end', [
                ':date_end'     =>  mktime(23, 59, 59, date( 'm', $d ), date( 'd', $d ), date( 'Y', $d )),
              ]);
        }

        return $dataProvider;
    }
}