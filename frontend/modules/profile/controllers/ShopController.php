<?php

namespace frontend\modules\profile\controllers;

use common\models\User;
use common\models\DictionaryCities;
use common\models\DictionaryRegion;
use common\models\DictionaryCountries;
use common\filters\HtmlPurifier;

use frontend\modules\profile\models\Shop;
use frontend\modules\profile\models\ShopSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use Yii;

class ShopController extends Controller
{
    public function behaviors()
    {
        return [
          'access' => [
            'class' => AccessControl::className(),
            'only'  => [ 'index', 'view', 'create', 'update', 'delete' ],
            'rules' => [
              [
                'actions' => [ 'index', 'view', 'create', 'update', 'delete' ],
                'allow'   => true,
                'roles'   => [User::ROLE_RETAIL],
              ]
            ],
          ],

          'verbs'  => [
            'class'   => VerbFilter::className(),
            'actions' => [
              'delete' => ['post'],
            ],
          ],
        ];
    }

    /**
     * Список магазинов
     *
     * @return string
     */
    public function actionIndex()
	{
		$searchModel = new ShopSearch;
		$dataProvider = $searchModel->search($_GET);

		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

    /**
     * Просмотр данных о магазине
     *
     * @param $id
     *
     * @return string
     */
    public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

    /**
     * Создание нового магазина
     *
     * @return string|\yii\web\Response
     */
    public function actionCreate()
	{
		$oModelShop = new Shop;

		if ($oModelShop->load($_POST) && $oModelShop->save()) {

            Yii::$app->session->setFlash('order', 'Магазин успешно создан!');

            return $this->redirect(['view', 'id' => $oModelShop->id]);
		}

        $aCounties = DictionaryCountries::getMap();
        $aRegions  = [];
        $aCities   = [];

        if( !is_null( $oModelShop->country_id ) )
            $aRegions = DictionaryRegion::getMapByCountryId( $oModelShop->country_id );

        if( !is_null( $oModelShop->region_id ) )
            $aCities = DictionaryCities::getMapByRegionId( $oModelShop->region_id );

        return $this->render('create', [
            'oModelShop' => $oModelShop,
            'aCounties'  => $aCounties,
            'aRegions'   => $aRegions,
            'aCities'    => $aCities
        ]);
	}

    /**
     * Обновление магазина
     *
     * @param $id
     *
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id)
	{
        $oModelShop = $this->findModel($id);

		if ($oModelShop->load($_POST) && $oModelShop->save()) {

            Yii::$app->session->setFlash('order', 'Информация сохранена.');

            return $this->redirect(['view', 'id' => $oModelShop->id]);
		}


        $aCounties = DictionaryCountries::getMap();
        $aRegions  = [];
        $aCities   = [];

        if( !is_null( $oModelShop->country_id ) )
            $aRegions = DictionaryRegion::getMapByCountryId( $oModelShop->country_id );

        if( !is_null( $oModelShop->region_id ) )
            $aCities = DictionaryCities::getMapByRegionId( $oModelShop->region_id );

        $oModelShop->description = HtmlPurifier::clear( $oModelShop->description );

        return $this->render('update', [
            'oModelShop' => $oModelShop,
            'aCounties'  => $aCounties,
            'aRegions'   => $aRegions,
            'aCities'    => $aCities
        ]);
	}

    /**
     * Удаление магазина
     *
     * @param $id
     *
     * @return \yii\web\Response
     */
    public function actionDelete($id)
	{
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

    /**
     * Загрузка модели магазина
     *
     * @param $id
     *
     * @return null|\yii\db\ActiveQuery|\yii\db\ActiveRecord
     * @throws \yii\web\NotFoundHttpException
     */
    protected function findModel($id)
	{
		if (($model = Shop::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('Страница не найдена.');
		}
	}
}