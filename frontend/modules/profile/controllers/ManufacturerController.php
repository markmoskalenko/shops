<?php

namespace frontend\modules\profile\controllers;

use common\models\User;
use common\models\Manufacturer;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use Yii;

class ManufacturerController extends Controller
{
	public function behaviors()
	{
		return [
          'access' => [
            'class' => AccessControl::className(),
            'only'  => ['get-manufacturer-by-category'],
            'rules' => [
              [
                'actions' => ['get-manufacturer-by-category'],
                'allow'   => true,
                'roles'   => [User::ROLE_RETAIL],
              ]
            ],
          ],
		];
	}

    /**
     * Экшен для Ajax запроса.
     * Результат массив в JSon формате содержащий мануфактуры
     *
     * @param $id
     *
     * @return array
     *
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetManufacturerByCategory( $id ){

        if( !\Yii::$app->request->isAjax )
            throw new NotFoundHttpException('Страница не найдена.');

        Yii::$app->response->format = Response::FORMAT_JSON;

        return Manufacturer::getMapByCategoryId( $id );
    }
}