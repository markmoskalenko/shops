<?php

namespace frontend\modules\profile\controllers;

use common\models\User;
use common\models\ProductGroup;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\AccessControl;
use Yii;

class ProductGroupController extends Controller
{
	public function behaviors()
	{
		return [
          'access' => [
            'class' => AccessControl::className(),
            'only'  => [ 'get-product-group-by-category' ],
            'rules' => [
              [
                'actions' => [ 'get-product-group-by-category' ],
                'allow'   => true,
                'roles'   => [User::ROLE_RETAIL],
              ]
            ],
          ],
		];
	}

    /**
     * Экшен для Ajax запроса.
     * Результат массив в JSon формате содержащий группы товаров.
     *
     * @param $id
     *
     * @return array
     *
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetProductGroupByCategory( $id ){

        if( !\Yii::$app->request->isAjax )
            throw new NotFoundHttpException('Страница не найдена.');

        Yii::$app->response->format = Response::FORMAT_JSON;

        return ProductGroup::getMapByCategoryId( $id );
    }
}