<?php

namespace frontend\modules\profile\controllers;

use common\models\User;
use common\models\CategoryCatalog;
use common\models\Manufacturer;
use common\models\ProductGroup;
use common\filters\HtmlPurifier;

use frontend\modules\profile\models\Product;
use frontend\modules\profile\models\ProductSearch;
use frontend\modules\profile\models\Shop;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii;

class ProductController extends Controller
{
	public function behaviors()
	{
        return [
          'access' => [
            'class' => AccessControl::className(),
            'only'  => [ 'get-eav-by-product-group','search-alike-by-name', 'index', 'view', 'create', 'update', 'delete' ],
            'rules' => [
              [
                'actions' => [ 'get-eav-by-product-group', 'search-alike-by-name', 'index', 'view', 'create', 'update', 'delete' ],
                'allow'   => true,
                'roles'   => [User::ROLE_RETAIL],
              ]
            ],
          ],

          'verbs'  => [
            'class'   => VerbFilter::className(),
            'actions' => [
              'delete' => ['post'],
            ],
          ],
        ];
	}

    /**
     * Возвращает массив параметров товара - категория, мануфактура и тд
     * Требуется для реализации умного заполнения товара.
     * Когда пользователь вводит название товара именно сюда приходит запрос на данные
     *
     * @param $desired - входная строка для поиска
     *
     * @return array - результат ответа модели
     * @throws \yii\web\NotFoundHttpException - Если не ajax запрос, то выкидываем exception
     */
    public function actionSearchAlikeByName( $desired ){

        if( !\Yii::$app->request->isAjax )
            throw new NotFoundHttpException('Страница не найдена.');

        Yii::$app->response->format = Response::FORMAT_JSON;

        $aModel = Product::getMapAllByLikeName( $desired );

        return $aModel;
    }

    /**
     * Возвращает форму кастомных параметров для группы товара
     *
     * @param      $id
     * @param null $product_id
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetEavByProductGroup( $id, $product_id = null ){

        if( !\Yii::$app->request->isAjax )
            throw new NotFoundHttpException('Страница не найдена.');

        $oModel = new Product();

        $oModel->product_group_id = (int) $id;

        $oModel->id = (int) $product_id;

        return $this->renderPartial('_eav',[
              'oModel' => $oModel
          ]);
    }

    /**
     * Получаем список параметров определенного товара
     * @param $id - ID товара
     *
     * @return string - html
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetEavByProductId( $id ){

        if( !\Yii::$app->request->isAjax )
            throw new NotFoundHttpException('Страница не найдена.');

        $oModel = Product::findOne( (int)$id );

        return $this->renderPartial('_eav',[
            'oModel' => $oModel
          ]);
    }

    /**
     * Список всех продуктов пользователя
     *
     * @return string
     */
    public function actionIndex()
	{
		$searchModel = new ProductSearch;
		$dataProvider = $searchModel->search($_GET);

		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

    /**
     * Просмотр товара
     * @param $id
     *
     * @return string
     */
    public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

    /**
     * Создание товара
     *
     * @return string|Response
     */
    public function actionCreate()
	{
		$oModel = new Product;

		if ( $oModel->load($_POST) && $oModel->save() ) {
			return $this->redirect(['create']);
		}

        $aManufacturer = [];
        $aGroups       = [];
        $aShops        = Shop::getMap();
        $aCategories   = CategoryCatalog::getTreeMap();

        if( !is_null( $oModel->category_id ) ){
            $aManufacturer = Manufacturer::getMapByCategoryId($oModel->category_id);
            $aGroups       = ProductGroup::getMapByCategoryId($oModel->category_id);
        }

        if(empty( $oModel->count )) $oModel->count = 1;
        if(empty( $oModel->is_published )) $oModel->is_published = 1;

        return $this->render('create', [
            'oModel'        => $oModel,
            'aManufacturer' => $aManufacturer,
            'aShops'        => $aShops,
            'aGroups'       => $aGroups,
            'aCategories'   => $aCategories,
        ]);
	}

    /**
     * Обновление товара
     *
     * @param $id
     *
     * @return string|Response
     */
    public function actionUpdate($id)
	{
        $oModel = $this->findModel($id);

		if ( $oModel->load($_POST) && $oModel->save() ) {
			return $this->redirect(['index']);
		}

        $aManufacturer = [];
        $aGroups       = [];
        $aShops        = Shop::getMap();
        $aCategories   = CategoryCatalog::getTreeMap();

        if( !is_null( $oModel->category_id ) ){
            $aManufacturer = Manufacturer::getMapByCategoryId($oModel->category_id);
            $aGroups       = ProductGroup::getMapByCategoryId($oModel->category_id);
        }

        $oModel->description = HtmlPurifier::clear( $oModel->description );
        return $this->render('update', [
            'oModel'        => $oModel,
            'aManufacturer' => $aManufacturer,
            'aShops'        => $aShops,
            'aGroups'       => $aGroups,
            'aCategories'   => $aCategories,
          ]);
	}

    /**
     * Удаление товара
     *
     * @param $id
     *
     * @return Response
     */
    public function actionDelete($id)
	{
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

    /**
     * Поиск модели товара
     *
     * @param $id
     *
     * @return null|yii\db\ActiveQuery|yii\db\ActiveRecord
     * @throws \yii\web\NotFoundHttpException
     */
    protected function findModel($id)
	{
		if (($model = Product::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('Страница не найдена.');
		}
	}
}