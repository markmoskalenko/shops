<?php

namespace frontend\modules\profile\controllers;

use frontend\modules\profile\models\Order;
use frontend\modules\profile\models\OrderSearch;
use common\models\User;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;

class OrderController extends Controller
{
    public function behaviors()
    {
        return [
          'access' => [
            'class' => AccessControl::className(),
            'only'  => [ 'index', 'view', 'update', 'my' ],
            'rules' => [
              [
                'actions' => [ 'index', 'view', 'update', 'my' ],
                'allow'   => true,
                'roles'   => [User::ROLE_RETAIL],
              ]
            ],
          ],
        ];
    }

    /**
     * Список всех заявок
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch;

        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        $dataProvider->query->byShop();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionMy()
    {
        $searchModel = new OrderSearch;

        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        $dataProvider->query->byUser();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
          ]);
    }

    /**
     * Общая информация по заявке.
     *
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $oModel = $this->findModel($id);

        $oModel->setStatusView();

        return $this->render('view', [
            'model' => $oModel,
        ]);
    }

    /**
     * Обновление заявки
     *
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $oModel = $this->findModel($id);

        $oModel->setStatusView();

        if( isset( $_POST['Order']['status'] ) ){

            $oModel->setStatus( (int)$_POST['Order']['status'] );

        }

        $oModel->setScenario('update');

        if ( $oModel->load( Yii::$app->request->post() ) && $oModel->save() ) {

            return $this->redirect(['view', 'id' => $oModel->id]);

        } else {

            return $this->render('update', [
                'model' => $oModel,
            ]);

        }
    }

    /**
     * Поиск модели
     *
     * @param string $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::find()->where([ 'id'=>$id])->byShopOrUser()->one() ) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Страница не найдена.');
        }
    }
}