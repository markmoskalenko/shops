<?php

namespace frontend\modules\profile\controllers;

use common\models\User;
use common\models\ProductImages;

use yii\web\Controller;
use yii\web\Response;
use yii\filters\AccessControl;
use yii;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductImageController extends Controller
{
	public function behaviors()
	{
		return [
          'access' => [
            'class' => AccessControl::className(),
            'only'  => [ 'upload' ],
            'rules' => [
              [
                'actions' => [ 'upload' ],
                'allow'   => true,
                'roles'   => [User::ROLE_RETAIL],
              ]
            ],
          ],
		];
	}

	public function actionUpload()
	{
        $oModel = new ProductImages();

        $oModel->scenario = 'upload';

        Yii::$app->response->format = Response::FORMAT_JSON;

        if( $oModel->load( $_FILES )  && $oModel->saveImg() ) {

            return ['name'=>$oModel->alias, 'status'=>'success'];

        }

        return ['status'=>'error','message'=>'Неверный формат файла!'];
	}
}