<?php
use \yii\helpers\Html;

\frontend\assets\FilterAsset::register($this);
?>
<?= Html::beginForm(\yii\helpers\Url::to(['product/index','url'=>$sCategoryUrl]), 'GET', ['class'=>'form-filter']); ?>
<div class="list-group filter">
    <div class="count-result">Найдено <span>10</span></div>
    <?php foreach( $aEav as $iAttributeId => $oEav ): ?>

        <div class="list-group-item active">
            <?php if( !empty( $oEav['description'] ) ): ?>
                <span title="<?=$oEav['description']; ?>" class="info glyphicon glyphicon-info-sign"></span>
            <?php endif; ?>
            <?= $oEav['title']?>
        </div>

        <div class="list-group-item">
            <?php if( $oEav['type'] == \common\models\Attributes::TYPE_LIST ): ?>
                <?= Html::checkboxList( $oEav['name'], isset($aFilter[$iAttributeId]) ? $aFilter[$iAttributeId] : null, $oEav['values'] ) ?>
            <?php elseif( $oEav['type'] == \common\models\Attributes::TYPE_BOOLEAN ): ?>
                <div class="checkbox">
                <label>
                <?= Html::checkbox( $oEav['name'], isset($aFilter[$iAttributeId]) ? 1 : 0 ) ?>
                    Да
                </label>
                </div>
            <?php elseif( $oEav['type'] == \common\models\Attributes::TYPE_INTEGER ): ?>
                <label for="">
                    от:
                    <?= Html::textInput( $oEav['name'].'[min]', isset($aFilter[$iAttributeId]['min']) ? $aFilter[$iAttributeId]['min'] : '', ['class'=>'form-control min_attribute']) ?>
                </label>
                <label for="">
                    до:
                    <?= Html::textInput( $oEav['name'].'[max]', isset($aFilter[$iAttributeId]['max']) ? $aFilter[$iAttributeId]['max'] : '', ['class'=>'form-control max_attribute']) ?>
                </label>
                <?= \yii\jui\Slider::widget([
                    'clientOptions'=>[
                        'range'=>true,
                        'min'=> ($oEav['min'] - 1),
                        'max'=> ($oEav['max'] + 1),
                        'values'=>[
                          (isset($aFilter[$iAttributeId]['min']) && !empty($aFilter[$iAttributeId]['min']) ? $aFilter[$iAttributeId]['min'] : $oEav['min'] - 1),
                          (isset($aFilter[$iAttributeId]['max']) && !empty($aFilter[$iAttributeId]['max']) ? $aFilter[$iAttributeId]['max'] : ($oEav['max'] + 1))
                        ],
                    ],
                    'clientEvents'=>[
                        'slide' => 'function (event, ui) {
                            var input_min = $(this).parents(".list-group-item").find(".min_attribute");
                            var input_max = $(this).parents(".list-group-item").find(".max_attribute");
                            var min = '.($oEav["min"] - 1).';
                            var max = '.($oEav["max"] + 1).';
                            if( ui.values[0] <= min ){
                                input_min.val("");
                            }else{
                                input_min.val(ui.values[0]);
                            }

                            if( ui.values[1] >= max ){
                                input_max.val("");
                            }else{
                                input_max.val(ui.values[1]);
                            }
                            input_min.change();
                            input_max.change();
                        }'
                    ]
                  ]); ?>
            <?php endif; ?>
        </div>
    <?php endforeach; ?>
    <div class="list-group-item">
        <button type="submit" style="width: 100%" class="btn btn-warning">Фильтровать</button>
    </div>
</div>
<?=Html::endForm(); ?>

<script type="text/javascript">
    window.onload = function(){
        $('.list-group input').change(function(){
            var data = $('.form-filter').serializeArray();
            var el = $(this);
            $.ajax({
                url : '/product/filter-count?url=<?= $sCategoryUrl ?>',
                data : data,
                type: 'get'
            }).done(function( r ){
                var pos_container = $('.form-filter').offset();
                var pos = el.offset();

                $('.count-result span').text(r);
                $('.count-result').stop(true).animate({
                    top: (pos.top  - pos_container.top - 15),
                    opacity : 1
                }, 200);
                console.log( pos.top );
                setTimeout(function(){
                    $('.count-result').stop(true).animate({
                        opacity: 0
                    }, 200);
                }, 1000);

            });
        });

    }
</script>