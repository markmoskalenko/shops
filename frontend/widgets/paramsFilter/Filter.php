<?php
namespace frontend\widgets\paramsFilter;

use common\models\CategoryBase;
use common\models\CategoryCatalog;
use yii\base\Widget;

class Filter extends Widget
{
    /**
     * Категория к которой строится фильтр по параметрам
     * @var null
     */
    public $iCategoryId = null;

	public function init()
	{
		parent::init();

        $sAliasCategory = \Yii::$app->request->getQueryParam('url', null);

        $oCategory = CategoryCatalog::find()->byUrl( $sAliasCategory )->one();

        if( $oCategory === null ) return;

        $aEav = $oCategory->getEav();

        $aFilter = isset($_GET['Product']['attributesEav']) ? $_GET['Product']['attributesEav'] : [];

        if( count( $aEav ) ){
            echo $this->render('filter',[
                'aEav'         => $aEav,
                'aFilter'      => $aFilter,
                'sCategoryUrl' => $oCategory->url
              ]
            );
        }
	}
}