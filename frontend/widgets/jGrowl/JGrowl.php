<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\widgets\JGrowl;

/**
 * Alert widget renders a message from session flash. All flash messages are displayed
 * in the sequence they were assigned using setFlash. You can set message as following:
 *
 * - \Yii::$app->getSession()->setFlash('error', 'This is the message');
 * - \Yii::$app->getSession()->setFlash('success', 'This is the message');
 * - \Yii::$app->getSession()->setFlash('info', 'This is the message');
 *
 */
class JGrowl extends \yii\bootstrap\Widget
{
	/**
	 * @var array the alert types configuration for the flash messages.
	 * This array is setup as $key => $value, where:
	 * - $key is the name of the session flash variable
	 * - $value is the bootstrap alert type (i.e. danger, success, info, warning)
	 */
	public $alertTypes = [
		'error'   => 'danger',
		'danger'  => 'danger',
		'success' => 'success',
		'info'    => 'info',
		'warning' => 'warning'
	];
	
	/**
	 * @var array the options for rendering the close button tag.
	 */
	public $closeButton = [];

    // TODO::Сделать нотифекатор цветной. Цвет зависит от статуса сообщения.
	public function init()
	{
		parent::init();

        $this->registerScript();

		$session = \Yii::$app->getSession();
		$flashes = $session->getAllFlashes();
//		$appendCss = isset($this->options['class']) ? ' ' . $this->options['class'] : '';
		
		foreach ($flashes as $type => $message) {

            $js = '$.jGrowl("'.$message.'", { life: 1000 });';

            $this->getView()->registerJs($js);

			/* initialize css class for each alert box */
//			$this->options['class'] = 'alert-' . $this->alertTypes[$type] . $appendCss;
//
//			/* assign unique id to each alert box */
//			$this->options['id'] = $this->getId() . '-' . $type;
//
//			echo \yii\bootstrap\Alert::widget([
//				'body' => $message,
//				'closeButton' => $this->closeButton,
//				'options' => $this->options
//			]);

			$session->removeFlash($type);
		}
	}

    public function registerScript(){
        JGrowlAsset::register($this->getView());
    }
}