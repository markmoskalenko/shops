<?php

namespace frontend\widgets\JGrowl;

use yii\web\AssetBundle;

class JGrowlAsset extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/jGrowl/assets';
    public $css = [
        'jquery.jgrowl.min.css'
    ];
    public $js = [
        'jquery.jgrowl.min.js'
    ];

    public $depends = [
      'yii\web\JqueryAsset',
    ];
}