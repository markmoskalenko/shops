<?php
namespace frontend\widgets;

use yii\base\Widget;
use yii\bootstrap\BootstrapPluginAsset;
use yii\helpers\Html;
use frontend\assets\RatyAsset;
use frontend\components\View;

class Raty extends Widget
{

    public $value = 0;

    public $id;

    public function run()
    {
        $this->setId(md5(microtime()));
        echo Html::tag('div','',['id'=>$this->getId()]);

        $this->registerScript();
    }

    protected function registerScript(){

        if( !\Yii::$app->request->isAjax )
            RatyAsset::register($this->view);

        $this->view->registerJs("$('#".$this->getId()."').raty({ path: '/library/raty/img', readOnly: true, score: ".$this->value." });", View::POS_END, 'raty_'.$this->getId());
    }
}