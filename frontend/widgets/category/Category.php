<?php
namespace frontend\widgets\category;

use common\models\CategoryCatalog;
use yii\base\Widget;

class Category extends Widget
{

	public function init()
	{
		parent::init();

        $oCategories = CategoryCatalog::find()->addOrderBy('lft, root')->all();

        echo $this->render('category',[
              'oCategories' => $oCategories
        ]);

	}
}