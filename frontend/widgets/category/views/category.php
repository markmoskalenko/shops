<?php
    use yii\helpers\Url;
?>
<div class="list-group">
    <?php foreach( $oCategories as $oCategory ): ?>
        <a href="<?= Url::to(['product/index', 'url'=>$oCategory->url]); ?>" class="list-group-item <?= !$oCategory->isLeaf() ? 'active' : '' ?>"><?=$oCategory->name ?></a>
    <?php endforeach; ?>
</div>