<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 16.02.14
 * Time: 21:02
 */

namespace frontend\components;
use yii\helpers\Html;


class View extends \yii\web\View {

    /**
     * Показывать ли левый сайдбар
     * @var bool
     */
    public $bLeftSidebar = true;

    /**
     * Показывать ли правый сайдбар
     * @var bool
     */
    public $bRightSidebar = true;

    /**
     * Показывать ли виджет - фильтр
     * @var bool
     */
    public $bFilterSidebar = true;

    /**
     * Показывать ли слайдер
     * @var bool
     */
    public $bTopSlider = true;

    public $bTopBanner = true;

    public $aBreadcrumbs = [
      'Лучшие товары, новинки и новости',
    ];
}