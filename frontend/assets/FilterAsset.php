<?php
namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class FilterAsset
 *
 * @package frontend\assets
 */
class FilterAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $js = [
        'js/filter.js',
	];
	public $depends = [
      'yii\web\JqueryAsset',
      'yii\jui\ThemeAsset',
      'yii\jui\TooltipAsset',
      'yii\jui\SliderAsset',
    ];
}
