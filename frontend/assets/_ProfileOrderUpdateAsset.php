<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

class _ProfileOrderUpdateAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
      'js/profile.order.update.js',
    ];

    public $depends = [
      'frontend\assets\AppAsset',
//      'frontend\assets\SelectAsset',
    ];
}