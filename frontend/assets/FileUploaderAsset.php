<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class FileUploaderAsset
 *
 * @package frontend\assets
 */
class FileUploaderAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
//		'library/fileUploader/css/style.css',
	];
	public $js = [
        'library/fileUploader/js/jquery.knob.js',
        'library/fileUploader/js/jquery.ui.widget.js',
        'library/fileUploader/js/jquery.fileupload.js',
        'library/fileUploader/js/script.js',
	];

	public $depends = [
        'yii\web\JqueryAsset',
    ];
}