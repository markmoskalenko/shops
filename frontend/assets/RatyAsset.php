<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class RatyAsset
 *
 * @package frontend\assets
 */
class RatyAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';

	public $js = [
        'library/raty/jquery.raty.min.js',
	];

	public $depends = [
        'yii\web\JqueryAsset',
    ];
}