<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class _ProfileProductUpdateAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
      'js/profile.product.form.js',
    ];

    public $depends = [
      'frontend\assets\AppAsset',
      'frontend\assets\SelectAsset',
      'frontend\assets\FileUploaderAsset',
    ];
}