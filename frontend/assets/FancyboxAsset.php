<?php
namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class FancyboxAsset
 *
 * @package frontend\assets
 */
class FancyboxAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
		'library/fancybox/jquery.fancybox.css',
	];
	public $js = [
        'library/fancybox/jquery.fancybox.js',
	];
	public $depends = [
      'yii\web\JqueryAsset',
    ];
}
