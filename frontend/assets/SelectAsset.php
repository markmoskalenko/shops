<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class SelectAsset
 *
 * @package frontend\assets
 */
class SelectAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
		'library/select2/select2.css',
		'library/select2/select2-bootstrap.css',
	];
	public $js = [
        'library/select2/select2.js',
	];

	public $depends = [
        'yii\web\JqueryAsset',
    ];
}