$(function(){

    var ul = $('#upload ul');

    $('#drop .alert a').click(function(){
        $(this).parent().find('input').click();
    });

    $('#upload').fileupload({
        dropZone: $('#drop'),

        add: function (e, data) {

            var uploadErrors = [];
            var acceptFileTypes = /^image\/(gif|jpe?g|png)$/i;

            if(data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
                uploadErrors.push('Неверный формат файла. Выберите изображение - gif, jpeg, png.');
            }
            if(data.originalFiles[0]['size'].length && data.originalFiles[0]['size'] > 5000000) {
                uploadErrors.push('Filesize is too big');
            }
            if(uploadErrors.length > 0) {
                alert(uploadErrors.join("\n"));
            } else {


            var tpl = $('<li class="working"><img src=""><input style="float: left" type="text" value="0" data-width="48" data-height="48"'+
                ' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" /><a class="delete-img" href="javascript:void(0)" data-img="">Удалить</a></li>');

//            // Append the file name and file size
//            tpl.find('p').text(data.files[0].name)
//                         .append('<i>' + formatFileSize(data.files[0].size) + '</i>');

            data.context = tpl.appendTo(ul);

            tpl.find('input').knob();

            tpl.find('span').click(function(){

                if(tpl.hasClass('working')){
                    jqXHR.abort();
                }

                tpl.fadeOut(function(){
                    tpl.remove();
                });

            });

            var jqXHR = data.submit();
            }
        },

        progress: function(e, data){

            var progress = parseInt(data.loaded / data.total * 100, 10);

            data.context.find('input').val(progress).change();

            if(progress == 100){
                data.context.find('canvas').remove();
                data.context.removeClass('working');
            }
        },

        fail:function(e, data){
            // Something has gone wrong!
            data.context.addClass('error');
        },

        done:function(e, data){

            if(data.result.status == 'error') {
                alert(data.result.message);
            }else{

            var name = data.result.name;
            var img = data.context.find('input').parents('li').find('img');
            data.context.find('input').parents('li').find('a').attr('data-img',name);
            console.log(data);
            img.attr('src','/uploads/' + new String(data.result.name).replace('.', '_100x100.'));
            img.attr('data-img', name);

            var images = $('#product-simagesform').val();

            if( images == '' ){
                $('#product-simagesform').val(name);
            }else{
                images = images.split(',');
                images.push(name);
                $('#product-simagesform').val( images.join(',') );
            }
        }
        }
    });


    $(document).on('click','#upload ul li .delete-img',function(){

        var img = $(this).attr('data-img');

        if($('#product-simagemainform').val() == img) $('#product-simagemainform').val('');

        var images = $('#product-simagesform').val().split(',');

        images.splice(images.indexOf(img), 1);

        $('#product-simagesform').val(images.join(',') );

        $(this).parents('li').hide(300);
    });

    $(document).on('click','#upload ul li img',function(){

        $('#upload ul li img').removeClass('active');

        $(this).addClass('active');

        $('#product-simagemainform').val( $(this).attr('data-img') );
    });

    // Prevent the default action when a file is dropped on the window
    $(document).on('drop dragover', function (e) {
        e.preventDefault();
    });

    // Helper function that formats the file sizes
    function formatFileSize(bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }

        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }

        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }

        return (bytes / 1000).toFixed(2) + ' KB';
    }
});