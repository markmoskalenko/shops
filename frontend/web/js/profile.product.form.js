var load_empty_eav = true;

$('#product-category_id').change(function(){
    var category_id = $(this).val();
    $.ajax({
        url : '/profile/manufacturer/get-manufacturer-by-category',
        data : { id : category_id },
        type: 'GET'
    }).done(function(r){
            $("#product-manufacturer_id").select2("val", "");
            $("#product-manufacturer_id").empty();
            $.each( r, function(i, item) {
                $("#product-manufacturer_id").append( $('<option value="'+i+'">'+r[i]+'</option>'))
            });
            $("#product-manufacturer_id").change();
        });

    $.ajax({
        url : '/profile/product-group/get-product-group-by-category',
        data : { id : category_id },
        type: 'GET'
    }).done(function(r){
            $("#product-product_group_id").select2("val", "");
            $("#product-product_group_id").empty();
            $.each( r, function(i, item) {
                $("#product-product_group_id").append( $('<option value="'+i+'">'+r[i]+'</option>'))
            });
            $("#product-product_group_id").change();
        });
});

$('#product-product_group_id').change(function(){
    var product_group_id = $(this).val();
    var product_id = $('#product-id').val();
    if( load_empty_eav ){
        $.ajax({
            url : '/profile/product/get-eav-by-product-group',
            data : { id : product_group_id, product_id : product_id },
            type: 'GET'
        }).done(function(r){
                $("#eav-container").html(r);
            });
    }else{
        load_empty_eav = true;
    }
});

$("#product-status, #product-shop_id, #product-manufacturer_id, #product-product_group_id, #product-category_id").select2();

$("#product-title").select2({
    placeholder: "Введите название продукта",
    minimumInputLength: 1,
    ajax: {
        url: "/profile/product/search-alike-by-name",
        dataType: 'json',
        data: function (term, page) {
            return {
                desired: term // search term
            };
        },
        results: function (data, page) {
            var results = [];
            results.push({
                id: data.term,
                text: data.term
            });
            $.each(data.data, function(index, item){
                results.push({
                    id: item.title,
                    ids: item.id,
                    text: item.title,
                    price: item.price,
                    amount: item.amount,
                    category: item.category.name,
                    category_id : item.category_id,
                    manufacturer: item.manufacturer.name,
                    manufacturer_id: item.manufacturer_id,
                    productGroup: item.productGroup.name,
                    productGroup_id: item.product_group_id,
                    description: item.description,
                    img: ( item.mainImage != null ? item.mainImage.alias : false),
                    images: ( item.images != null ? item.images : false)
                });
            });
            return {
                results: results
            };
        }
    },

    formatResult: formatResult,
    formatSelection: formatSelection,
    dropdownCssClass: "bigdrop",
    escapeMarkup: function (m) { return m; }
});

if($('#product-title').val() !=null){
    $('#product-title').prev('#s2id_product-title').find('.select2-chosen').text($('#product-title').val());
}

function formatResult(item) {
    var image = new String(item.img).replace('.', '_100x100.');


    var markup = "<table class='result'><tr>";
    if( typeof item.img !== 'undefined' && item.img != false){
        markup += "<td class='item-image'><img width='100' src='/uploads/" + image + "'/></td>";
    }else{
        markup += "<td class='item-image'><img width='100' src='/images/no-img.jpg'/></td>";
    }

    markup += "<td class='movie-image'>";
    markup += "<p class='header'>"+item.text+"</p>";
    if( typeof item.category !== 'undefined' )
        markup += "<p><span>Категория</span> "+item.category+"</p>";
    if( typeof item.manufacturer !== 'undefined' )
        markup += "<p><span>Производитель</span> "+item.manufacturer+"</p>";
    if( typeof item.productGroup !== 'undefined' )
        markup += "<p><span>Группа товара</span> "+item.productGroup+"</p>";
    if( typeof item.price !== 'undefined' )
        markup += "<p><span>Стоимость</span> "+item.price+"</p>";
    if( typeof item.amount !== 'undefined' )
        markup += "<p><span>Скидка</span> "+item.amount+"%</p>";
    markup += "</td>";
    return markup;
}

function formatSelection(item) {
    fill_form(item);
    if(typeof item.ids != 'undefined'){
        load_empty_eav = false;
        load_eav(item.ids)
    }
    return item.text;
}

function load_eav( id ){
    $.ajax({
        url : "/profile/product/get-eav-by-product-id",
        data : { id : id }
    }).done(function( r ){
        $("#eav-container").html( r );
    });
}

function fill_form( data ){
    if( data.manufacturer_id != null)
        $("#product-manufacturer_id").select2("val", data.manufacturer_id);

    if( data.productGroup_id != null)
        $("#product-product_group_id").select2("val", data.productGroup_id);

    if( data.category_id != null){
        $("#product-category_id").select2("val", data.category_id);
        $('#product-category_id').change();
    }

    if( data.price != null)
        $("#product-price").val(data.price);

    if( data.description != null)
        $("#product-description").val(data.description);

    if( data.amout != null)
        $("#product-amout").val(data.amout);

    if( data.images != null ){
        var images = new Array();
        $.each(data.images, function(index, item){
            images.push(item.alias);
            $('#upload ul').append(
                '<li>' +
                    '<img src="'+item.absolute_url+'" data-img="'+item.alias+'" class="">' +
                    '<a class="delete-img" href="javascript:void(0)" data-img="'+item.alias+'">Удалить</a>' +
                '</li>'
            );
        });
        $('#product-simagesform').val( images.join(',') );
    }
}

$('#product-status').change(function(){
    var val = $(this).val();
    if(val==1){
        $('#product-count').val(1);
    }else{
        $('#product-count').val(0);
    }
});