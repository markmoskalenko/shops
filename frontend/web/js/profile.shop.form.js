$('#shop-country_id').change(function(){
    var country_id = $(this).val();
    $.ajax({
        url : '/region/get-regions-by-country',
        data : { id : country_id },
        type: 'GET'
    }).done(function(r){
        $("#shop-region_id").select2("val", "");
        $("#shop-region_id").empty();
        $("#shop-region_id").parents('.custom-hide').show();
        $.each( r, function(i, item) {
            $("#shop-region_id").append( $('<option value="'+i+'">'+r[i]+'</option>'))
        });

        $("#shop-city_id").select2("val", "");
        $("#shop-city_id").empty();
    });
});

$('#shop-region_id').change(function(){
    var region_id = $(this).val();
    $.ajax({
        url : '/city/get-cities-by-region',
        data : { id : region_id },
        type: 'GET'
    }).done(function(r){
        $("#shop-city_id").select2("val", "");
        $("#shop-city_id").empty();
        $("#shop-city_id").parents('.custom-hide').show();
        $.each( r, function(i, item) {
            $("#shop-city_id").append( $('<option value="'+i+'">'+r[i]+'</option>'))
        });
    });
});
$("#shop-status, #shop-country_id, #shop-region_id, #shop-city_id").select2();