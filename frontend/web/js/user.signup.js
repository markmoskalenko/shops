$('#user-country_id').change(function(){
    var country_id = $(this).val();
    $.ajax({
        url : '/region/get-regions-by-country',
        data : { id : country_id },
        type: 'GET'
    }).done(function(r){
            $("#user-region_id").select2("val", "");
            $("#user-region_id").empty();
            $("#user-region_id").parents('.custom-hide').show();
            $.each( r, function(i, item) {
                $("#user-region_id").append( $('<option value="'+i+'">'+r[i]+'</option>'))
            });
            $("#user-region_id").change();
        });
});

$('#user-region_id').change(function(){
    var region_id = $(this).val();
    $.ajax({
        url : '/city/get-cities-by-region',
        data : { id : region_id },
        type: 'GET'
    }).done(function(r){
            $("#user-city_id").select2("val", "");
            $("#user-city_id").empty();
            $("#user-city_id").parents('.custom-hide').show();
            $.each( r, function(i, item) {
                $("#user-city_id").append( $('<option value="'+i+'">'+r[i]+'</option>'))
            });
        });
});
$("#user-country_id, #user-region_id, #user-city_id").select2();