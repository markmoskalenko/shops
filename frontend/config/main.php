<?php
$rootDir = __DIR__ . '/../..';

$params = array_merge(
  require($rootDir . '/common/config/params.php'),
  require($rootDir . '/common/config/params-local.php'),
  require(__DIR__ . '/params.php'),
  require(__DIR__ . '/params-local.php')
);

return [
  'id'                  => 'app-frontend',
  'basePath'            => dirname(__DIR__),
  'vendorPath'          => $rootDir . '/vendor',
  'controllerNamespace' => 'frontend\controllers',
  'language'            => 'ru',
//  'preload'             => ['debug'],
'bootstrap' => ['debug'],
  'modules'             => [
    'gii'     => 'yii\gii\Module',
    'debug'   => $params['module.debug'],
    'profile' => 'frontend\modules\profile\Profile'
  ],
  'extensions'          => require($rootDir . '/vendor/yiisoft/extensions.php'),
  'components'          => [
    'db'           => $params['components.db'],
//    'cache'        => $params['components.cache'],
    'mail'         => $params['components.mail'],
    'authManager'  => $params['components.authManager'],
    'cache' => [
      'class' => '\yii\caching\FileCache',
    ],
    'urlManager'   => [
      'enablePrettyUrl' => true,
      'showScriptName'  => false,
      'rules'           => [
          'product/view/<url:(\p{L}|\p{N}|-)+>/'=>'product/view',
          'product/list/<url:(\p{L}|\p{N}|-)+>/'=>'product/index',
          '<controller:\w+>/<id:\d+>'              => '<controller>/view',
          'uploads/<url:([A-Za-z0-9.-_])+>' => 'uploads/index',
          '<controller:\w+>/<action:\w+>'          => '<controller>/<action>',
//          '<controller:\w+>/<action:\w+>/<url:(0-9 a-Z)+>' => '<controller>/<action>',
//          '<controller:\w+>/<action:\w+>'          => '<controller>/<action>',
//          'uploads/<url:(\p{L}|\p{N}|-)+>/'=>'uploads/index'
//        '<controller:(product)>/<action:(index)>/<url:\w+>'                     => 'product/<url:\w+>',
//        '<controller:\w+>/<id:\d+>'              => '<controller>/view',
//        '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
//        '<controller:\w+>/<action:\w+>'          => '<controller>/<action>',
        'debug/<controller>/<action>'            => 'debug/<controller>/<action>',
      ]
    ],
    'view'         => [
      'class' => 'frontend\components\View',
    ],
    'user'         => [
      'identityClass' => 'common\models\User',
      'loginUrl'      => ['user/login'],
    ],
    'log'          => [
      'traceLevel' => YII_DEBUG ? 3 : 0,
      'targets'    => [
        [
          'class'  => 'yii\log\FileTarget',
          'levels' => ['error', 'warning'],
        ],
      ],
    ],
    'errorHandler' => [
      'errorAction' => 'site/error',
    ],
  ],
  'params'              => $params,
];
