<?php

namespace frontend\controllers;

use common\models\DictionaryCities;
use common\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use Yii;

class CityController extends Controller
{
    /**
     * Экшен для Ajax запроса.
     * Результат массив в JSon формате содержащий города региона $id=>$title
     *
     * @param $id
     *
     * @return array
     *
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetCitiesByRegion($id)
    {

        if (!\Yii::$app->request->isAjax) throw new NotFoundHttpException('Страница не найдена.');

        Yii::$app->response->format = Response::FORMAT_JSON;

        return DictionaryCities::getMapByRegionId($id);
    }
}