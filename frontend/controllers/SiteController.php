<?php

namespace frontend\controllers;

use common\models\Product;
use Yii;
use yii\web\Controller;

class SiteController extends Controller
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'only' => ['login', 'logout', 'signup'],
				'rules' => [
					[
						'actions' => ['signup','login'],
						'allow' => true,
						'roles' => ['?'],
					],
					[
						'actions' => ['logout'],
						'allow' => true,
						'roles' => ['@'],
					]
				],
			],
		];
	}

    public function actions()
    {
        return [
          'error' => [
            'class' => 'yii\web\ErrorAction',
          ],
        ];
    }

	public function actionIndex()
	{
        //Todo Published
        $oModelProducts = Product::find()->limit(10)->orderBy('id DESC')->all();
		return $this->render('index', ['oModelProducts' => $oModelProducts]);
	}
}