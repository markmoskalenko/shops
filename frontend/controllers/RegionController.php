<?php

namespace frontend\controllers;

use common\models\User;
use common\models\DictionaryRegion;
use yii\web\Response;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Yii;

class RegionController extends Controller
{
    /**
     * Экшен для Ajax запроса.
     * Результат массив в JSon формате содержащий регионы страны $id=>$title

     * @param $id
     *
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetRegionsByCountry( $id ){

        if( !\Yii::$app->request->isAjax )
            throw new NotFoundHttpException('Страница не найдена.');

        Yii::$app->response->format = Response::FORMAT_JSON;

        return DictionaryRegion::getMapByCountryId( $id );
    }
}