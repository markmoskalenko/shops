<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 09.04.14
 * Time: 18:39
 */

namespace frontend\controllers;


use Imagine\Image\ManipulatorInterface;
use yii\imagine\Image;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;


class UploadsController extends Controller {

    public function actionIndex( $url ){

        $request = \Yii::$app->request->url;

        $resourcesPath = \Yii::getAlias('@webroot').$request;

        if (preg_match('/_(\d+)x(\d+).*\.(jpg|jpeg|png|gif)/i', $resourcesPath, $matches)) {

            if (!isset($matches[0]) || !isset($matches[1]) || !isset($matches[2]) || !isset($matches[3]))
                throw new BadRequestHttpException('Некорректный запрос.');

            if ($matches[2] === 0) $matches[2] = null;

            if ($matches[2] === 0) $matches[2] = null;

            $originalFile = str_replace($matches[0], '', $resourcesPath).'.'.$matches[3];

            if (!file_exists($originalFile))
                throw new NotFoundHttpException('Страница не найдена.');

                $dirname = dirname($resourcesPath);

                if (!is_dir($dirname)) mkdir($dirname, 0775, true);

                Image::thumbnail( $originalFile , $matches[1], $matches[2], ManipulatorInterface::THUMBNAIL_INSET)
                     ->save( $resourcesPath, ['quality' => 100]);

                $this->refresh();
        }
    }
}