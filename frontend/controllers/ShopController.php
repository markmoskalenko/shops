<?php

namespace frontend\controllers;

use common\models\Shop;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


class ShopController extends Controller
{

    public function actionGetInfo($id){

        if (!\Yii::$app->request->isAjax) throw new NotFoundHttpException('Страница не найдена.');

        $oShop = Shop::find()->where('id = :id', [':id'=>(int)$id])->one();

        return $this->renderPartial('_info', [
              'oShop' => $oShop,
        ]);
    }
}