<?php

namespace frontend\controllers;

use common\models\CategoryCatalog;
use common\models\Product;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


class ProductController extends Controller
{
    /**
     * Количество товаров, которые соответствуют набору параметров фильтра
     * Action для ajax запроса, на количество товара
     * @param $url - alias категории
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionFilterCount( $url ){
        if (!\Yii::$app->request->isAjax) throw new NotFoundHttpException('Страница не найдена.');

        return Product::getProductByFilter( $url, isset($_GET['Product']['attributesEav']) ? $_GET['Product']['attributesEav'] : [] , true );
    }

    /**
     * Список товаров отфильтрованные по категории и по параметрам
     *
     * @param $url - alias категории
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex( $url ){

        $aCategory = CategoryCatalog::find()->byUrl( $url )->asArray()->one();

        if( !count( $aCategory ) )
            throw new NotFoundHttpException('Страница не найдена.');

        $this->view->aBreadcrumbs = [ 'Список товаров', $aCategory['name'] ];

        $aFilter = isset($_GET['Product']['attributesEav']) ? $_GET['Product']['attributesEav'] : [];

        $iTotalCountProduct = Product::getProductByFilter( $url, $aFilter, true );

        $oPagination = new Pagination([ 'totalCount' => $iTotalCountProduct ]);

        if( $limit = Yii::$app->request->getQueryParam('limit', false) )
            Yii::$app->session->set('pageSize', (int)$limit );

        if( $pageSize = Yii::$app->session->get('pageSize', false ) )
            $oPagination->setPageSize( $pageSize );
        else
            $oPagination->setPageSize( Product::DEFAULT_PAGE_SIZE );

        if( Yii::$app->request->getQueryParam('order', false) && Yii::$app->request->getQueryParam('type', false) ){

            $order = Yii::$app->request->getQueryParam('order', false) . ' ' . Yii::$app->request->getQueryParam('type');

            Yii::$app->session->set('orderBy', $order);
        }

        $oModel = Product::getProductByFilter( $url, $aFilter, false, $oPagination->limit, $oPagination->offset, Yii::$app->session->get('orderBy', Product::DEFAULT_ORDER ) );

        if( $oModel === null )
            throw new NotFoundHttpException('Страница не найдена.');

		return $this->render('index', [
            'oModel'      => $oModel,
            'aFilter'     => $aFilter,
            'aCategory'   => $aCategory,
            'oPagination' => $oPagination,
            'sOrder'      => Yii::$app->session->get('orderBy', Product::DEFAULT_ORDER)
          ]);
	}

    /**
     * Карточка товара
     * @param $url - alias категории
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView( $url ){

        $this->view->bTopBanner = false;

        $oModel = Product::getProductByUrl( $url );

        if( $oModel === null )
            throw new NotFoundHttpException('Страница не найдена.');


        return $this->render('view', [
            'oModel' => $oModel,
          ]);
    }
}