<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\widgets\ActiveForm;
use yii\web\Response;

use common\models\DictionaryCities;
use common\models\DictionaryRegion;
use common\models\DictionaryCountries;


use common\models\LoginForm;
use common\models\User;


class UserController extends Controller
{

    public $layout = '@frontend/views/layouts/fullClear';
	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'only' => ['login', 'logout', 'signup', 'loginajax'],
				'rules' => [
					[
						'actions' => ['signup','login', 'loginajax'],
						'allow' => true,
						'roles' => ['?'],
					],
					[
						'actions' => ['logout'],
						'allow' => true,
						'roles' => ['@'],
					]
				],
			],
		];
	}

    /**
     * Авторизация пользователя AJAX
     *
     * @return array|string
     * @throws \yii\web\HttpException
     */
    public function actionLoginAjax(){

        if( !\Yii::$app->request->isAjax ) throw new HttpException(404, 'Страница не найдена.');

        $model = new LoginForm();

        if ( $model->load($_POST) ) {

            if( $model->login() ){ echo '1'; exit; }

            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);

        }

        return $this->renderPartial('ajax/login', [
            'model' => $model,
          ]);
    }

    /**
     * Авторизация пользователя
     *
     * @return string|\yii\web\Response
     */
    public function actionLogin()
	{
		if (!\Yii::$app->user->isGuest) {
			$this->goHome();
		}

		$model = new LoginForm();
		if ($model->load($_POST) && $model->login()) {
			return $this->goBack();
		} else {
			return $this->render('login', [
				'model' => $model,
			]);
		}
	}

    /**
     * Выход из аккаунта
     *
     * @return \yii\web\Response
     */
    public function actionLogout()
	{
		Yii::$app->user->logout();

		return $this->goHome();
	}

    /**
     * Регистрация нового пользователя
     *
     * @return string|\yii\web\Response
     */
    public function actionSignup()
	{
		$oModel = new User();

        $oModel->setScenario('signup');

        if ( $oModel->load($_POST) && $oModel->save() ) {

			if (Yii::$app->getUser()->login($oModel)) return $this->goHome();

        }

        $aCounties = DictionaryCountries::getMap();
        $aRegions  = [];
        $aCities   = [];

        if( !is_null( $oModel->country_id ) )
            $aRegions = DictionaryRegion::getMapByCountryId( $oModel->country_id );

        if( !is_null( $oModel->region_id ) )
            $aCities = DictionaryCities::getMapByRegionId( $oModel->region_id );

		return $this->render('signup', [
            'model'     => $oModel,
            'aRegions'  => $aRegions,
            'aCities'   => $aCities,
            'aCounties' => $aCounties,
		]);
	}

    /**
     * Запрос на сброс пароля
     * @return string|\yii\web\Response
     */
    public function actionRequestPasswordReset()
	{
		$model = new User();

		$model->scenario = 'requestPasswordResetToken';

        if ( $model->load($_POST) && $model->validate() ) {

				if ( User::sendPasswordResetEmail( $model->email ) ) {

					Yii::$app->getSession()->setFlash('success', 'Проверьте свою электронную почту для получения дальнейших инструкций.');

                    return $this->goHome();

                } else {

				    Yii::$app->getSession()->setFlash('error', 'Ошибка отправки сообщения на почту. Обратитесь к администрации портала.');

                }
		}

		return $this->render('requestPasswordResetToken', [
			'model' => $model,
		]);
	}

    /**
     * Сброс пароля
     *
     * @param $token
     *
     * @return string|\yii\web\Response
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionResetPassword($token)
	{
        $oModel = User::find([
            'password_reset_token' => $token,
            'status'               => User::STATUS_ACTIVE,
        ]);

		if ( !$oModel ) throw new BadRequestHttpException('Некорректный запрос на сброс пароля.');

        $oModel->scenario = 'resetPassword';

        if ($oModel->load($_POST) && $oModel->save()) {

			Yii::$app->getSession()->setFlash('success', 'Новый пароль сохранен.');

            return $this->goHome();

        }

		return $this->render('resetPassword', [
			'model' => $oModel,
		]);
	}
}