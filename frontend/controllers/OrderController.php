<?php

namespace frontend\controllers;

use common\models\CategoryCatalog;
use common\models\Product;
use common\models\Order;
use frontend\modules\profile\models\Shop;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class OrderController extends Controller
{
    public function behaviors()
    {
        return [
          'access' => [
            'class' => \yii\filters\AccessControl::className(),
            'only'  => [ 'index', 'view', 'create', 'update', 'delete' ],
            'rules' => [
              [
                'actions' => [ 'index', 'view', 'update', 'delete' ],
                'allow'   => true,
                'roles'   => ['@'],
              ],
              [
                'actions' => [ 'create' ],
                'allow'   => true,
              ]
            ],
          ],
        ];
    }

    public function actionCreate($id, $productCount){

        if (!\Yii::$app->request->isAjax) throw new NotFoundHttpException('Страница не найдена.');

        $oProduct = Product::findOne((int)$id);

        $aShopIds = Shop::getIDsByUser();

        $isAllowedOrder = !in_array( $oProduct->shop_id, $aShopIds );

        $oShops = $oProduct->shop->user->shops;

        $aShops = [];

        foreach($oShops as $oShop){
            $aShops[ $oShop->country->name ][ $oShop->city->name ][] = $oShop;
        }

        if( $oProduct === null ) throw new NotFoundHttpException('Товар не найден.');

        return $this->renderPartial('create', [
              'aShops' => $aShops,
              'oProduct' => $oProduct,
              'productCount'=>$productCount,
              'isAllowedOrder'=>$isAllowedOrder
          ]);
    }

    public function actionConfirm($productId, $productCount, $shopId, $clientComment = null ){

        if (!\Yii::$app->request->isAjax) throw new NotFoundHttpException('Страница не найдена.');

        $result = Order::create( $productId, $productCount, $shopId, $clientComment );

        Yii::$app->response->format = Response::FORMAT_JSON;

        return [ 'status' => $result ];

    }
}