<div class="row">

    <div class="col-xs-12">
        <div class="footer">

            <div class="row links">
                <div class="col-xs-3 col-1">
                    <ul>
                        <li><a class="footer-cabinet" href="<?= \yii\helpers\Url::to('profile/product') ?>">Личный кабинет</a></li>
                        <li><a class="footer-cart" href="<?= \yii\helpers\Url::to('profile/order/my') ?>">Мои заявки</a></li>
                        <li><a class="send-message" href="/sendmessage/">Задать вопрос</a></li>
                    </ul>
                </div>
                <div class="col-xs-2">
                    <ul>
                        <li class="title"><a href="/o-kompanii/">О компании</a></li>
                        <li><a class="grey-text" href="/o-kompanii/novosti-kompanii/">Новости компании</a></li>
                        <li><a class="grey-text" href="/o-kompanii/gipjermarkjet/" target="_blank">Гипермаркет</a></li>
                        <li><a class="grey-text" href="/o-kompanii/konkursy-i-rozygryshi/">Конкурсы и розыгрыши</a></li>
                        <li><a class="grey-text" href="/o-kompanii/ishchem-postavshchikov/">Ищем поставщиков</a></li>
                        <li><a class="grey-text" href="/o-kompanii/sotsialjnaya-otvetstvennostj/">Социальная
                                ответственность</a></li>
                    </ul>
                </div>
                <div class="col-xs-2">
                    <ul>
                        <li class="title"><a href="/catalog/">Каталог</a></li>
                        <li><a class="grey-text" href="/new/">Все новинки</a></li>
                        <li><a class="grey-text" href="/sale/">Товары со скидками</a></li>
                        <li><a class="grey-text" href="/hit/">Хиты продаж</a></li>
                        <li><a class="grey-text" href="/trademarks/">Торговые марки</a></li>
                        <li><a class="grey-text" href="/series/">Серии товаров</a></li>
                        <li><a class="grey-text" href="/price/">Прайс-лист</a></li>
                        <li><a class="grey-text" href="/publications/list/">Интересное о товарах</a></li>
                    </ul>
                </div>
                <div class="col-xs-2">
                    <ul>
                        <li class="title"><a href="/info/">Условия работы</a></li>
                        <li><a class="grey-text" href="/info/dogovor/">Заключение договора</a></li>
                        <li><a class="grey-text" href="/info/skidki/">Скидки</a></li>
                        <li><a class="grey-text" href="/info/oplata/">Оплата</a></li>
                        <li><a class="grey-text" href="/info/dostavka/">Доставка товара</a></li>
                        <li><a class="grey-text" href="/info/pravila_priemki/">Правила приемки товара</a></li>
                        <li><a class="grey-text" href="/info/vozvrat/">Возврат товара и <br>претензии по качеству</a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-3">
                    <ul class="social">
                        <li><a class="vk" href="http://vkontakte.ru/public32409346" target="_blank">наша группа
                                ВКонтакте</a></li>
                        <li><a class="facebook"
                               href="http://www.facebook.com/pages/%D0%A1%D0%B8%D0%BC%D0%B0-%D0%BB%D0%B5%D0%BD%D0%B4/292881977411443"
                               target="_blank">читайте нас на Facebook</a></li>
                        <li><a class="twitter" href="http://twitter.com/#!/sima_land" target="_blank">читайте нас в
                                Twitter</a></li>
                        <li><a class="livejournal" href="http://sima-land.livejournal.com/" target="_blank">наш блог в
                                Livejournal</a></li>
                        <li><a class="youtube" href="http://www.youtube.com/user/simalandRU" target="_blank">смотрите
                                нас на YouTube</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 footer_gray cooperation">
                    По вопросам сотрудничества обращайтесь по телефону +375 33 623 01 01 или пишите на почту
                    office@it-yes.com
                </div>
            </div>

            <div class="copyright">
                © Собственность студии web-дизайна и полигрофии <a href="http://it-yes.com">IT-Yes.com</a>,
                2013-<?php echo date('Y') ?>
            </div>

        </div>
    </div>
</div>
