<div class="row">
    <div class="col-xs-12">
        <div class="searchbar">
            <div class="inner">
                <form class="search" action="" method="post">
                    <div class="input-group">
                        <input type="text" class="form-control"  placeholder="Введите поисковую фразу">
                                  <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Найти</button>
                                  </span>
                    </div>
                </form>
            </div>
        </div>
        <div class="menu">

            <?= \yii\widgets\Menu::widget([
                'items' => [
                  ['label' => 'Главная', 'url' => ['site/index']],
//                  ['label' => 'Барахолка', 'url' => ['#']],
                  ['label' => 'Новости', 'url' => ['#']],
//                  ['label' => 'Объявления', 'url' => ['#']],
//                  ['label' => 'Контакты', 'url' => ['#']],
                  ['label' => 'Контакты', 'url' => ['#']],

                ],
                'options'=>[ 'class'=>'left']
                ])
            ?>

            <?= \yii\widgets\Menu::widget([
                'items' => [
                  ['label' => 'Личный кабинет', 'url' => ['profile/product'], 'visible' => !Yii::$app->user->isGuest],
                  ['label' => 'Авторизация', 'url' => ['user/login'], 'visible' => Yii::$app->user->isGuest],
                  ['label' => 'Регистрация', 'url' => ['user/signup'], 'visible' => Yii::$app->user->isGuest],
                  ['label' => !Yii::$app->user->isGuest ? Yii::$app->user->identity->username.' - Выход' : '', 'url' => ['user/logout'], 'visible' => !Yii::$app->user->isGuest],
                ],
                'options'=>[ 'class'=>'right']
              ])
            ?>

        </div>
    </div>
</div>

