<div class="user-control">
    <div class="hello">
        <?php if( Yii::$app->user->isGuest ): ?>
            Добро пожаловать дорогой гость!
        <?php else: ?>
            Добро пожаловать <?php echo Yii::$app->user->identity->username; ?>.
        <?php endif; ?>
    </div>
    <div id="topmenu">
        <div class="moduletable-nav">
            <ul class="menu">
                <?php if( !\Yii::$app->user->isGuest ): ?>
                    <li id="current" class="active item29"><a href="<?= Yii::$app->urlManager->createUrl('/profile/shop'); ?>"><span>Магазины</span></a></li>
                    <li class="item18"><a href="<?= Yii::$app->urlManager->createUrl('/profile/product'); ?>"><span>Товары</span></a></li>
                    <li class="item30"><a href=""><span>Заявки</span></a></li>
                    <li class="item53"><a href=""><span>Профиль</span></a></li>
                    <li><a href="<?= Yii::$app->urlManager->createUrl('/user/logout'); ?>"><span>Выйти</span></a></li>
                <?php endif; ?>

                <?php if( Yii::$app->user->isGuest ): ?>
                    <li><a href="<?= Yii::$app->urlManager->createUrl('/user/signup'); ?>"><span>Зарегистрироваться</span></a></li>
                    <li><a class="login fancybox.ajax" href="<?= Yii::$app->urlManager->createUrl('/user/login-ajax'); ?>"><span>Войти</span></a></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</div>