<?php
use frontend\assets\AppAsset;

AppAsset::register($this);

$this->beginPage();
?>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,700&subset=latin,cyrillic" rel="stylesheet" type="text/css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->head() ?>
</head>

<body id="body">
<?php $this->beginBody() ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
           <?= $content; ?>
        </div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
