<?php

use \yii\widgets\Breadcrumbs;

use frontend\assets\AppAsset;
use frontend\widgets\jGrowl\JGrowl;
use frontend\widgets\category\Category;

AppAsset::register($this);

JGrowl::widget();

$this->beginPage();
?>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<!--    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,500,700&subset=latin,cyrillic" rel="stylesheet" type="text/css">-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext' rel='stylesheet' type='text/css'>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->head() ?>
</head>

<body id="body">
<?php $this->beginBody() ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">

                    <div class="row head">
                        <div class="col-xs-2">
                            <a href="/"><img alt="" src="/images/logo.png"/></a>
                        </div>
                    </div>

                    <?= $this->render('//layouts/menu/_general'); ?>
                </div>
            </div>
            <div class="indent-bottom"></div>
            <div class="row">
                <div class="col-xs-12 content">

                    <?php if ($this->bTopSlider): ?>
                        <div class="banner-top">
                            <a href="#">
                                <img src="/images/slider/banner1.jpg" alt="Banner">
                            </a>
                        </div>
                        <div class="indent-bottom"></div>
                    <?php endif; ?>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="column-left">
                                <?= Category::widget(); ?>
                            </div>

                            <?php if( $this->bRightSidebar ): ?>
                                <div class="column-right">
                                    <?= $this->render('//layouts/sidebar/_right'); ?>
                                </div>
                            <?php endif; ?>


                            <div class="column-center">
                                <div class="wrapper">
                                    <?= Breadcrumbs::widget([
                                            'links' => $this->aBreadcrumbs,
                                            'options'=>['class'=>'breadcrumb-custom']
                                          ]);
                                    ?>
                                </div>
                                <div class="indent-bottom"></div>

                                <?php if( $this->bTopBanner ): ?>
                                    <div class="wrapper">
                                        <?= $this->render('//layouts/_topBanner'); ?>
                                    </div>
                                <?php endif; ?>

                                <div class="indent-bottom"></div>

                                <?= $content; ?>

                                <div class="indent-bottom"></div>
                            </div>
                        </div>
                    </div>
                    <?= $this->render('//layouts/_footer'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
