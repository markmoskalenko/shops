<div class="right login">
    <form class="" role="form">
        <div class="form-inline">
            <div class="form-group">
                <label class="sr-only" for="exampleInputEmail2">Введите Email</label>
                <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Введите Email">
            </div>
            <div class="form-group">
                <label class="sr-only" for="exampleInputPassword2">Пароль</label>
                <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Пароль">
            </div>
            <button type="submit" class="btn btn-primary">Войти</button>
        </div>
        <div class="checkbox left">
            <label>
                <input type="checkbox"> Запомнить меня
            </label>
        </div>
        <a class="registration" href="">Зарегистрироваться</a>
    </form>
</div>