<?php
    \frontend\assets\_ProductIndexAsset::register($this);
?>
<div class="wrapper list-product">
<div class="row">
    <div class="col-xs-12">
        <h3><?= $aCategory['name']; ?></h3>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <?php if( count( $oModel ) ): ?>

        <?php  yii\widgets\Pjax::begin([ 'linkSelector' => '.filterbar a, .pagination a' ]) ?>

        <?= $this->render('_filterBrar', [
                'aCategory'      => $aCategory,
                'aFilter'        => $aFilter,
                'oPagination'    => $oPagination,
                'filterOpenType' => 'dropdown'

          ]); ?>

        <table class="table items-table infinityScrollData">
        <thead>
        <tr>
            <td></td>
            <td style="width: 220px">Наименование</td>
            <td>Фасовка, наличие</td>
            <td class="center">Стоимость позиции (долл. США)</td>
            <td class="count">Количество для заказа</td>
        </tr>
        </thead>
            <tbody>
                <?php foreach( $oModel as $oProduct ): ?>
                    <?php echo $this->render('_item', ['oProduct'=>$oProduct]); ?>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?= \yii\widgets\LinkPager::widget([
                'pagination' => $oPagination,
            ]);
        ?>

        <?= $this->render('_filterBrar', [
                'aCategory'      => $aCategory,
                'aFilter'        => $aFilter,
                'oPagination'    => $oPagination,
                'filterOpenType' => 'dropup'
          ]); ?>
        <?php yii\widgets\Pjax::end(); ?>
        <?php else: ?>
          <p class="message">Товаров не найдено</p>
        <?php endif; ?>

    </div>
</div>
<div class="indent-bottom"></div>
</div>