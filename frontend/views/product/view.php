<?php

use frontend\assets\_ProductViewAsset;
use \yii\helpers\Url;
_ProductViewAsset::register($this);

?>

<div class="wrapper product-card">
    <div class="row">
        <div class="col-xs-12">
            <h1><?=$oModel->title; ?></h1>
            <span>Код товара: <?=$oModel->code; ?></span>
        </div>
    </div>
    <div class="indent-bottom"></div>
    <div class="row">
        <div class="col-xs-4">
            <?php if( $oModel->mainImage ): ?>
                <a rel="group" href="<?= $oModel->mainImage->getUrl() ?>" class="fancybox">
            <?php endif; ?>

                <img class="img-thumbnail" src="<?= $oModel->mainImage ? $oModel->mainImage->getUrl(326,326) : 'http://placehold.it/326x326' ?>" alt=""/>

            <?php if( $oModel->mainImage ): ?>
                </a>
            <?php endif; ?>

            <div class="row">
                <div class="col-xs-12 old-images">
                    <?php foreach( $oModel->images as $oImage ): ?>
                        <a rel="group" href="<?= $oImage->getUrl() ?>" class="fancybox">
                            <img class="img-thumbnail" src="<?= $oImage->getUrl(50,50); ?>" alt=""/>
                        </a>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

        <div class="col-xs-8 ">
            <div class="old-param">
                <div class="row header">
                    <div class="col-xs-4">
                        <p>
                            Производитель:
                            <br/>
                            <?=$oModel->manufacturer->name; ?>
                        </p>
                    </div>
                    <div class="col-xs-4">
                        <p>
                            Количество:
                            <br/>
                            <?=$oModel->count; ?> шт.
                        </p>
                    </div>
                    <div class="col-xs-4">
                        <p>
                            Продавец:
                            <br/>
                            "<?=$oModel->shop->title; ?>"
                        </p>
                    </div>
                </div>

                <hr/>
                <div class="row">
                    <div class="col-xs-4 price">
                        <p><span class="l">Стоимость:</span> <?=$oModel->price; ?>$</p>
                        <p><span class="l">Скидка:</span> <?=$oModel->amount ? $oModel->amount : 0; ?>%</p>
                        <p><span class="l">Оформить заявку</span>
                            <div class="qty-btn active">
                            <input type="text" class="count-product-input numeric" maxlength="4">
                            <div class="min-qty">штук</div>
                            <a href="<?= Url::to(['order/create', 'id'=>$oModel->id]) ?>" onclick="return false;" class="add-basket alt-add fancybox.ajax">
                                <img class="empty" src="/images/cart.png">
                            </a>
                        </div>
                        </p>

                    </div>
                    <div class="col-xs-4 border">
                        <p class="op op_1 "><span></span>Распродажа</p>
                        <p class="op op_2 active"><span></span>Акция</p>
                        <p class="op op_3 active"><span></span>Новинка</p>
                        <p class="op op_4 "><span></span>Хит продаж</p>
                        <p class="op op_5 active"><span></span>Товар в наличии</p>

                    </div>
                    <div class="col-xs-4">
                        <div style="margin-bottom: 10px">
                            <span class="l" style="width: 40px">Рейтинг:</span>
                            <div class="star right">
                                <?= \frontend\widgets\Raty::widget(['value'=>1]) ?>
                            </div>
                        </div>
                        <p><span class="l">Телефон:</span> <br/><?=$oModel->shop->phone; ?></p>
                        <p><span class="l">Skype:</span> <br/><?=$oModel->shop->skype; ?></p>
                        <p><span class="l">E-mail:</span> <br/><?=$oModel->shop->email; ?></p>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="indent-bottom"></div>
    <div class="row">
        <div class="col-xs-12">
            <h2>Описание</h2>
            <p>
                <?= $oModel->description ?>
            </p>
        </div>
    </div>


    <div class="indent-bottom"></div>
    <div class="row">
        <div class="col-xs-12">
            <h2>Технические параметры</h2>
            <div class="eav">
                <table class="table table-bordered">
                    <?php foreach(  $oModel->getEavFront() as $aAttribute ): ?>
                        <tr>
                            <td><?=$aAttribute['title'] ?></td>
                            <td><?=implode(',', $aAttribute['value']) ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>

    <div class="indent-bottom"></div>
    <div class="row">
        <div class="col-xs-12">
            <h2>Комментарии</h2>

            <div id="hypercomments_widget"></div>
            <script type="text/javascript">
                _hcwp = window._hcwp || [];
                _hcwp.push({widget:"Stream", widget_id:18337});
                (function() {
                    if("HC_LOAD_INIT" in window)return;
                    HC_LOAD_INIT = true;
                    var lang = (navigator.language || navigator.systemLanguage || navigator.userLanguage || "en").substr(0, 2).toLowerCase();
                    var hcc = document.createElement("script"); hcc.type = "text/javascript"; hcc.async = true;
                    hcc.src = ("https:" == document.location.protocol ? "https" : "http")+"://w.hypercomments.com/widget/hc/18337/"+lang+"/widget.js";
                    var s = document.getElementsByTagName("script")[0];
                    s.parentNode.insertBefore(hcc, s.nextSibling);
                })();
            </script>
        </div>
    </div>
</div>