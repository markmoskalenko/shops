<?php
    use \yii\helpers\Url;
?>
<tr>
    <td rowspan="1">
        <div class="th140">
            <a href="">
                <img src="<?= $oProduct->mainImage ? $oProduct->mainImage->getUrl(140,140) : 'http://placehold.it/140x140' ?>" style="display: inline;">
            </a>
            <?php if( $oProduct->amount ): ?>
                <div class="sale-ico"><span class="sl-minus">-</span><?= $oProduct->amount ?><span class="sl-percent">%</span></div>
            <?php endif; ?>
            <div class="pic"></div>
        </div>
    </td>
    <td rowspan="1">
        <a href="<?= Url::to(['product/view','url'=>$oProduct->url]) ?>"><?= $oProduct->manufacturer->name ?> <?= $oProduct->title ?></a>
        <div>Артикул:
            <span class="value"><?= $oProduct->code ?></span>
        </div>
<!--        <div>Страна производитель:-->
<!--            <span class="value">Китай</span>-->
<!--        </div>-->
    </td>
    <td>
        <div>Продавец:
            <span class="value"><?= $oProduct->shop->title ?></span>
        </div>
        <div class="balance">
            На складе: <span class="value"><?= $oProduct->count ?> шт.</span>
        </div>
    </td>
    <td class="center">
        <div class="price">
<!--            <div class="old">1.78</div>-->

            <div class="actual"><?= $oProduct->price ?>$</div>

            <div class="info">
                <?php if( $oProduct->amount ): ?>
                    <div class="" title="Скидка на товар <?= $oProduct->manufacturer->name ?> <?= $oProduct->title ?> -  <?= $oProduct->amount ?> %">Скидка <?= $oProduct->amount ?> %</div>
                <?php else: ?>
                    <div class="noDiscounts" title="Основная система скидок не действует">без скидок</div>
                <?php endif; ?>
            </div>
        </div>
    </td>
    <td>
        <div class="qty-btn">
            <input type="text" class="count-product-input numeric" maxlength="4">
            <div class="min-qty">штук</div>
            <a href="<?= Url::to(['/order/create', 'id'=>$oProduct->id]) ?>" onclick="return false;" class="add-basket alt-add fancybox.ajax">
                <img class="empty" src="/images/cart.png">
            </a>
        </div>
    </td>
</tr>

