<div class="filterbar">
    <div class="filter-sort">
        <div class="title">Сортировать по:
            <div class="btn-group <?= $filterOpenType ?>">
                <?php
                $itemsSortMenu[] = [ 'label' => 'Заголовку (А-Я)', 'url' => \yii\helpers\Url::to( ['product/index','order'=>'title','type'=>'desc', 'url'=>$aCategory['url'], 'Product'=>['attributesEav'=>$aFilter] ]) ];
                $itemsSortMenu[] = [ 'label' => 'Заголовку (Я-А)', 'url' => \yii\helpers\Url::to( ['product/index','order'=>'title','type'=>'asc', 'url'=>$aCategory['url'], 'Product'=>['attributesEav'=>$aFilter] ]) ];
                $itemsSortMenu[] = [ 'label' => 'Цене (по убиванию)', 'url' => \yii\helpers\Url::to( ['product/index','order'=>'price','type'=>'desc', 'url'=>$aCategory['url'], 'Product'=>['attributesEav'=>$aFilter] ]) ];
                $itemsSortMenu[] = [ 'label' => 'Цене (по возрастанию)', 'url' => \yii\helpers\Url::to( ['product/index','order'=>'price','type'=>'asc', 'url'=>$aCategory['url'], 'Product'=>['attributesEav'=>$aFilter] ]) ];
                $itemsSortMenu[] = [ 'label' => 'Скидке (по убиванию)', 'url' => \yii\helpers\Url::to( ['product/index','order'=>'amount','type'=>'desc', 'url'=>$aCategory['url'], 'Product'=>['attributesEav'=>$aFilter] ]) ];
                $itemsSortMenu[] = [ 'label' => 'Скидке (по возрастанию)', 'url' => \yii\helpers\Url::to( ['product/index','order'=>'amount','type'=>'asc', 'url'=>$aCategory['url'], 'Product'=>['attributesEav'=>$aFilter] ]) ];
                $itemsSortMenu[] = [ 'label' => 'Артиклу (9-0)', 'url' => \yii\helpers\Url::to( ['product/index','order'=>'code','type'=>'desc', 'url'=>$aCategory['url'], 'Product'=>['attributesEav'=>$aFilter] ]) ];
                $itemsSortMenu[] = [ 'label' => 'Артиклу (0-9)', 'url' => \yii\helpers\Url::to( ['product/index','order'=>'code','type'=>'asc', 'url'=>$aCategory['url'], 'Product'=>['attributesEav'=>$aFilter] ]) ];
                echo \yii\bootstrap\ButtonDropdown::widget([
                    'label' => 'Выберите',
                    'dropdown' => [ 'items' => $itemsSortMenu ],
                    'options'=>[ 'class'=>'btn-success']
                  ]);
                ?>
            </div>
        </div>
    </div>

    <div class="filter-limit">
        <div class="left">
            Показывать по:
        </div>
        <div class="left">
                <span class="label label-info">
                    <a class="<?= $oPagination->getPageSize() == 10  ? 'active' : '' ?>" href="<?= \yii\helpers\Url::to( ['product/index','limit'=>10,  'url'=>$aCategory['url'], 'Product'=>['attributesEav'=>$aFilter] ]) ?>">10</a>
                </span>

                <span class="label label-info">
                    <a class="<?= $oPagination->getPageSize() == 20  ? 'active' : '' ?>" href="<?= \yii\helpers\Url::to( ['product/index','limit'=>20,  'url'=>$aCategory['url'], 'Product'=>['attributesEav'=>$aFilter] ]) ?>">20</a>
                </span>

                <span class="label label-info">
                    <a class="<?= $oPagination->getPageSize() == 50  ? 'active' : '' ?>" href="<?= \yii\helpers\Url::to( ['product/index','limit'=>50,  'url'=>$aCategory['url'], 'Product'=>['attributesEav'=>$aFilter] ]) ?>">50</a>
                </span>

                <span class="label label-info">
                    <a class="<?= $oPagination->getPageSize() == 100 ? 'active' : '' ?>" href="<?= \yii\helpers\Url::to( ['product/index','limit'=>100, 'url'=>$aCategory['url'], 'Product'=>['attributesEav'=>$aFilter] ]) ?>">100</a>
                </span>

                <span class="label label-info">
                    <a class="<?= $oPagination->getPageSize() == 500 ? 'active' : '' ?>" href="<?= \yii\helpers\Url::to( ['product/index','limit'=>500, 'url'=>$aCategory['url'], 'Product'=>['attributesEav'=>$aFilter] ]) ?>">500</a>
                </span>
        </div>
    </div>
</div>