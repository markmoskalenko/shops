<div class="wrapper product-container-w">
    <div class="row">
        <div class="col-xs-12 header">
            <ul class="left">
                <li>
                    <a href="">Лучшие товары</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="row product-container">
        <div class="col-xs-12">
            <?php foreach( $oModelProducts as $oModelProduct ): ?>
                <div class="col-xs-3 item">
                    <div class="header">
                        <span>Продавец и рейтинг</span>
                        <div class="star">
                        <?= \frontend\widgets\Raty::widget(['value'=>1]) ?>
                        </div>
                        <div class="clear store">Продавец "<?= $oModelProduct->shop->title ?>"</div>
                    </div>
                    <img src="<?= $oModelProduct->mainImage ? $oModelProduct->mainImage->getUrl(170,160) : 'http://placehold.it/170x160' ?>" alt="<?= $oModelProduct->shop->title ?> - <?= $oModelProduct->manufacturer->name ?> <?= $oModelProduct->title ?>"/>
                   <a class="title" href="<?= \yii\helpers\Url::to(['product/view', 'url'=>$oModelProduct->url]) ?>"> <strong><?= $oModelProduct->manufacturer->name ?> <?= $oModelProduct->title ?></strong></a>
                    <div class="price"><strong><?= $oModelProduct->price ?>$</strong></div>
                    <div class="control">
                        <span class="count">В наличии <?= $oModelProduct->count ?> шт.</span>
                        <a href="<?= \yii\helpers\Url::to(['order/create', 'id'=>$oModelProduct->id]) ?>" onclick="return false;" class="btn btn-primary add-basket fancybox.ajax">Куплю</a>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
</div>

<div class="indent-bottom"></div>

<div class="wrapper news">
    <h1>Новости недели</h1>

    <div class="row news-container">
        <div class="col-xs-4 item">
            <center>
            <img src="http://placehold.it/240x130" alt=""/>
            </center>
            <h2>
                <a href="">Уникальные профессии. Кремовар: «Не стоит </a>
            </h2>
            <div class="description">
                Onliner.by продолжает рассказывать о людях с уникальными профессиями. Сегодня в цикле «Работа» кремовар Юлия Титова рассказала нам, как стала этим заниматься, какие косметические продукты и ингредиенты особенно вредны для здоровья и почему белорусская косметика не хуже импортной премиум-класса.
            </div>
        </div>
        <div class="col-xs-4 item">
            <center>
            <img src="http://placehold.it/240x130" alt=""/>
            </center>
            <h2>
                <a href="">Уникальные профессии. Кремовар: «Не стоит </a>
            </h2>
            <div class="description">
                Onliner.by продолжает рассказывать о людях с уникальными профессиями. Сегодня в цикле «Работа» кремовар Юлия Титова рассказала нам, как стала этим заниматься, какие косметические продукты и ингредиенты особенно вредны для здоровья и почему белорусская косметика не хуже импортной премиум-класса.
            </div>

        </div>
        <div class="col-xs-4 item">
            <center>
            <img src="http://placehold.it/240x130" alt=""/>
            </center>
            <h2>
                <a href="">Уникальные профессии. Кремовар: «Не стоит </a>
            </h2>
            <div class="description">
                Onliner.by продолжает рассказывать о людях с уникальными профессиями. Сегодня в цикле «Работа» кремовар Юлия Титова рассказала нам, как стала этим заниматься, какие косметические продукты и ингредиенты особенно вредны для здоровья и почему белорусская косметика не хуже импортной премиум-класса.
            </div>

        </div>
    </div>
</div>

<div class="indent-bottom"></div>

<div class="wrapper">
    <div class="about">
        <h3 class="b-posts-1-item__title"><span>Манифест</span></h3>
            <p>Наша цель — предоставлять читателям максимально полную, непредвзятую и точную информацию о технике, а также помогать покупателям находить лучшие модели по минимальным ценам.<br></p>
            <h3>Редакционная политика</h3>
            <p>Статьи и новости сайта Onliner.by отражают только точку зрения автора. Материалы, размещенные на сайте, не продаются ни в каком виде. У нас нет заказных статей, новостей или обзоров.<br></p>
            <p>Мы уверены, что в любом уважающем себя издании между коммерческим и редакционным отделами должна вырасти стена, пробить которую не должен ни один доллар. Угрозы снять рекламу из-за острого материала на нас не действуют.<br></p>
            <p>Мы считаем, что проплаченные материалы недостойны называться «пиаром» и вызывают у целевой аудитории только отвращение. Мы готовы совершенно бесплатно и в наиболее полной форме писать об информационных поводах любых компаний.<br></p>
            <p>Мы равноудалены от всех компаний на рынке, деятельность которых описывается в материалах. Никакие бутерброды на фуршетах не заставят нас изменить свою редакционную позицию.<br></p>
    </div>
</div>