<?php $this->beginPage(); ?>
<?php $this->beginBody(); ?>
    <div class="fb-order-container">
    <div class="header">
            Продавец - ООО "ТехноСтиль"
            <div class="right">
                <?php echo \frontend\widgets\Raty::widget(['value'=>1]) ?>
            </div>
    </div>

    <div class="sidebar left">
        <div class="logo">
            <img src="http://placehold.it/220x100" alt=""/>
        </div>

        <h3>Магазины продавца</h3>
        <div class="list-group">
            <?php foreach( $aShops as $sCountry => $aShopCountry ): ?>

                <a href="javascript:void(0)" class="list-group-item active"><?=$sCountry; ?></a>

                <?php foreach( $aShopCountry as $sCity => $aShopCity ): ?>

                    <a href="javascript:void(0)" class="list-group-item city"><?=$sCity; ?></a>

                    <?php foreach( $aShopCity as $oShop ): ?>

                        <a href="<?= \yii\helpers\Url::to(['shop/get-info','id'=>$oShop->id]) ?>" class="list-group-item shop">- <?=$oShop->title; ?></a>

                    <?php endforeach; ?>

                <?php endforeach; ?>

            <?php endforeach; ?>
        </div>
    </div>
    <div class="content-order shop-info">
        <?= $this->render('/shop/_info',['oShop'=>$oProduct->shop]); ?>
    </div>

    <div class="content-order">
        <h3>Оформление заявки</h3>
        <?php if( !Yii::$app->user->isGuest ): ?>
            <?php if( $isAllowedOrder ): ?>
                <input type="hidden" value="<?=$oProduct->id; ?>" id="order-product-id"/>
                <input type="hidden" value="<?=$oProduct->shop->id; ?>" id="order-shop-id"/>
                <div class="order">
                    Количество: <input type="number" value="<?=$productCount ?>" id="order-count" class="form-control numeric" type="text"/> шт.
                    <div>                Пожелания к заказу:
                    </div>
                    <textarea id="order_client_comment" cols="30" rows="10"></textarea>
                    <a href="<?= \yii\helpers\Url::to(['order/confirm']); ?>" class="btn btn-success" id="order-confirm">Подтвердить</a>
                </div>
            <?php else: ?>
                <div class="order">Вы не можете заказывать товар у самого себя.</div>
            <?php endif; ?>
        <?php else: ?>
            <div class="order">Для оформления заявки <a href="<?= \yii\helpers\Url::to(['user/login'])?>">авторизуйтесь</a> или <a href="<?= \yii\helpers\Url::to(['user/signup'])?>">зарегистрируйтесь</a>.</div>
        <?php endif; ?>
    </div>

    <div class="content-order">

        <h3>Описание товара</h3>

        <span class="code">
            Код товара - <?= $oProduct->code ?>
        </span>
        <img src="<?= $oProduct->mainImage ? $oProduct->mainImage->getUrl(140,140) : 'http://placehold.it/140x140' ?>" alt=""/>

        <div class="mini-description">
            <h4><?= $oProduct->manufacturer->name ?> <?= $oProduct->title ?></h4>

            <div class="price">

                <div class="actual">Стоимость - <?= $oProduct->price ?>$ <span>
                        <?php if( $oProduct->amount ): ?>
                            Скидка <?= $oProduct->amount ?> %
                        <?php else: ?>
                            без скидок
                        <?php endif; ?>
                </span></div>

                <div class="info">


                </div>
            </div>

        </div>
        <div class="description">
            <table class="table table-bordered">
                <?php foreach( $oProduct->getEavFront() as $aAttribute ): ?>
                    <tr>
                        <td><?=$aAttribute['title'] ?></td>
                        <td><?=implode(',', $aAttribute['value']) ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>
    <script type="text/javascript">
        $('.shop').click(function(){
            var href = $(this).attr('href');
            $.ajax({
                url : href
            }).done(function(r){
                $('.shop-info').html(r);
            });
            return false;
        });

        $('#order-confirm').click(function(){
            var product_id = $('#order-product-id').val();
            var shop_id = $('#order-shop-id').val();
            var product_count = $('#order-count').val();
            var client_comment = $('#order_client_comment').val();

            var href = $(this).attr('href');

            $.ajax({
                url : href,
                data : {
                    productId : product_id,
                    productCount : product_count,
                    shopId : shop_id,
                    clientComment : client_comment
                }
            }).done(function(r){
                if( r.status == true ){
                    $.fancybox.close();
                    $.jGrowl("Заявка отправлена.<br><a href=''>Просмотреть мои заявки.</a>");
                }
            });
            return false;
        });
    </script>

    <style type="text/css">
        .order textarea#order_client_comment{
            display: block;
            width: 100%;
            height: 100px;
            margin: 10px 0;
        }
    </style>
<?php $this->endBody(); ?>
<?php $this->endPage() ?>