<h3><?= $oShop->title ?></h3>
<ul>
    <li><span>Страна:</span> <i><?= !empty($oShop->country->name) ? $oShop->country->name : 'Не указано' ?></i></li>
    <li><span>Регион:</span> <i><?= !empty($oShop->region->name) ? $oShop->region->name : 'Не указано' ?></i></li>
    <li><span>Город:</span> <i><?= !empty($oShop->city->name) ? $oShop->city->name : 'Не указано' ?></i></li>
    <li><span>Улица:</span> <i><?= !empty($oShop->street) ? $oShop->street : 'Не указано' ?></i></li>
    <li><span>Дом:</span> <i><?= !empty($oShop->house) ? $oShop->house : 'Не указано' ?></i></li>
    <li><span>Корпус:</span> <i><?= !empty($oShop->corps) ? $oShop->corps : 'Не указано' ?></i></li>
</ul>

<ul>
    <li><span>Офис:</span> <i><?= !empty($oShop->cabinet) ? $oShop->cabinet : 'Не указано' ?></i></li>
    <li><span>Телефон:</span> <i><?= !empty($oShop->phone) ? $oShop->phone : 'Не указано' ?></i></li>
    <li><span>Факс:</span> <i><?= !empty($oShop->fax) ? $oShop->fax : 'Не указано' ?></i></li>
    <li><span>Email:</span> <i><?= !empty($oShop->email) ? $oShop->email : 'Не указано' ?></i></li>
    <li><span>Skype:</span> <i><?= !empty($oShop->skype) ? $oShop->skype : 'Не указано' ?></i></li>
</ul>