<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
frontend\assets\_UserSignUpAsset::register($this);
/**
 * @var yii\web\View           $this
 * @var yii\widgets\ActiveForm $form
 * @var common\models\User     $model
 */
$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1>Регистрация</h1>
    <div class="row-fluid">
    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
        <?= $form->field($model, 'username') ?>
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'phone')->textInput() ?>
        <?= $form->field($model, 'country_id')->dropDownList( $aCounties, ['prompt'=>'-- Выберите страну --'] ) ?>
        <?= $form->field($model, 'region_id' )->dropDownList( $aRegions, ['prompt'=>'-- Выберите регион --'] ) ?>
        <?= $form->field($model, 'city_id')->dropDownList( $aCities, ['prompt'=>'-- Выберите город --'] ) ?>

    <div class="form-group">
            <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-success']) ?>
            <a class="btn btn-primary" href="/">Верниться на сайт</a>
        </div>
    <?php ActiveForm::end(); ?>
    </div>
</div>