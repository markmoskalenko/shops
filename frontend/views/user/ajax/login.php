<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var app\models\LoginForm $model
 */
$this->title = 'Вход';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login-ajax-form">
    <div class="header"><span>ВОЙТИ</span> или <a href="">ЗАРЕГИСТРИРОВАТЬСЯ</a></div>

	<div class="form">
        <?php $form = ActiveForm::begin( [ 'id' => 'login-form' ] ); ?>

            <div class="left" style="margin-right: 20px">
                <?= $form->field($model, 'username')->textInput() ?>
            </div>

            <div class="left">
                <?= $form->field($model, 'password')->passwordInput()  ?>
            </div>

            <div class="clear"></div>


            <div style="color:#999;margin:1em 0">
                Восстановить доступ к акаунту <?= Html::a('сбросить пароль', ['site/request-password-reset']) ?>.
            </div>
        <?= $form->field($model, 'rememberMe')->checkbox() ?>

            <div class="form-group control-button">
                <?= Html::submitButton('Войти', ['class' => 'button-yellow submit']) ?>
            </div>

        <?php ActiveForm::end(); ?>
	</div>
</div>
<script type="text/javascript" src="/js/script.loginFormAjax.js"></script>